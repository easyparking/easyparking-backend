var env = process.env.NODE_ENV || 'production'
let	config = require('./config/config');
let	log = config.log, 
	App = require('./config/server');
require("dotenv").config();

log.info("env %j", env);

App.initialize(config, function(app){
	App.run(app, config, process.env.PORT || config.port); 
});
