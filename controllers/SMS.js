const axios = require('axios');
const { LOGOUT_MSG,
  EMPLOYEE_LOGIN,
  PARKING_ENTRY,
  PARKING_EXIT,
  MONTHLY_CARD_PURCHASE_OFFLINE,
  MONTHLY_CARD_PURCHASE_ONLINE,
  MONTHLY_CARD_REMINDER,
  MONTHLY_CARD_EXPIRED,
  MATCHED_VEHICLE,
  SLIP_LOST,
  STAND_RENEWAL_REMINDER,
  STAND_LICENCE_EXPIRED,
  STAND_CREATED,
  APP_DOWNLOAD_URL
} = require('../config/constants');
const config = require('../config/config');
const logger = require('../config/logger');
exports.testSMS = async (message, mobile) => {
  const mobileNumber = mobile;
  const url = 'https://api.textlocal.in/send/?apikey=' + config.textLocalAPIkey + '&numbers=' + mobileNumber + '&sender=STSPAR&message=' + encodeURIComponent(message);
  let response;
  await axios
    .get(url)
    .then(function (resp) {
      response = resp.data
    })
    .catch(function (error) {
      response = error;
      console.log(error);
    });
  return response
}

exports.sendSMS = async (option, data, mobileNumber) => {
  try {
    let message = '';
    let senderId = '';
    if (option === 'employee_logout') {
      message = LOGOUT_MSG(data);
      senderId = 'STSPAR';
    } else if (option === 'employee_login') {
      message = EMPLOYEE_LOGIN(data, data.ownerDetails);
      senderId = 'STSPAR';
    } else if (option === 'parking_entry') {
      message = PARKING_ENTRY(data);
      senderId = 'STSPAR';
    } else if (option === 'parking_exit') {
      message = PARKING_EXIT(data);
      senderId = 'STSPAR';
    } else if (option === 'monthlycard_purchase_offline') {
      message = MONTHLY_CARD_PURCHASE_OFFLINE(data);
      senderId = '690528';
    } else if (option === 'monthlycard_purchase_online') {
      message = MONTHLY_CARD_PURCHASE_ONLINE(data);
      senderId = 'STSPAR';
    } else if (option === 'monthlycard_renewal_alert_3') {
      message = MONTHLY_CARD_REMINDER(data);
      senderId = 'STSPAR';
    } else if (option === 'monthlycard_renewal_alert_1') {
      message = MONTHLY_CARD_REMINDER(data);
      senderId = 'STSPAR';
    } else if (option === 'monthlycard_renewal_alert') {
      message = MONTHLY_CARD_REMINDER(data);
      senderId = 'STSPAR';
    } else if (option === 'monthlycard_renewal_success') {
      message = MONTHLY_CARD_REMINDER(data);
      senderId = 'STSPAR';
    } else if (option === 'monthlycard_expired') {
      message = MONTHLY_CARD_EXPIRED(data);
      senderId = 'STSPAR';
    } else if (option === 'matched_vechile') {
      message = MATCHED_VEHICLE(data, data.vehicleData);
      senderId = 'STSPAR';
    } else if (option === 'sliplost_vehicle') {
      message = SLIP_LOST(data);
      senderId = 'STSPAR';
    } else if (option === 'Owner_Renewal') {
      message = STAND_RENEWAL_REMINDER(data);
      senderId = 'STSPAR';
    } else if (option === 'Owner_Expired') {
      message = STAND_LICENCE_EXPIRED(data);
      senderId = 'STSPAR';
    }  else if (option === 'app_registration') {
      message = APP_DOWNLOAD_URL();
      senderId = 'STSPAR';
    }  else if (option === 'Stand_creation') {
      message = STAND_CREATED(data);
      senderId = 'STSPAR';
    }
    const url = `https://api.textlocal.in/send/?apikey=${config.textLocalAPIkey}&numbers=${mobileNumber}&sender=${senderId}&message=${encodeURIComponent(message)}`;
    let response;
    logger.warn({message: `SMS content: ${message}`});
    logger.warn({message: `SMS URL: ${url}`});
    await axios
      .get(url)
      .then(function (resp) {
        response = resp.data;
        logger.warn({message: `SMS sent succesfully, ${JSON.stringify(response)}`})
      })
      .catch(function (error) {
        response = error;
        logger.error({message: `Error while sending SMS, ${JSON.stringify(response)}`})
      });
    return response
  } catch (error) {
    return error
  }
}

exports.getSMSBalance = async () => {
  var url = 'https://api.textlocal.in/balance/?apikey=' + config.textLocalAPIkey;
  let result;
  await axios
    .get(url)
    .then(function (response) {
      result = response.data.balance;
    })
    .catch(function (error) {
      result = error;
    });
  return result;
}
exports.getSMSSent = async () => {
  var url = 'https://api.textlocal.in/get_history_api/?apikey=' + config.textLocalAPIkey;
  let result;
  await axios
    .get(url)
    .then(function (response) {
      result = response.data.total;
    })
    .catch(function (error) {
      result = error;
    });
  return result;
}

