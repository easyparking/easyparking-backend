exports.getLoginActivity = async(req,res)=> {
    const {searchText, startDate, endDate, userName, size, pageNo, ownerId, standId} = req.body;
    if (pageNo < 0 || pageNo === 0) {
        return res.json({ status: "fail", message: "invalid page number" });
    }
    const skip = size * (pageNo - 1);
    const limit = size;
    try {
        let query = {};
        let condition = [{
            createdDate: {
                $gte: new Date(startDate),
                $lt: new Date(endDate)
            }
        },{role: 'employee'},{ownerId}]
        if(userName){
            condition.push({userName});
        }
        if(standId){
            condition.push({standId});
        }
        if (searchText) {
            condition.push({
                $or: [
                    { 'userName': { $regex:  searchText, $options: 'i' } },
                    { 'employeeId': { $regex:  searchText, $options: 'i' } },
                    { 'name': { $regex:  searchText, $options: 'i' } },
                    { 'phone': { $regex:  searchText, $options: 'i' } },
                    { 'ipaddress': { $regex:  searchText, $options: 'i' } },
                ]
            })
        } 
            query = {$and: condition}
            let totalCount = await Sessions.countDocuments(query).exec();
            if (totalCount < 0) {
                throw new ApplicationError("No Login Activiy data found", 500, "");
            }
            const result = await Sessions.find(query).skip(skip).limit(limit).sort({ '_id': -1 }).exec();
            if (!_.isEmpty(result)) {
                return res.json({ status: "success", message: "Login Activities Data", data: result, totalCount: totalCount });
            } else {
                throw new ApplicationError("No Login Activiy data found", 500, "");
            }
        
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

exports.getAllEmployeesList = async(req, res) =>{
    try {
        const {ownerId} = req.params || 'W21093';
        const result = await Users.find({$and: [{role: 'employee'},{status: 'Active'},{ownerId}]}).exec();
        res.json({status: 'success', result});
    } catch(error) {
        res.json({status: 'fail', error});
    }
}