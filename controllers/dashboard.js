const SMS = require("./SMS")
exports.getAllData = async function (req, res) {

    var searchObject = req.body
    try {
        if (!_.isEmpty(searchObject)) {
        } else {
            var activeOwners = await Users.countDocuments({ role: "owner" ,status : 'Active'}).exec();
            var expired = await Users.countDocuments({ role: "owner" ,status : 'InActive'}).exec();

            var finalData = {}
            finalData.accounts = {
                activeOwners : activeOwners,
                expired : expired, 
                totalCount : activeOwners + expired
            }
            
            return res.json({ status: "success", message: "Data", data: finalData });


            if (_.isEmpty(activeOwners) || _.isEmpty(activeOwners)) {
                throw new ApplicationError("Owners data not found", 500, "");
            } else if (!_.isEmpty(activeOwners) || !_.isEmpty(activeOwners)){
                return res.json({ status: "success", message: "No Data", data: ownerslist });
            }

        }

    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

//Admin Dashboard details
exports.getDashboardDetails = async (req, res)=>{
    try{
        const query = helper.getOwnerCountsQuery();
        const activeAccounts = await Users.aggregate(query).exec();
        let accountsCount = {
            Active: activeAccounts.length && activeAccounts[0].Active ? activeAccounts[0].Active : 0,
            Total: activeAccounts.length && activeAccounts[0].Total ? activeAccounts[0].Total : 0,
        };
        const vehiclesQuery = helper.getDashboardVehicleQuery();
        const parkingsResult = await Parkings.aggregate(vehiclesQuery).exec();
        let vehiclesCount = {
            Exit: parkingsResult.length && parkingsResult[0].Exit ? parkingsResult[0].Exit : 0,
            Cancelled: parkingsResult.length && parkingsResult[0].Cancelled ? parkingsResult[0].Cancelled : 0,
            Total: parkingsResult.length && parkingsResult[0].Total ? parkingsResult[0].Total : 0,
        };
        const standsquery = helper.getOnlineCardsQuery();
        const standsResult = await Stands.aggregate(standsquery).exec();
        const standsCount = {
            Active: standsResult.length && standsResult[0].OnlineCards ? standsResult[0].OnlineCards : 0,
            Total: standsResult.length && standsResult[0].Total ? standsResult[0].Total : 0,
        };
        const SMS_Balance = await SMS.getSMSBalance();
        const SMS_Sent = await SMS.getSMSSent();
        const wantedVehiclesquery = helper.getWantedVehiclesQuery();
        const wantedVehicles = await WantedVehicles.aggregate(wantedVehiclesquery).exec();
        return res.json({ status: 'success',
         standsCount: standsCount,
          accountsCount: accountsCount,
          SMS_Balance: SMS_Balance,
           SMS_Sent: SMS_Sent,
        vehiclesCount: vehiclesCount,
        wantedVehicles: wantedVehicles.length && wantedVehicles[0].wanted ? wantedVehicles[0].wanted : 0,
        matchedVehicles: wantedVehicles.length && wantedVehicles[0].matched ? wantedVehicles[0].matched : 0
      })
    } catch(e){
        return res.json({ status: "fail", message: e.message });
    }
}

//Owner Dashboard details
exports.getOwnersDashboard = async function (req, res) {
    try {
        const ownerId = req.params.ownerId;
        if (_.isEmpty(ownerId)) {
            return res.json({ status: "fail", message: "Enter required parameters" });
        }
        const date = moment().subtract(30, 'days').toDate();
        const query = helper.getOwnerVehiclesQuery(ownerId, date);
        const parkingsResult = await Parkings.aggregate(query).exec();
        let dashboardResult = [{
            type: "bicycle",
            collection: 0,
            total: parkingsResult.length && parkingsResult[0].bicycle ? parkingsResult[0].bicycle : 0,
            exit: parkingsResult.length && parkingsResult[0].bicycleExit ? parkingsResult[0].bicycleExit : 0,
        },{
            type: "twoWheeler",
            collection: 0,
            total: parkingsResult.length && parkingsResult[0].twoWheeler ? parkingsResult[0].twoWheeler : 0,
            exit: parkingsResult.length && parkingsResult[0].twoWheelerExit ? parkingsResult[0].twoWheelerExit : 0,
        },{
            type: "threeWheeler",
            collection: 0,
            total: parkingsResult.length && parkingsResult[0].threeWheeler ? parkingsResult[0].threeWheeler : 0,
            exit: parkingsResult.length && parkingsResult[0].threeWheelerExit ? parkingsResult[0].threeWheelerExit : 0,
        },{
            type: "fourWheeler",
            collection: 0,
            total: parkingsResult.length && parkingsResult[0].fourWheeler ? parkingsResult[0].fourWheeler : 0,
            exit: parkingsResult.length && parkingsResult[0].fourWheelerExit ? parkingsResult[0].fourWheelerExit : 0,
        }];
        const vehcileTypesData = await PriceRules.find({ownerId: ownerId}).exec();
        if(vehcileTypesData.length) {
            if(vehcileTypesData[0].vehicleTypes.length > 4) {
                const vehicleTypes = vehcileTypesData[0].vehicleTypes;
            const extraCount = await getExtraVehiclesCount(ownerId,vehicleTypes, date);
            dashboardResult.push(...extraCount);
            }
        }

        const defaultVehicles = ['bicycle', 'twoWheeler', 'threeWheeler', 'fourWheeler'];
        const vehiclesData = vehcileTypesData.map(item => item.vehicleTypes);
        const types = vehiclesData && vehiclesData.length ? vehiclesData[0].map(item => item.vehicletype) : defaultVehicles;
        for(let i=0;i<types.length;i++){
            let data = {
                ownerId: ownerId,
                date: date,
                vehicleType: types[i]
            }
            const collectionCountquery = await getTotalCollectionByVehicleByOwnerQuery(data);
            const totalCollectionByVehicle = await Parkings.aggregate(collectionCountquery).exec();
            const parkingsAmount = totalCollectionByVehicle.length ? totalCollectionByVehicle[0].SUM : 0;
            let selectedVehicle = dashboardResult.filter(item => item.type ==  types[i])[0];
            selectedVehicle.collection = parkingsAmount;
        }
        return res.json({ status: "success", message: "Dashboard Data", data: dashboardResult });
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

exports.getParkingsCountByStand = async function (req, res) {
    try {
        const ownerId = req.query.ownerId;
        const standId = req.query.standId;
        const date = moment().subtract(30, 'days').toDate();
        const query = helper.getVehiclesByStandQuery(ownerId, standId, date);
        const cardsCountquery = helper.getCardsByStandQuery(ownerId, standId,date);
        if (ownerId == 'undefined' || standId == 'undefined') {
            return res.json({ status: "fail", message: "Enter required parameters" });
        }
        const parkingsResult = await Parkings.aggregate(query).exec();
        const cardsResult = await Cards.aggregate(cardsCountquery).exec();
        let dashboardResult = [{
            type: "bicycle",
            collection: 0,
            offlineCards: cardsResult.length && cardsResult[0].bicycleCount ? cardsResult[0].bicycleCount : 0,
            total: parkingsResult.length && parkingsResult[0].bicycle ? parkingsResult[0].bicycle : 0,
            cancelled: parkingsResult.length && parkingsResult[0].bicycleCancelled ? parkingsResult[0].bicycleCancelled : 0,
            exit: parkingsResult.length && parkingsResult[0].bicycleExit ? parkingsResult[0].bicycleExit : 0,
        },{
            type: "twoWheeler",
            collection: 0,
            offlineCards: cardsResult.length && cardsResult[0].twoWheelerCount ? cardsResult[0].twoWheelerCount : 0,
            cancelled: parkingsResult.length && parkingsResult[0].twoWheelerCancelled ? parkingsResult[0].twoWheelerCancelled : 0,
            total: parkingsResult.length && parkingsResult[0].twoWheeler ? parkingsResult[0].twoWheeler : 0,
            exit: parkingsResult.length && parkingsResult[0].twoWheelerExit ? parkingsResult[0].twoWheelerExit : 0,
        },{
            type: "threeWheeler",
            collection: 0,
            offlineCards: cardsResult.length && cardsResult[0].threeWheelerCount ? cardsResult[0].threeWheelerCount : 0,
            cancelled: parkingsResult.length && parkingsResult[0].threeWheelerCancelled ? parkingsResult[0].threeWheelerCancelled : 0,
            total: parkingsResult.length && parkingsResult[0].threeWheeler ? parkingsResult[0].threeWheeler : 0,
            exit: parkingsResult.length && parkingsResult[0].threeWheelerExit ? parkingsResult[0].threeWheelerExit : 0,
        },{
            type: "fourWheeler",
            collection: 0,
            offlineCards: cardsResult.length && cardsResult[0].fourWheelerCount ? cardsResult[0].fourWheelerCount : 0,
            cancelled: parkingsResult.length && parkingsResult[0].fourWheelerCancelled ? parkingsResult[0].fourWheelerCancelled : 0,
            total: parkingsResult.length && parkingsResult[0].fourWheeler ? parkingsResult[0].fourWheeler : 0,
            exit: parkingsResult.length && parkingsResult[0].fourWheelerExit ? parkingsResult[0].fourWheelerExit : 0,
        }];
        const vehcileTypesData = await PriceRules.find({ownerId: ownerId}).exec();
        if(vehcileTypesData.length) {
            if(vehcileTypesData[0].vehicleTypes.length > 4) {
                const vehicleTypes = vehcileTypesData[0].vehicleTypes;
                const extraCount = await getExtraVehiclesCount(ownerId,vehicleTypes, standId, date);
                dashboardResult.push(...extraCount);
            }
        }
        const defaultVehicles = ['bicycle', 'twoWheeler', 'threeWheeler', 'fourWheeler'];
        const vehiclesData = vehcileTypesData.map(item => item.vehicleTypes);
        const types = vehiclesData && vehiclesData.length ? vehiclesData[0].map(item => item.vehicletype) : defaultVehicles;
        for(let i=0;i<types.length;i++){
            let data = {
                ownerId: ownerId,
                standId: standId,
                date: date,
                vehicleType: types[i]
            }
            const collectionCountquery = await getTotalCollectionByVehicleByStandQuery(data);
            const totalCollectionByVehicle = await Parkings.aggregate(collectionCountquery).exec();
            const parkingsAmount = totalCollectionByVehicle.length ? totalCollectionByVehicle[0].SUM : 0;
            let selectedVehicle = dashboardResult.filter(item => item.type ==  types[i])[0];
            selectedVehicle.collection = parkingsAmount;
        }
        return res.json({ status: "success", message: "parkings Data", data: dashboardResult});
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

exports.getOwnerDashboardDetails = async function(req, res) {
    try {
        let empId = ""
        const standId = req.query.standId;
        const ownerId = req.query.ownerId;
        let date = moment().subtract(1, 'days').toDate();
        const sessionResult = await Sessions.find({standId,ownerId}).sort({_id:-1}).limit(1);
        
        if(sessionResult.length){
            date = new Date(sessionResult[0].start_time);
            empId = sessionResult[0].employeeId;
        } else {
            return res.json({ status: "fail", message: "No sessions from employee." })
        }
        const query = helper.getVehiclesByEmployeeStandQuery(empId, standId, ownerId, date);
        const cardsCountquery = helper.getCardsByStandQuery(ownerId, standId, date);
        const olineCardsCountquery = helper.getOnlineCardsByStandQuery(ownerId, standId, date);
        if (_.isEmpty(empId) || _.isEmpty(ownerId) || _.isEmpty(standId)) {
            return res.json({ status: "fail", message: "Enter required parameters" });
        }
        const parkingsResult = await Parkings.aggregate(query).exec();
        const cardsResult = await Cards.aggregate(cardsCountquery).exec();
        const onlineCards = await Cards.aggregate(olineCardsCountquery).exec();
        let dashboardResult = [{
            type: "bicycle",
            collection: 0,
            offlineCards: cardsResult.length && cardsResult[0].bicycleCount ? cardsResult[0].bicycleCount : 0,
            onlineCards: onlineCards.length && onlineCards[0].bicycleCount ? onlineCards[0].bicycleCount : 0,
            total: parkingsResult.length && parkingsResult[0].bicycle ? parkingsResult[0].bicycle : 0,
            pending: parkingsResult.length && parkingsResult[0].bicyclePending ? parkingsResult[0].bicyclePending : 0,
            pendingOnLogout: parkingsResult.length && parkingsResult[0].bicyclePendingOnLogout ? parkingsResult[0].bicyclePendingOnLogout : 0,
            cancelled: parkingsResult.length && parkingsResult[0].bicycleCancelled ? parkingsResult[0].bicycleCancelled : 0,
            exit: parkingsResult.length && parkingsResult[0].bicycleExit ? parkingsResult[0].bicycleExit : 0,
        },{
            type: "twoWheeler",
            collection: 0,
            offlineCards: cardsResult.length && cardsResult[0].twoWheelerCount ? cardsResult[0].twoWheelerCount : 0,
            onlineCards: onlineCards.length && onlineCards[0].twoWheelerCount ? onlineCards[0].twoWheelerCount : 0,
            cancelled: parkingsResult.length && parkingsResult[0].twoWheelerCancelled ? parkingsResult[0].twoWheelerCancelled : 0,
            pending: parkingsResult.length && parkingsResult[0].twoWheelerPending ? parkingsResult[0].twoWheelerPending : 0,
            pendingOnLogout: parkingsResult.length && parkingsResult[0].twoWheelerPendingOnLogout ? parkingsResult[0].twoWheelerPendingOnLogout : 0,
            total: parkingsResult.length && parkingsResult[0].twoWheeler ? parkingsResult[0].twoWheeler : 0,
            exit: parkingsResult.length && parkingsResult[0].twoWheelerExit ? parkingsResult[0].twoWheelerExit : 0,
        },{
            type: "threeWheeler",
            collection: 0,
            onlineCards: onlineCards.length && onlineCards[0].threeWheelerCount ? onlineCards[0].threeWheelerCount : 0,
            offlineCards: cardsResult.length && cardsResult[0].threeWheelerCount ? cardsResult[0].threeWheelerCount : 0,
            cancelled: parkingsResult.length && parkingsResult[0].threeWheelerCancelled ? parkingsResult[0].threeWheelerCancelled : 0,
            pending: parkingsResult.length && parkingsResult[0].threeWheelerPending ? parkingsResult[0].threeWheelerPending : 0,
            pendingOnLogout: parkingsResult.length && parkingsResult[0].threeWheelerPendingOnLogout ? parkingsResult[0].threeWheelerPendingOnLogout : 0,
            total: parkingsResult.length && parkingsResult[0].threeWheeler ? parkingsResult[0].threeWheeler : 0,
            exit: parkingsResult.length && parkingsResult[0].threeWheelerExit ? parkingsResult[0].threeWheelerExit : 0,
        },{
            type: "fourWheeler",
            collection: 0,
            onlineCards: onlineCards.length && onlineCards[0].fourWheelerCount ? onlineCards[0].fourWheelerCount : 0,
            offlineCards: cardsResult.length && cardsResult[0].fourWheelerCount ? cardsResult[0].fourWheelerCount : 0,
            cancelled: parkingsResult.length && parkingsResult[0].fourWheelerCancelled ? parkingsResult[0].fourWheelerCancelled : 0,
            pending: parkingsResult.length && parkingsResult[0].fourWheelerPending ? parkingsResult[0].fourWheelerPending : 0,
            pendingOnLogout: parkingsResult.length && parkingsResult[0].fourWheelerPendingOnLogout ? parkingsResult[0].fourWheelerPendingOnLogout : 0,
            total: parkingsResult.length && parkingsResult[0].fourWheeler ? parkingsResult[0].fourWheeler : 0,
            exit: parkingsResult.length && parkingsResult[0].fourWheelerExit ? parkingsResult[0].fourWheelerExit : 0,
        }];
        const vehcileTypesData = await PriceRules.find({standId: standId}).exec();
        if(vehcileTypesData.length) {
            if(vehcileTypesData[0].vehicleTypes.length > 4) {
                const vehicleTypes = vehcileTypesData[0].vehicleTypes;
                const extraCount = await getExtraVehiclesCount(ownerId,vehicleTypes, standId, date);
                dashboardResult.push(...extraCount);
            }
        }
        const defaultVehicles = ['bicycle', 'twoWheeler', 'threeWheeler', 'fourWheeler'];
        const vehiclesData = vehcileTypesData.map(item => item.vehicleTypes);
        const types = vehiclesData && vehiclesData.length ? vehiclesData[0].map(item => item.vehicletype) : defaultVehicles;
        for(let i=0;i<types.length;i++){
            let data = {
                ownerId: ownerId,
                standId: standId,
                employeeId:empId,
                date: date,
                vehicleType: types[i]
            }
            const collectionCountquery = await getTotalCollectionByVehicleQuery(data);
            const offlineCardCollectionCountquery = await getTotalCollectionByCardQuery(data);
            const totalCollectionByVehicle = await Parkings.aggregate(collectionCountquery).exec();
            const totalCollectionByOfflineCard = await Cards.aggregate(offlineCardCollectionCountquery).exec();
            const parkingsAmount = totalCollectionByVehicle.length ? totalCollectionByVehicle[0].SUM : 0;
            const cardsAmount = totalCollectionByOfflineCard.length ? totalCollectionByOfflineCard[0].SUM : 0;
            let selectedVehicle = dashboardResult.filter(item => item.type ==  types[i])[0];
            selectedVehicle.collection = parkingsAmount;
            selectedVehicle.cardsAmount = cardsAmount;
        }

        return res.json({ status: "success", message: "parkings Data", data: dashboardResult, sessionResult });
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

exports.getGraphData = async (req, res) => {
    try{
        const {ownerId} = req.params;
        const {startDate, endDate} = req.body;
        const parkingsCount = await Parkings.aggregate(
            [   
              {
                $match: {
                    ownerId: ownerId,
                    dateAndTimeOfParking: {
                        $gte: new Date(startDate),
                        $lte: new Date(endDate)
                    }
                }
              },
                {
                    $group:
                    {
                        _id:
                        {
                            day: { $dayOfMonth: "$dateAndTimeOfParking" },
                            month: { $month: "$dateAndTimeOfParking" }, 
                            year: { $year: "$dateAndTimeOfParking" }
                        }, 
                        count: { $sum:1 },
                        dateAndTimeOfParking: { $first: "$dateAndTimeOfParking" }
                    }
                },
                {
                    $project:
                    {
                        dateAndTimeOfParking:
                        {
                            $dateToString: { format: "%Y-%m-%d", date: "$dateAndTimeOfParking" }
                        },
                        count: 1,
                        _id: 0
                    }
                }
            ]);
        const collectionCount = await Parkings.aggregate(
            [{
                $match: {
                    ownerId: ownerId,
                    dateAndTimeOfExit: {
                        $gte: new Date(startDate),
                        $lte: new Date(endDate)
                    }
                }
              },{
                $group : {
                    _id:
                    {
                        day: { $dayOfMonth: "$dateAndTimeOfExit" },
                        month: { $month: "$dateAndTimeOfExit" }, 
                        year: { $year: "$dateAndTimeOfExit" }
                    }, 
                    dateAndTimeOfExit: { $first: "$dateAndTimeOfExit" },
                    totalPrice: { $sum: "$finalPrice" },
                    count: { $sum: 1 }
                        }
                    }, {
                        $project:
                        {
                            dateAndTimeOfExit:
                            {
                                $dateToString: { format: "%Y-%m-%d", date: "$dateAndTimeOfExit" }
                            },
                            totalPrice: "$totalPrice",
                            count: 1,
                            _id: 0
                        }
                    }
            ] )
            res.json({status: 'success', parkingsCount, collectionCount})
    } catch(error) {

    }
}

//Employee Dashboard details
exports.getParkingsByEmployee = async function (req, res) {
    try {
        const empId = req.params.id;
        const ownerId = req.query.ownerId;
        const standId = req.query.standId;
        let date = moment().subtract(1, 'days').toDate();
        
        const sessionResult = await Sessions.find({employeeId : empId}).sort({_id:-1}).limit(1);
        if(sessionResult.length){
            date = new Date(sessionResult[0].start_time.toString);
        }
        if (_.isEmpty(empId) || _.isEmpty(empId) | _.isEmpty(empId)) {
            return res.json({ status: "fail", message: "Enter required parameters" });
        }
        const query = helper.getVehiclesByEmployeeQuery(empId,date);
        const parkingsResult = await Parkings.aggregate(query).exec();
        let dashboardResult = [{
            type: "bicycle",
            total: parkingsResult.length && parkingsResult[0].bicycle ? parkingsResult[0].bicycle : 0,
            exit: parkingsResult.length && parkingsResult[0].bicycleExit ? parkingsResult[0].bicycleExit : 0,
            offlineCards: 0
        },{
            type: "twoWheeler",
            offlineCards: 0,
            total: parkingsResult.length && parkingsResult[0].twoWheeler ? parkingsResult[0].twoWheeler : 0,
            exit: parkingsResult.length && parkingsResult[0].twoWheelerExit ? parkingsResult[0].twoWheelerExit : 0,
        },{
            type: "threeWheeler",
            offlineCards: 0,
            total: parkingsResult.length && parkingsResult[0].threeWheeler ? parkingsResult[0].threeWheeler : 0,
            exit: parkingsResult.length && parkingsResult[0].threeWheelerExit ? parkingsResult[0].threeWheelerExit : 0,
        },{
            type: "fourWheeler",
            offlineCards: 0,
            total: parkingsResult.length && parkingsResult[0].fourWheeler ? parkingsResult[0].fourWheeler : 0,
            exit: parkingsResult.length && parkingsResult[0].fourWheelerExit ? parkingsResult[0].fourWheelerExit : 0,
        }];
        const vehcileTypesData = await PriceRules.find({standId: standId}).exec();
        if(vehcileTypesData.length) {
            if(vehcileTypesData[0].vehicleTypes.length > 4) {
                const vehicleTypes = vehcileTypesData[0].vehicleTypes;
            const extraCount = await getExtraVehiclesCount(ownerId,vehicleTypes, standId, date);
            dashboardResult.push(...extraCount);
            }
        }
        return res.json({ status: "success", message: "parkings Data", data: dashboardResult });
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

exports.getParkingsByEmployeeStand = async function (req, res) {
    try {
        const empId = req.query.empId;
        const standId = req.query.standId;
        const ownerId = req.query.ownerId;
        let date = moment().subtract(1, 'days').toDate();
        const sessionResult = await Sessions.find({employeeId : empId}).sort({_id:-1}).limit(1);
        
        if(sessionResult.length){
            date = new Date(sessionResult[0].start_time)
        }
        const query = helper.getVehiclesByEmployeeStandQuery(empId, standId, ownerId, date);
        const cardsCountquery = helper.getCardsByStandQuery(ownerId, standId, date);
        const olineCardsCountquery = helper.getOnlineCardsByStandQuery(ownerId, standId, date);
        if (_.isEmpty(empId) || _.isEmpty(ownerId) || _.isEmpty(standId)) {
            return res.json({ status: "fail", message: "Enter required parameters" });
        }
        const parkingsResult = await Parkings.aggregate(query).exec();
        const cardsResult = await Cards.aggregate(cardsCountquery).exec();
        const onlineCards = await Cards.aggregate(olineCardsCountquery).exec();
        let dashboardResult = [{
            type: "bicycle",
            collection: 0,
            offlineCards: cardsResult.length && cardsResult[0].bicycleCount ? cardsResult[0].bicycleCount : 0,
            onlineCards: onlineCards.length && onlineCards[0].bicycleCount ? onlineCards[0].bicycleCount : 0,
            total: parkingsResult.length && parkingsResult[0].bicycle ? parkingsResult[0].bicycle : 0,
            pending: parkingsResult.length && parkingsResult[0].bicyclePending ? parkingsResult[0].bicyclePending : 0,
            pendingOnLogout: parkingsResult.length && parkingsResult[0].bicyclePendingOnLogout ? parkingsResult[0].bicyclePendingOnLogout : 0,
            cancelled: parkingsResult.length && parkingsResult[0].bicycleCancelled ? parkingsResult[0].bicycleCancelled : 0,
            exit: parkingsResult.length && parkingsResult[0].bicycleExit ? parkingsResult[0].bicycleExit : 0,
        },{
            type: "twoWheeler",
            collection: 0,
            offlineCards: cardsResult.length && cardsResult[0].twoWheelerCount ? cardsResult[0].twoWheelerCount : 0,
            onlineCards: onlineCards.length && onlineCards[0].twoWheelerCount ? onlineCards[0].twoWheelerCount : 0,
            cancelled: parkingsResult.length && parkingsResult[0].twoWheelerCancelled ? parkingsResult[0].twoWheelerCancelled : 0,
            pending: parkingsResult.length && parkingsResult[0].twoWheelerPending ? parkingsResult[0].twoWheelerPending : 0,
            pendingOnLogout: parkingsResult.length && parkingsResult[0].twoWheelerPendingOnLogout ? parkingsResult[0].twoWheelerPendingOnLogout : 0,
            total: parkingsResult.length && parkingsResult[0].twoWheeler ? parkingsResult[0].twoWheeler : 0,
            exit: parkingsResult.length && parkingsResult[0].twoWheelerExit ? parkingsResult[0].twoWheelerExit : 0,
        },{
            type: "threeWheeler",
            collection: 0,
            onlineCards: onlineCards.length && onlineCards[0].threeWheelerCount ? onlineCards[0].threeWheelerCount : 0,
            offlineCards: cardsResult.length && cardsResult[0].threeWheelerCount ? cardsResult[0].threeWheelerCount : 0,
            cancelled: parkingsResult.length && parkingsResult[0].threeWheelerCancelled ? parkingsResult[0].threeWheelerCancelled : 0,
            pending: parkingsResult.length && parkingsResult[0].threeWheelerPending ? parkingsResult[0].threeWheelerPending : 0,
            pendingOnLogout: parkingsResult.length && parkingsResult[0].threeWheelerPendingOnLogout ? parkingsResult[0].threeWheelerPendingOnLogout : 0,
            total: parkingsResult.length && parkingsResult[0].threeWheeler ? parkingsResult[0].threeWheeler : 0,
            exit: parkingsResult.length && parkingsResult[0].threeWheelerExit ? parkingsResult[0].threeWheelerExit : 0,
        },{
            type: "fourWheeler",
            collection: 0,
            onlineCards: onlineCards.length && onlineCards[0].fourWheelerCount ? onlineCards[0].fourWheelerCount : 0,
            offlineCards: cardsResult.length && cardsResult[0].fourWheelerCount ? cardsResult[0].fourWheelerCount : 0,
            cancelled: parkingsResult.length && parkingsResult[0].fourWheelerCancelled ? parkingsResult[0].fourWheelerCancelled : 0,
            pending: parkingsResult.length && parkingsResult[0].fourWheelerPending ? parkingsResult[0].fourWheelerPending : 0,
            pendingOnLogout: parkingsResult.length && parkingsResult[0].fourWheelerPendingOnLogout ? parkingsResult[0].fourWheelerPendingOnLogout : 0,
            total: parkingsResult.length && parkingsResult[0].fourWheeler ? parkingsResult[0].fourWheeler : 0,
            exit: parkingsResult.length && parkingsResult[0].fourWheelerExit ? parkingsResult[0].fourWheelerExit : 0,
        }];
        const vehcileTypesData = await PriceRules.find({standId: standId}).exec();
        if(vehcileTypesData.length) {
            if(vehcileTypesData[0].vehicleTypes.length > 4) {
                const vehicleTypes = vehcileTypesData[0].vehicleTypes;
                const extraCount = await getExtraVehiclesCount(ownerId,vehicleTypes, standId, date);
                dashboardResult.push(...extraCount);
            }
        }
        const defaultVehicles = ['bicycle', 'twoWheeler', 'threeWheeler', 'fourWheeler'];
        const vehiclesData = vehcileTypesData.map(item => item.vehicleTypes);
        const types = vehiclesData && vehiclesData.length ? vehiclesData[0].map(item => item.vehicletype) : defaultVehicles;
        for(let i=0;i<types.length;i++){
            let data = {
                ownerId: ownerId,
                standId: standId,
                employeeId:empId,
                date: date,
                vehicleType: types[i]
            }
            const collectionCountquery = await getTotalCollectionByVehicleQuery(data);
            const offlineCardCollectionCountquery = await getTotalCollectionByCardQuery(data);
            const totalCollectionByVehicle = await Parkings.aggregate(collectionCountquery).exec();
            const totalCollectionByOfflineCard = await Cards.aggregate(offlineCardCollectionCountquery).exec();
            const parkingsAmount = totalCollectionByVehicle.length ? totalCollectionByVehicle[0].SUM : 0;
            const cardsAmount = totalCollectionByOfflineCard.length ? totalCollectionByOfflineCard[0].SUM : 0;
            let selectedVehicle = dashboardResult.filter(item => item.type ==  types[i])[0];
            selectedVehicle.collection = parkingsAmount;
            selectedVehicle.cardsAmount = cardsAmount;
        }

        return res.json({ status: "success", message: "parkings Data", data: dashboardResult });
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

// function for getting extra vehicles andoffline  cards count --> owner dashbaord
async function getExtraVehiclesCount(ownerId, vehicleTypes, standId, date) {
    let dashboardResult = [];
    const defaultVehicles = ['bicycle', 'twoWheeler', 'threeWheeler', 'fourWheeler'];
    for (let i=0;i<vehicleTypes.length;i++) {
        const element = vehicleTypes[i];
        let offlineCards = null;
        if (!defaultVehicles.includes(element.vehicletype)) {
            let parkingsResult = [];
            if(standId){
                parkingsResult = await Parkings.aggregate(extraVehiclecountBystandQuery(ownerId,element,standId,date)).exec();
                offlineCards = await Cards.countDocuments({
                    $and: [{
                        vehicleType: element.vehicletype
                    }, {
                        ownerId: ownerId
                    }, {
                        type: 'offline'
                    },{
                        standId: standId
                    }]
                    }).exec();
            } else {
                parkingsResult = await Parkings.aggregate(extraVehiclecountsQuery(ownerId,element,date)).exec();
            }
            dashboardResult.push({
                type: element.vehicletype,
                collection: 0,
                offlineCards: offlineCards,
                onlineCards: 0,
                total: parkingsResult.length && parkingsResult[0].total ? parkingsResult[0].total : 0,
                cancelled: parkingsResult.length && parkingsResult[0].cancelled ? parkingsResult[0].cancelled : 0,
                pending: parkingsResult.length && parkingsResult[0].pending ? parkingsResult[0].pending : 0,
                pendingOnLogout: parkingsResult.length && parkingsResult[0].pendingOnLogout ? parkingsResult[0].pendingOnLogout : 0,
                exit: parkingsResult.length && parkingsResult[0].exit ? parkingsResult[0].exit : 0
            })
        }
    }
    return dashboardResult;
}

//queries 
function extraVehiclecountsQuery(ownerId,element,date){
    return [{
		"$facet": {
			"total": [{
				$match: {
					$and: [{
                        vehicleType: element.vehicletype
                    }, {
                        ownerId: ownerId
                    },{
						"createdDate": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "total"
			}],
			"cancelled": [{
				$match: {
					$and: [{
                        vehicleType: element.vehicletype
                    }, {
                        ownerId: ownerId
                    }, {
                        status: 'Cancelled'
                    },{
						"createdDate": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "cancelled"
			}],
            "pending": [{
                $match: {
                    $and: [{
                        vehicleType: element.vehicletype
                    }, {
                        ownerId: ownerId
                    },{
                        status: 'Entry'
                    },{
                        "createdDate": 
                        {
                            $lt: date
                        }
                    }]
                }
            }, {
                $count: "pending"
            }],
            "pendingOnLogout": [{
                $match: {
                    $and: [{
                        vehicleType: element.vehicletype
                    }, {
                        ownerId: ownerId
                    },{
                        status: 'Entry'
                    }]
                }
            }, {
                $count: "pendingOnLogout"
            }],
			"exit": [{
				$match: {
					$and: [{
                        vehicleType: element.vehicletype
                    }, {
                        ownerId: ownerId
                    }, {
                        status: 'Exit'
                    },{
						"createdDate": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "exit"
			}]
		}
	},
	{
		"$project": {
			"total": {
				"$arrayElemAt": [
					"$total.total",
					0
				]
			},
			"cancelled": {
				"$arrayElemAt": [
					"$cancelled.cancelled",
					0
				]
			},
			"exit": {
				"$arrayElemAt": [
					"$exit.exit",
					0
				]
			},
            "pending": {
                "$arrayElemAt": [
                    "$pending.pending",
                    0
                ]
            },
            "pendingOnLogout": {
                "$arrayElemAt": [
                    "$pendingOnLogout.pendingOnLogout",
                    0
                ]
            }
		}
	}
	]
}

function extraVehiclecountBystandQuery(ownerId,element, standId, date){
    return [{
		"$facet": {
			"total": [{
				$match: {
					$and: [{
                        vehicleType: element.vehicletype
                    }, {
                        ownerId: ownerId
                    }, {
                        standId: standId
                    },{
						"createdDate": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "total"
			}],
			"cancelled": [{
				$match: {
					$and: [{
                        vehicleType: element.vehicletype
                    }, {
                        ownerId: ownerId
                    }, {
                        status: 'Cancelled'
                    }, {
                        standId: standId
                    },{
						"createdDate": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "cancelled"
			}],
            "pending": [{
                $match: {
                    $and: [{
                        vehicleType: element.vehicletype
                    }, {
                        ownerId: ownerId
                    },{
                        status: 'Entry'
                    }, {
                        standId: standId
                    },{
                        "createdDate": 
                        {
                            $lt: date
                        }
                    }]
                }
            }, {
                $count: "pending"
            }],
            "pendingOnLogout": [{
                $match: {
                    $and: [{
                        vehicleType: element.vehicletype
                    }, {
                        ownerId: ownerId
                    },{
                        status: 'Entry'
                    }, {
                        standId: standId
                    }]
                }
            }, {
                $count: "pendingOnLogout"
            }],
			"exit": [{
				$match: {
					$and: [{
                        vehicleType: element.vehicletype
                    }, {
                        ownerId: ownerId
                    }, {
                        status: 'Exit'
                    }, {
                        standId: standId
                    },{
						"dateAndTimeOfExit": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "exit"
			}]
		}
	},
	{
		"$project": {
			"total": {
				"$arrayElemAt": [
					"$total.total",
					0
				]
			},
			"cancelled": {
				"$arrayElemAt": [
					"$cancelled.cancelled",
					0
				]
			},
			"exit": {
				"$arrayElemAt": [
					"$exit.exit",
					0
				]
			},
            "pending": {
                "$arrayElemAt": [
                    "$pending.pending",
                    0
                ]
            },
            "pendingOnLogout": {
                "$arrayElemAt": [
                    "$pendingOnLogout.pendingOnLogout",
                    0
                ]
            }
		}
	}
	]
}

getTotalCollectionByVehicleQuery = data => {
    const { date, employeeId, ownerId, standId, vehicleType } = data;
    return [{
        $match: {
            $and: [{
                dateAndTimeOfExit: {
                $gt: date
                }
            },{
                ownerId: ownerId
            },{
                standId: standId
            },{
                closedEmpId: employeeId
            },{
                vehicleType: vehicleType
            }]
        }
     }, {
  $group: {
     _id: null,
     SUM: {
        $sum: "$finalPrice"
     },
     COUNT: {
        $sum: 1
     }
  }
  }]
}
getTotalCollectionByCardQuery = data => {
    const { date, employeeId, ownerId, standId, vehicleType } = data;
    return [{
        $match: {
            $and: [{ 
                    $or:[{
                        "createdDate": 
                        {
                            $gte: date
                        }
                    },
                    {
                        "renewedDate": 
                        {
                            $gte: date
                        }
                    }
                ]
            },{
                ownerId: ownerId
            },{
                standId: standId
            },{
                employeeId: employeeId
            },{
                vehicleType: vehicleType
            }]
        }
     }, {
  $group: {
     _id: null,
     SUM: {
        $sum: "$amount"
     },
     COUNT: {
        $sum: 1
     }
  }
  }]
}
getTotalCollectionByVehicleByOwnerQuery = data => {
    const { date, ownerId, vehicleType } = data;
    return [{
        $match: {
            $and: [{
                dateAndTimeOfExit: {
                $gte: date
                }
            },{
                ownerId: ownerId
            },{
                vehicleType: vehicleType
            },{
                "createdDate": 
                {
                    $gte: date
                }
            }]
        }
     }, {
  $group: {
     _id: null,
     SUM: {
        $sum: "$finalPrice"
     },
     COUNT: {
        $sum: 1
     }
  }
  }]
}
getTotalCollectionByVehicleByStandQuery = data => {
    const { date, ownerId, vehicleType, standId } = data;
    return [{
        $match: {
            $and: [{
                dateAndTimeOfExit: {
                $gte: date
                }
            },{
                ownerId: ownerId
            },{
                standId: standId
            },{
                vehicleType: vehicleType
            }]
        }
     }, {
  $group: {
     _id: null,
     SUM: {
        $sum: "$finalPrice"
     },
     COUNT: {
        $sum: 1
     }
  }
  }]
}