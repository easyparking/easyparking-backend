exports.getAnalytics = async (req, res) => {
    try {
        const { startTime, endTime, type, ownerId, standId } = req.body;
        if(!type) {
            throw new ApplicationError("missing type parameter", 500, "");
        }
        let query = {};
        if(type =='Entry'){
            query = {dateAndTimeOfParking: {
                $gte: new Date(startTime),
                $lt: new Date(endTime)
            }, status: 'Entry', standId, ownerId}
        } else if(type == 'Exit'){
            query = {dateAndTimeOfExit: {
                $gte: new Date(startTime),
                $lt: new Date(endTime)
            }, $or: [{status: 'Exit'}, {status: 'Cancelled'}] , standId, ownerId }
        } else {
            query = {$or:[{dateAndTimeOfParking: {
                $gte: new Date(startTime),
                $lt: new Date(endTime)
            }}, {dateAndTimeOfExit: {
                $gte: new Date(startTime),
                $lt: new Date(endTime)
            }}], standId, ownerId}
        }
        const result = await Parkings.find(query).select('dateAndTimeOfParking dateAndTimeOfExit createdDate status parkingId').exec();
        res.json({result: result, status: 'success'})
    } catch(error){
        res.json({error: error, status: 'fail'})
    }
}