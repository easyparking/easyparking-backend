exports.getEmployees = async (req, res) => {

    var ownerId = req.body.ownerId

    try {
        if (_.isEmpty(ownerId)) {
            throw new ApplicationError("Owner Id is required parameter", 500, "");
        }
        var usersData = await Users.find({ ownerId: ownerId, role: "employee" }, { auth: 0 }).exec();

        if (!_.isEmpty(usersData)) {
            return res.json({ status: 'Sucess', data: usersData })
        } else {
            throw new ApplicationError("Employees data not found", 500, "");

        }
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

exports.getEmployeesByStand = async (req, res) => {
    var ownerId = req.params.ownerId;
    try {
        if (_.isEmpty(ownerId)) {
            throw new ApplicationError("owner Id is required parameter", 500, "");
        }
        var usersData = await Users.find({$and: [{ ownerId: ownerId, role: "employee" }]}, { auth: 0 }).exec();
        if (!_.isEmpty(usersData)) {
            return res.json({ status: 'Success', data: usersData });
        } else {
            throw new ApplicationError("Employees data not found", 500, "");
        }
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

exports.updateEmployee = async function (req, res) {
    var new_password = req.body.password;
    const {name, userName, phoneNumber, vehicleNumber, id} = req.body;
    try {
        if (_.isEmpty(userName) || _.isEmpty(id)) {
            return res.json({ status: "fail", message: "Enter required parameters" });
        }
        var user = await Users.findOne({ "_id": id, status: "Active" }).exec();
        if (_.isEmpty(user)) {
            throw new ApplicationError("User data not found", 500, "");
        } else if (userName != user.userName) {
            var user_data = await Users.findOne({ "userName": userName }).exec()
            if (!_.isEmpty(user_data)) {
                throw new ApplicationError("User Name already registered", 500, "");
            }
        }
        if (new_password && new_password != "") {
            var saltKey = Math.round((new Date().valueOf() * Math.random())) + '';
            var password = crypto.createHmac('sha512', saltKey).update(new_password).digest('hex');
            var result = await Users.findOneAndUpdate({ "_id": id }, { $set: { 'auth.password': password, 'auth.salt': saltKey, name, userName, phoneNumber, vehicleNumber  }},{new: true}).exec();     
        } else {
            var result = await Users.findOneAndUpdate({ "_id": id }, { $set: { name, userName, phoneNumber, vehicleNumber } },{new: true}).exec();
        }
        
        return res.json({ status: "success", message: " Employee updated successfully!..", result });
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
};

exports.updateEmployeeStatus = async function (req, res) {
    try {
        const id = mongoose.mongo.ObjectId(req.params.id);
        if (_.isEmpty(id)) {
            return res.json({ status: "fail", message: "Enter required parameters" });
        }
        const employeeData = await Users.find({ _id: id }).exec();
        if (_.isEmpty(employeeData)) {
            throw new ApplicationError("No record found", 500, "");
        }
        const result = await Users.update({ "_id": id }, { $set: { status: req.body.status } }).exec();
        return res.json({ status: "success", message: "Employee status updated successfully!.." });

    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
};

exports.deleteEmployee = async function (req, res) {
    const id = mongoose.mongo.ObjectId(req.params.id);
    const employeeCount = req.query.employeeCount;
    const standId = req.query.standId;
    try {
        const employeeData = await Users.find({ _id: id }).exec();
        if (_.isEmpty(employeeData)) {
            throw new ApplicationError("No record found to delete", 500, "");
        }
        var result = await Users.remove({ _id: id }).exec();
        const updateStand= await Stands.update({standId: standId},{$set: {employeeCount: employeeCount}});

        if (!_.isEmpty(result)) {
            return res.json({ status: "success", message: "Employee Deleted successfully!.." });
        } else {
            throw new ApplicationError("Unable to delete the record", 500, "");
        }
    } catch (error) {
        return res.json({ status: "fail", error: error })
    }
}
