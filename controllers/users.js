exports.verifyUserName = async function (req, res) {
    var userName = req.params.userName;
    if (_.isEmpty(userName)) {
        return res.json({ status: "fail", message: "Enter required parameters" });
    }
    try {
        var result = await Users.findOne({ "userName": userName }).exec();
        if (!_.isEmpty(result) && result._id) {
            throw new ApplicationError("User name already existed", 500, "");
        } else {
            return res.json({ status: "success", message: "User name Avialable" });
        }
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
};

exports.createUser = async function (req, res) {
    var userData = req.body;
    var saltKey = Math.round((new Date().valueOf() * Math.random())) + '';
    var password = crypto.createHmac('sha512', saltKey).update(userData.password).digest('hex');
    const employeeCount = req.body.employeeCount;
    // userData.password = password;
    userData.status = "Active";
    userData.loginStatus = false;
    userData.auth = {
        salt: saltKey,
        default: userData.password,
        auth_type: "",
        auth_id: "",
        password: password
    };
    try {
        var user = await Users.findOne({$or:[{ "userName": userData.userName },{phoneNumber: userData.phoneNumber}]}).exec();
        
        if (!_.isEmpty(user)) {
            return res.json({ status: "fail", message: "User or phonenumber already registered" });
        }

        var user_details = await Users.countDocuments({ role: userData.role }).sort({ _id: -1 }).exec();

        if (userData.role == "owner") {
            if (user_details == 0) {
                userData.ownerId = await helper.getUserId(0, userData.role);
            } else {
                userData.ownerId = await helper.getUserId(user_details, userData.role);
            }
        } else if (userData.role == "employee") {
            if (_.isEmpty(userData.standId)) {
                throw new ApplicationError("stand ID is required parameter", 500, "");
            }
            if (_.isEmpty(userData.ownerId)) {
                throw new ApplicationError("Owner ID is required parameter", 500, "");
            } else {
                if (user_details == 0) {
                    userData.employeeId = await helper.getUserId(0, userData.role);
                } else {
                    userData.employeeId = await helper.getUserId(user_details, userData.role);
                }
                userData.ownerId = userData.ownerId
            }
        } else if (userData.role == "customer") {
            //console.log("nothing");
        if (user_details == 0) {
            userData.customerId = await helper.getUserId(0, userData.role);
        } else {
            userData.customerId = await helper.getUserId(user_details, userData.role);
        }
        }
        const result = await Users.create(userData);
        if (!_.isEmpty(result)) {
            if (userData.role == "employee") {
                const updateStand= await Stands.update({standId: userData.standId},{$set: {employeeCount: employeeCount}});
            }
            return res.json({ status: "success", message: "User created successfully" });
        } else {
            throw new ApplicationError("User not created", 500, "");
        }
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

exports.resetPassword = async function (req, res) {
    const currentPassword = req.body.currentPassword;
    const newPassword = req.body.newPassword;
    const userName = req.body.userName;

    const saltKey = Math.round((new Date().valueOf() * Math.random())) + '';
    if (_.isEmpty(newPassword) || _.isEmpty(currentPassword)) {
        return res.json({ status: "fail", message: "Enter required parameters" });
    }
    try {
        var user = await Users.findOne({ "userName": userName }).exec();
        if (_.isEmpty(user)) {
            throw new ApplicationError("User data not found !", 500, "");
        } else if(user && user.status == 'Inactive') {
            throw new ApplicationError("Your account is inactive. Please contact your administrator", 500, "");
        }
        var password = crypto.createHmac('sha512', user.auth.salt).update(currentPassword).digest('hex');

        if (!(user.auth.password === password)) {
            throw new ApplicationError("current password incorrect", 500, "");
        } else {
            const newPass = crypto.createHmac('sha512', saltKey).update(newPassword).digest('hex');
            let auth = {
                salt: saltKey,
                raw: Buffer.from(newPassword).toString('base64'),
                auth_type: "",
                auth_id: "",
                password: newPass
            };
            const result = await Users.update({ _id: user._id }, { $set: { auth } }).exec();
            return res.json({ status: "success", message: "Password updated succesfully.",result });
        }
    } catch (e) {
        return res.json({ status: "fail", message: e });
    }
}

exports.userSignIn = async function (req, res) {
    var userName = req.body.userName;
    var password = req.body.password;
    // var saltKey = Math.round((new Date().valueOf() * Math.random())) + '';
    if (_.isEmpty(userName) || _.isEmpty(password)) {
        return res.json({ status: "fail", message: "Enter required parameters" });
    }
    try {
        const accessToken = await Authorization.generateAccessToken({ userName: userName });
        const refreshToken = await Authorization.generateRefreshToken({ userName: userName });

        if (_.isEmpty(accessToken) || _.isEmpty(refreshToken)) {
            throw new ApplicationError("Tokens not created", 500, "");
        }
        var user = await Users.findOne({ "userName": userName }).exec();
        const hasSessions = await Sessions.find({"userName": userName, end_time: null}).exec();
        if (_.isEmpty(user)) {
            throw new ApplicationError("User data not found !", 500, "");
        } else if(user && user.status == 'Inactive') {
            throw new ApplicationError("Your account is inactive. Please contact your administrator", 500, "");
        }
        const encoded_pass = crypto.createHmac('sha512', user.auth.salt).update(password).digest('hex');
        if (!(user.auth.password === encoded_pass)) {
            throw new ApplicationError("user name /Password incorrect", 500, "");
        }
         else if ((hasSessions && hasSessions.length > 0 && user.loginStatus == true) && user.role === 'employee') {
            throw new ApplicationError("Another session is running", 500, "");
        } 
        else {
            const result = await Users.update({ _id: user._id }, { $set: { loginStatus: true, accessToken, refreshToken } }).exec();
            res.cookie("jwt", accessToken, {secure: true, httpOnly: true})
            return res.json({ status: "success", message: "signin successfully!..", data: user, accessToken });
        }
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

exports.getUserData = async function (req, res) {
    var id = req.params.id;
    if (_.isEmpty(id)) {
        return res.json({ status: "fail", message: "Enter required parameters" });
    }
    try {
        var user = await Users.findOne({ "_id": id }).exec();
        if (!_.isEmpty(user)) {
            return res.json({ status: "success", message: "User Data", data: user });
        } else {
            throw new ApplicationError("No user record found", 500, "");
        }
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

exports.getAllUsers = async function (req, res) {
    var size = parseInt(req.body.size);
    var pageNo = parseInt(req.body.pageNo);
    var count = parseInt(req.body.count);
    var type = req.body.type
    if (pageNo < 0 || pageNo === 0) {
        return res.json({ status: "fail", message: "invalid page number" });
    }
    var skip = size * (pageNo - 1);
    var limit = size;
    try {
        if (count == 0) {
            var totalCount = await Users.countDocuments({ "status": "Active" }).exec();
            if (totalCount < 0) {
                throw new ApplicationError("No users found", 500, "");
            }
            var result = await Users.find({ "status": "Active" }, { auth: 0 }).skip(skip).limit(limit).sort({ 'createdDate': -1 }).exec();
            if (!_.isEmpty(result)) {
                return res.json({ status: "success", message: "user Data", "data": result, count: totalCount });
            } else {
                throw new ApplicationError("No users found", 500, "");
            }
        } else {
            var result = await Users.find({ "status": "Active" }, { auth: 0 }).skip(skip).limit(limit).sort({ 'createdDate': -1 }).exec();
            if (!_.isEmpty(result)) {
                return res.json({ status: "success", message: "user Data", "data": result });
            } else {
                throw new ApplicationError("No users found", 500, "");
            }
        }
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

exports.searchUser = async function (req, res) {
    var search_text = req.body.userName;
    if (_.isEmpty(search_text)) {
        return res.json({ status: "fail", message: "Enter required parameters" });
    }
    try {

        var result = await Users.find({ $or: [{ 'userName': { $regex:  search_text, $options: 'i' } }, { 'ownerId': { $regex:  search_text, $options: 'i' } }] }).sort({ '_id': 1 }).exec();

        // var result = await Users.find({'userName': { $regex:  search_text, $options: 'i' } }).sort({ 'userName': 1 }).exec();
        if (_.isEmpty(result)) {
            throw new ApplicationError("No users found", 500, "");
        } else {
            return res.json({ status: "success", message: "User data", data: result });
        }
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

exports.deleteUser = async function (req, res) {
    var id = req.params.id;
    var ownerId = req.query.ownerId;
    if (_.isEmpty(id)) {
        return res.json({ status: "fail", message: "Enter required parameters" });
    }
    if (_.isEmpty(ownerId)) {
        return res.json({ status: "fail", message: "ownerId required" });
    }
    try {
        var user = await Users.find({ _id: id }).exec();
        if (_.isEmpty(user)) {
            throw new ApplicationError("No record found to delete", 500, "");
        }
        if(req.query.role === "owner"){
            const standsResult= await Stands.deleteMany({ ownerId: ownerId }).exec();
            const UsersResult= await Users.deleteMany({ ownerId: ownerId }).exec();
            const PriceRulResult= await PriceRules.deleteMany({ ownerId: ownerId }).exec();
            const ParkingsResult= await Parkings.deleteMany({ ownerId: ownerId }).exec();
            const CardsResult= await Cards.deleteMany({ ownerId: ownerId }).exec();
        }
        var result = await Users.remove({ _id: id }).exec();
        if (!_.isEmpty(result)) {
            return res.json({ status: "success", message: "User Deleted successfully!.." });
        } else {
            throw new ApplicationError("Unable to delete the record found", 500, "");
        }
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

exports.updateUser = async function (req, res) {
    var new_password = req.body.password;
    const {name, userName, phoneNumber} = req.body;
    var id = mongoose.mongo.ObjectId(req.params.id);
    if (_.isEmpty(userName) || _.isEmpty(id) || _.isEmpty(new_password)) {
        return res.json({ status: "fail", message: "Enter required parameters" });
    }
    try {
        var user = await Users.findOne({ "_id": id, status: "Active" }).exec();
        if (_.isEmpty(user)) {
            throw new ApplicationError("User data not found", 500, "");
        } else if (userName != user.userName) {
            var user_data = await Users.findOne({ "userName": userName }).exec()
            if (!_.isEmpty(user_data)) {
                throw new ApplicationError("User Name already registered", 500, "");
            }
        }
        if (new_password != "") {
            var saltKey = Math.round((new Date().valueOf() * Math.random())) + '';
            var password = crypto.createHmac('sha512', saltKey).update(new_password).digest('hex');
            var result = await Users.update({ "_id": id }, { $set: { 'auth.password': password, 'auth.salt': saltKey, name, userName, phoneNumber  } }).exec();     
        } else {
            var result = await Users.update({ "_id": id }, { $set: { name, userName, phoneNumber } }).exec();
        }
        return res.json({ status: "success", message: " User data updated successfully!.." });

    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
};

exports.updateLoginStatus = async function (req, res) {
    var userData = req.body;
    // userData.loginStatus = userData.loginStatus;
    // userData.userName = userData.userName;
    // userData.role = userData.role

    if (_.isEmpty(userData.userName)) {
        return res.json({ status: "fail", message: "Enter required parameters" });
    }
    try {
        var user = await Users.findOne({ "userName": userData.userName }).exec();
        if (_.isEmpty(user)) {
            throw new ApplicationError("No user found", 500, "");
        } else {
            if (user.loginStatus) {
                var result = await Users.update({ _id: user._id }, { $set: { loginStatus: false } }).exec();
                return res.json({ status: "success", message: "Login status updated successfully!.." });
            } else {
                throw new ApplicationError("User has already Logged out", 500, "");
            }
        }
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

exports.refresh = async function (req, res) {
    let accessToken = req.body.jwt;
    if (!accessToken) {
        return res.json({ status: "fail" });
    }
    var payload;
    try {
        payload = jwt.verify(accessToken, process.env.ACCESS_TOKEN_SECRET)
    }
    catch (e) {
        return res.json({ status: "fail;", err: e });
    }
    var users = await Users.findOne({ userName: payload.userName }).exec();
    let refreshToken = users._doc.refreshToken;

    try {
        jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET)
    }
    catch (e) {
        return res.json({ status: "faill", err: e });
    }
    let newToken = jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET,
        {
            algorithm: "HS256"
            // expiresIn: process.env.ACCESS_TOKEN_LIFE
        })
    return res.json({ status: "success", newTokennewToken: newToken });
    //res.cookie("jwt", newToken, {secure: true, httpOnly: true})
    //res.send()
}

exports.getOwnerNames = async function(req, res){
    
    try {
        var result = await Users.find({role: 'owner'},{name : 1})
        if(_.isEmpty(result)){
            return res.json({ status: "fail", message: "No data found" });
        }
       return res.json({ status: "success", data: result });

    } catch (error) {
       return  res.json({status: "fail", error : error})
    }
}

exports.getEmpListCount = async function(stands){
    const getEmployeeList = await Users.countDocuments({standId: stands.standId}).exec();
    return {...stands,getEmployeeList}
}

