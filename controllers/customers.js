const axios = require('axios');
const { SIGNIN_data }  = require('../config/constants');
const config = require('../config/config');

exports.sendOTP = async function(req, res) {
    try{
        const phoneNumber = req.params.phoneNumber;
        const otp = Math.floor(100000 + Math.random() * 900000);
        // const schedule_time = moment().add(3, 'minutes').unix();  &schedule_time='+schedule_time+'

        const message = SIGNIN_data(otp);
        const url = 'https://api.textlocal.in/send/?apikey='+config.textLocalAPIkey+'&numbers='+phoneNumber+'&sender=STSPAR&message=' + encodeURIComponent(message);
        let response;
        await axios
        .get(url)
        .then(function (resp) {
            response = resp.data
        })
        .catch(function (error) {
        response = error;
        throw new ApplicationError(error, 500, "");
        });
        return res.json({ status: "success", message: response, otp });
    }
     catch(error){
        return res.json({ status: "fail", error });
    }
}

exports.getMonthlyCards = async (req, res)=>{
    try{
        const customerName = req.params.userName;
        const data = await Cards.find({customerName}).exec();
        res.json({'status': 'success', data})
    } catch(e) {
        return res.json({ 'status': 'fail', message: error.message })
    }
}