const SMS = require('./SMS');
exports.addWantedVehicle = async(req, res) => {
    try {
        let requestData = req.body;
        if (!requestData.vehicleNumber) {
            throw new ApplicationError("vehiclesNumber should not be empty", 500, "");
        }
        const isvehiclesNumberExist = await WantedVehicles.findOne({ vehiclesNumber: requestData.vehicleNumber }).exec();
        if (!_.isEmpty(isvehiclesNumberExist)) {
            throw new ApplicationError("vehiclesNumber already registered", 500, "");
        }
        const result = await WantedVehicles.create(requestData);
        return res.json({ status: "Success", message: "Details submitted successfully", data: result });
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

exports.getWantedVehiclesList = async (req, res) => {
    const search_text = req.query.searchText;
    const size = parseInt(req.query.size);
    const pageNo = parseInt(req.query.pageNo);
    if (pageNo < 0 || pageNo === 0) {
        return res.json({ status: "fail", message: "invalid page number" });
    }
    const skip = size * (pageNo - 1);
    const limit = size;
    try {
        let query = {};
        if (search_text) {
            query = {
                $or: [
                    { 'vehicleNumber': { $regex:  search_text, $options: 'i' } },
                    { 'type': { $regex:  search_text, $options: 'i' } },
                    { 'phoneNumber': { $regex:  search_text, $options: 'i' } },
                    { 'insuranceValidty': { $regex:  search_text, $options: 'i' } },
                    { 'missingDate': { $regex:  search_text, $options: 'i' } },
                    { 'requestBy': { $regex:  search_text, $options: 'i' } },
                    { 'requestDate': { $regex:  search_text, $options: 'i' } },
                    { 'policeComplaintMade': { $regex:  search_text, $options: 'i' } },
                    { 'remarks': { $regex:  search_text, $options: 'i' } },
                    { 'policeComplaintMade': { $regex:  search_text, $options: 'i' } },
                    // { 'createdDate': { $regex:  search_text, $options: 'i' } }
                ]
            }
        }
            let totalCount = await WantedVehicles.countDocuments(query).exec();
            if (totalCount < 0) {
                throw new ApplicationError("No wanted vehicles data found", 500, "");
            }
            const result = await WantedVehicles.find(query).skip(skip).limit(limit).sort({ '_id': -1 }).exec();
                return res.json({ status: "success", message: "Wanted vehicles ", data: result, totalCount: totalCount });
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

exports.deleteWantedVehicle = async (req, res) => {
    var id = req.params.id
    try {
        if (_.isEmpty(id)) {
            throw new ApplicationError("Id is required parameter", 500, "");
        }
        var data = await WantedVehicles.remove({ _id: id }).exec();

        if (data.deletedCount == 0) {
            throw new ApplicationError("No Data Found", 404, "");
        }
        return res.json({ status: "success", "message": "Record is successfully deleted", data: data })

    } catch (error) {
        return res.json({ status: "fail", error: error })
    }
}

exports.updateWantedVehicle = async (req, res) => {
    var id = req.params.id;
    let requestData = req.body;
    const matchedDate = moment().format();
    requestData.matchedDate = matchedDate;
    let locatedAt = `${requestData.standName}, ${requestData.location}`;
    try {
        if (_.isEmpty(id)) {
            throw new ApplicationError("Id is required parameter", 500, "");
        }
        var result = await WantedVehicles.update({ _id: id },{$set: {isMatched: true, matchedDate, locatedAt}}).exec();
        var adminData = await Users.findOne({role: 'admin'}).exec();       
        const sendSMS = await SMS.sendSMS('matched_vechile',requestData, adminData.phoneNumber);
        if(result.nModified > 0){
            return res.json({ status: "success", "message": "Record updated succesfully",sendSMS})
        } else {
            return res.json({ status: "fail", "message": "No record found with id" })
        }
    } catch (error) {
        return res.json({ status: "fail", error: error })
    }
}

exports.checkWantedVehicles = async (req, res) => {
    try {
        const vehicleNumber = req.params.vehicleNumber
        const result = await WantedVehicles.findOne({vehicleNumber: vehicleNumber}).exec();
        if (_.isEmpty(result)) {
            return res.json({ status: "success", message: "data retrived succesfully", isWantedVehicle: true});
        } else {
            return res.json({ status: "success", message: "data retrived succesfully", isWantedVehicle: true});
        }
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

