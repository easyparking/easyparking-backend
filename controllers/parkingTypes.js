exports.addParkingType = async function (req, res) {
    try {
    const type = req.body.type;
        if (_.isEmpty(type)) {
            throw new ApplicationError("Missing required parameter", 500, "");
        }
        const plans = await ParkingTypes.findOne({type}).exec();
        if (!_.isEmpty(plans)) {
            return res.json({ status: 'fail', message: plans + ' is already exists' })
        }
        const result = await ParkingTypes.create({type})
        return res.json({ status: 'Success', message: 'Parking type added successfully' })
    } catch (e) {
        return res.json({ status: 'fail', message: e.message })
    }
}

exports.getAllParkingTypes = async function (req, res) {
    try {
        const plans = await ParkingTypes.find({}).exec();
        return res.json({ status: 'Success', plans })
    } catch (e) {
        return res.json({ status: 'fail', message: e.message })
    }
}

exports.getParkingTypesList = async function (req, res) {
    try {
    const size = parseInt(req.query.size);
    const pageNo = parseInt(req.query.pageNo);
    const search_text = req.query.searchText;
    const skip = size * (pageNo - 1);
    const limit = size;
    let totalCount = 0;
    let query = {};

    if (pageNo < 0 || pageNo === 0) {
        return res.json({ status: "fail", message: "invalid page number" });
    }

    if (search_text) {
        query = {
            $or: [
                { 'type': { $regex:  search_text, $options: 'i' } },
            ]
        }
    }
        totalCount = await ParkingTypes.countDocuments(query).sort({ '_id': 1 }).exec();
        if (totalCount < 0) {
            throw new ApplicationError("Parking type data not found", 500, "");
        }
        var result = await ParkingTypes.find(query).skip(skip).limit(limit).sort({ '_id': 1 }).exec()
        if (_.isEmpty(result)) {
            return res.json({ status: 'success', message: "No result found" })
        }

        return res.json({ status: 'Success', data: result, totalCount: totalCount })
    } catch (e) {
        return res.json({ status: 'fail', message: e.message })
    }
}


exports.updateParkingType = async function (req, res) {
    
    try {
        const id = mongoose.mongo.ObjectId(req.body.id);
        const obj = {
        type: req.body.type, 
    }
        if (_.isEmpty(req.body.type)) {
            throw new ApplicationError("Missing required parameter", 500, "");
        }
        const result = await ParkingTypes.update({"_id": id},{$set: obj})
        return res.json({ status: 'Success', message: 'Parking type updated successfully' })
    } catch (e) {
        return res.json({ status: 'fail', message: e.message })
    }
}

exports.deleteParkingType = async function (req, res) {
    try {
    const id = mongoose.mongo.ObjectId(req.params.id);
    
        var result = await ParkingTypes.remove({ _id: id }).exec();
        if (!_.isEmpty(result)) {
            return res.json({ status: "success", message: "Parking type deleted successfully!.." });
        } else {
            throw new ApplicationError("Unable to delete the record", 500, "");
        }
    } catch (error) {
        return res.json({ status: "fail", error: "Unable to delete the record, please check the Id" })
    }
}