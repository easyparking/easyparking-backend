exports.searchStand = async(req,res) => {
    try{
        const { vehicleType, duration, startDate, state, city } = req.body;
        let query = {};
        if(state &&  city){
            query = {'location.state.id': state, 'location.city.id': city, allowOnlineCards: true };
        }
        if(vehicleType) {
            query.allowedVehicles = vehicleType;
        }
        const result = await Stands.find(query).sort({standName: 1}).exec();
        res.json({status: 'success', result})
    } catch(error){
        res.json({error})
    }
}

exports.getAllCities = async(req, res)=> {
    try{
        const {stateId} = req.params;
        const districts = await Districts.find({stateId}).exec();
        const cities = await Cities.find().exec();
        let result = [];
        districts.forEach(element => {
            let res = cities.filter(x => x.districtId === String(element._id));
            result.push({
                label: element,
                items: res
            })
        });
        res.json({status: 'success', result})
    } catch(e) {

    }
}