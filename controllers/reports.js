const logger = require('../config/logger');
exports.getEmpList = async (req, res) => {
    try {
        const ownerId = req.params.ownerId;
        const data = await Users.find({ $and: [{ role: 'employee', ownerId: ownerId }] }).select('employeeId name userName standId ownerId standName phoneNumber').exec();
        res.json({ status: 'success', data });
    } catch (error) {
        res.json({ status: 'fail', error });
    }
}

exports.getEmpSessionsList = async (req, res) => {
    try {
        const userName = req.params.userName;
        let date = moment().subtract(7, 'days').toDate();
        if (req.query.role === 'employee') {
            date = moment().subtract(1, 'days').toDate();
            const data = await Sessions.find({
                userName: userName, role: 'employee',"end_time": { $ne: null},
                "createdDate": {
                    $gte: date
                }
            }).sort({ createdDate: -1 }).select('start_time end_time userName standId ownerId').limit(1).exec();
            res.json({ status: 'success', data });
        } else {
            const data = await Sessions.find({
                userName: userName, role: 'employee',
                "createdDate": {
                    $gte: date
                }, "end_time":{$ne:null}
            }).sort({ createdDate: 1 }).select('start_time end_time userName standId ownerId').exec();
            res.json({ status: 'success', data });
        }
    } catch (error) {
        res.json({ status: 'fail', error });
    }
}

exports.getCardSalesReports = async (req, res) => {
    try {
        const {startTime,endTime,standId } = req.body
            const cardsList = await Cards.find({
                createdDate: {
                    $gte: new Date(startTime),
                    $lt: new Date(endTime)
                }, standId
            }).exec();
            res.json({ status: 'success', cardsList })
        
    } catch (e) {
        res.json({ status: 'error', e })
    }
}

const LogoutReportquery = (startDate, endDate, ownerId, standId, empId) => {
    return [{
        "$facet": {
            "entries": [{
                "$match": {
                    $and: [{
                        dateAndTimeOfParking: {
                            $gte: new Date(startDate),
                            $lt: new Date(endDate)
                        }
                    }, { $or: [{ openEmpId: empId }, { closedEmpId: empId }] }]
                }
            },
            {
                $count: "entries"
            }
            ],
            "exits": [{
                "$match": {
                    $and: [{
                        status: 'Exit'
                    }, {
                        dateAndTimeOfExit: {
                            $gt: new Date(startDate),
                            $lt: new Date(endDate)
                        }
                    }, { $or: [{ openEmpId: empId }, { closedEmpId: empId }] }]
                }
            },
            {
                $count: "exits"
            }
            ],
            "cancelled": [{
                "$match": {
                    $and: [{
                        status: 'Cancelled'
                    }, {
                        dateAndTimeOfExit: {
                            $gte: new Date(startDate),
                            $lt: new Date(endDate)
                        }
                    }, { $or: [{ openEmpId: empId }, { closedEmpId: empId }] }]
                }
            },
            {
                $count: "cancelled"
            }
            ],
            "cardVehicles": [{
                "$match": {
                    $and: [{
                        cardNumber: { $ne: "" }
                    }, {
                        dateAndTimeOfParking: {
                            $gte: new Date(startDate)
                        }
                    }, { $or: [{ openEmpId: empId }, { closedEmpId: empId }] }]
                }
            },
            {
                $count: "cardVehicles"
            }
            ]
        }
    },
    {
        "$project": {
            "entries": {
                "$arrayElemAt": [
                    "$entries.entries",
                    0
                ]
            },
            "exits": {
                "$arrayElemAt": [
                    "$exits.exits",
                    0
                ]
            },
            "cancelled": {
                "$arrayElemAt": [
                    "$cancelled.cancelled",
                    0
                ]
            },
            "cardVehicles": {
                "$arrayElemAt": [
                    "$cardVehicles.cardVehicles",
                    0
                ]
            }
        }
    }
    ]
}

const cardAmount = (startDate, endDate, standId) => {
    try {
        return [{
            $match: {
                standId: standId,
                createdDate: {
                    $gte: new Date(startDate),
                    $lte: new Date(endDate)
                }
            }
        }, {
            $group: {
                _id: null,
                SUM: {
                    $sum: "$amount"
                },
                COUNT: {
                    $sum: 1
                }
            }
        }]
    } catch (e) {
        console.log("error", error);
    }
}

exports.getEmployeeReport = async (req, res) => {
    try{
        const { sessionId, reportType } = req.body;
        if(reportType == "PendingCurrentVehiclesReport"){
            const PendingCurrentVehiclesReport = await generateCurrentVehiclesReport(req.body);
            logger.warn({message: "Searched for Pending Current Vehicles Reports."})
            res.json({ status: 'success', report: {PendingCurrentVehiclesReport} });
        } else {
            const report = await Reports.findOne({sessionId}).select(`${reportType} sessionId`).exec();
            if(!report){
                logger.warn({message: "Report not found, generating new report"});
               const generateReport = await this.generateEmployeeReports(req.body);
               const result = await Reports.findOne({sessionId}).select(`${reportType} sessionId`).exec();
               res.json({ status: 'success', report: result });
            } else {
                res.json({ status: 'success', report });
            }
        }
    } catch(e) {
        res.json({ status: 'fail', message: e });
    }
}

exports.generateEmployeeReports = async (data) => {
    try {
        const columns = ["pendingVehiclesLogin", "entries", "exits", "cancelled", "cardVehicles", "numberLessVehicles", "pendingVehiclesLogout"];
        const logoutReport = await generateLogoutReport(data, columns);
        const PendingVehiclesReport = await generatePendingVehiclesReport(data, columns);
        const PendingLogoutVehiclesReport = await generateLogoutVehiclesReport(data, columns);
        const PendingCurrentVehiclesReport = await generateCurrentVehiclesReport(data, columns);
        const cardsReport = await generateCardsReports(data, columns);
        let sessionReport = {
            sessionId: data.sessionId,
            standId: data.standId,
            ownerId: data.ownerId,
            employeeId: data.employeeId,
            logoutReport,
            PendingVehiclesReport,
            PendingLogoutVehiclesReport,
            PendingCurrentVehiclesReport,
            cardsReport,
        };
        const saveReports = await Reports.create(sessionReport);
        logger.warn({message: `Reports generated for employee: ${data.employeeId} for sessionId: ${data.sessionId}`});
    } catch (e) {
        console.log("error:", e);
    }
}

const generateLogoutReport = async (data, columns)=> {
    try{
        const { employeeId,start_time,end_time,standId,ownerId,sessionId } = data;
        const parkingsList = await Parkings.find({
            $and: [{ $or: [{ openEmpId: employeeId }, { closedEmpId: employeeId }] }, {
                $or: [{
                    dateAndTimeOfParking: {
                        $gte: new Date(start_time),
                        $lt: new Date(end_time)
                    }
                },{
                    dateAndTimeOfExit: {
                        $gte: new Date(start_time),
                        $lt: new Date(end_time)
                    }
                }]
            }]
        }).exec();
        const query = LogoutReportquery(start_time, end_time, ownerId, standId, employeeId);
        const parkingResult = await Parkings.aggregate(query).exec();
        const cardData = await Cards.aggregate(cardAmount(start_time, end_time, standId)).exec();
        const sessionData = await Sessions.findOne({_id: sessionId}).select('pendingonLogin pendingonLogout').exec();
        // const parkingsAmount = await Parkings.aggregate(cardAmount(startTime, endTime)).exec();
        let total = {};
        for (let i = 0; i < columns.length; i++) {
            total[columns[i]] = parkingResult.length && parkingResult[0][columns[i]] ? parkingResult[0][columns[i]] : 0;
        };
        if(sessionData){
            total['pendingVehiclesLogin'] = sessionData['pendingonLogin'];
            // total['pendingVehiclesLogout'] = sessionData['pendingonLogout'];
            total['pendingVehiclesLogout'] = sessionData['pendingonLogin'] + total['entries'] - total['exits'] - total['cancelled'];
        }
        return { parkingsList: parkingsList, total: total, cardAmount: cardData }
    } catch(e) {
        console.log("error:", e);
    }
}

const generatePendingVehiclesReport = async (data)=> {
    try{
        const { employeeId,end_time,standId } = data;
        const parkingsList = await Parkings.find({
            // $and: [{ $or: [{ openEmpId: employeeId }, { closedEmpId: employeeId }] },{dateAndTimeOfParking:{
            //     $lt: new Date(end_time)
            // }},{
            //     $or: [{
            //         dateAndTimeOfExit: {
            //             $gt: new Date(end_time)
            //         }
            //     },{
            //         status: 'Entry'
            //     }]
            // }]
            $and: [{
                dateAndTimeOfParking:{
                    $lt: new Date(end_time)
                },
                status: 'Entry',
                standId: standId
            }]
        }).exec();
        return parkingsList;
    } catch(e) {
        console.log("error:", e);
    }
}

const generateLogoutVehiclesReport = async (data)=> {
    try{
        const { employeeId,end_time,standId,start_time } = data;
        const parkingsList = await Parkings.find({
            // $and: [{ $or: [{ openEmpId: employeeId }, { closedEmpId: employeeId }] },{dateAndTimeOfParking:{
            //     $lt: new Date(end_time)
            // }},{
            //     $or: [{
            //         dateAndTimeOfExit: {
            //             $gt: new Date(end_time)
            //         }
            //     },{
            //         status: 'Entry'
            //     }]
            // }]
            $and: [{
                dateAndTimeOfParking:{
                    $lt: new Date(end_time)
                },
                status: 'Entry',
                standId: standId
            }]
        }).select('vehicleNumber').exec();
        return parkingsList;
    } catch(e) {
        console.log("error:", e);
    }
}

const generateCurrentVehiclesReport = async (data)=> {
    try{
        const { standId } = data;
        const parkingsList = await Parkings.find({
            standId,
            status: 'Entry'
        }).select('vehicleNumber').exec();
        return parkingsList;
    } catch(e) {
        console.log("error:", e);
    }
}

const generateCardsReports = async (data)=> {
    try{
        const { start_time,end_time,standId } = data;
        const cardsList = await Cards.find({
            createdDate: {
                $gte: new Date(start_time),
                $lt: new Date(end_time)
            }, standId
        }).exec();
        return cardsList;
    } catch(e) {
        console.log("error:", e);
    }
}