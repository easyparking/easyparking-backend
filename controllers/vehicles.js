exports.addVehicleName = async function (req, res) {
    const vehicleName = req.body.vehicleName;
    try {
        if (_.isEmpty(vehicleName) || _.isEmpty(req.body.vehicleType)) {
            throw new ApplicationError("Missing required parameter", 500, "");
        }
        const vehicles = await Vehicles.findOne({ "vehicleName": vehicleName }).sort({ '_id': 1 }).exec();
        if (!_.isEmpty(vehicles)) {
            return res.json({ status: 'fail', message: vehicleName + ' is already exists' })
        }
        const result = await Vehicles.create({"vehicleName": vehicleName, "type": req.body.vehicleType})
        return res.json({ status: 'Success', message: 'Vehicle Name added succesfully' })
    } catch (e) {
        return res.json({ status: 'fail', message: e.message })
    }
}

exports.updateVehicle = async function (req, res) {
    const id = mongoose.mongo.ObjectId(req.body.id);
    const vehcileObj = {
        vehicleName: req.body.vehicleName, 
        type: req.body.vehicleType, 
    }
    try {
        if (_.isEmpty(req.body.vehicleName) || _.isEmpty(req.body.vehicleType)) {
            throw new ApplicationError("Missing required parameter", 500, "");
        }
        const result = await Vehicles.update({"_id": id},{$set: vehcileObj})
        return res.json({ status: 'Success', message: 'Vehicle Name added succesfully',result: result })
    } catch (e) {
        return res.json({ status: 'fail', message: e.message })
    }
}

exports.getVehicleNamesList = async function (req, res) {
    const vehicleName = req.params.vehicleName;
    try {
        if (_.isEmpty(vehicleName)) {
            throw new ApplicationError("Vehicle Name is required parameter", 500, "");
        }
        const vehicles = await Vehicles.find({ "vehicleName":{ $regex: vehicleName, $options: 'i' } }).sort({ '_id': 1 }).exec();

        return res.json({ status: 'Success', data: vehicles })
    } catch (e) {
        return res.json({ status: 'fail', message: e.message })
    }
}

exports.getVehiclesList = async function (req, res) {
    const size = parseInt(req.query.size);
    const pageNo = parseInt(req.query.pageNo);
    const search_text = req.query.searchText;
    const skip = size * (pageNo - 1);
    const limit = size;
    let totalCount = 0;
    let query = {};

    if (pageNo < 0 || pageNo === 0) {
        return res.json({ status: "fail", message: "invalid page number" });
    }

    if (search_text) {
        query = {
            $or: [
                { 'name': { $regex: search_text, $options: 'i' } },
                { 'type': { $regex:  search_text, $options: 'i' } },
            ]
        }
    }
    try {
        totalCount = await Vehicles.countDocuments(query).sort({ '_id': 1 }).exec();
        if (totalCount < 0) {
            throw new ApplicationError("Vehicles data not found", 500, "");
        }
        var result = await Vehicles.find(query).skip(skip).limit(limit).sort({ '_id': 1 }).exec()
        if (_.isEmpty(result)) {
            return res.json({ status: 'success', message: "No result found", data: result })
        }

        return res.json({ status: 'Success', data: result, totalCount: totalCount })
    } catch (e) {
        return res.json({ status: 'fail', message: e.message })
    }
}

exports.deleteVehicleName = async function (req, res) {
    const id = mongoose.mongo.ObjectId(req.params.id);
    try {
        var result = await Vehicles.remove({ _id: id }).exec();
        if (!_.isEmpty(result)) {
            return res.json({ status: "success", message: "Vehicle Name Deleted successfully!.." });
        } else {
            throw new ApplicationError("Unable to delete the record", 500, "");
        }
    } catch (error) {
        return res.json({ status: "fail", error: error })
    }
}