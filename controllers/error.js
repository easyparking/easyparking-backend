class ApplicationError extends Error {
    constructor(message, status, err_code) {
	    super();
	    
	    Error.captureStackTrace(this, this.constructor);
	    
	    this.name = this.constructor.name;
	    
	    this.message = message || "Something went wrong";
	    
	    this.status = status || 500;

	    this.err_code = err_code || "FANT_000";
	}
}
module.exports = ApplicationError;