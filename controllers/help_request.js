exports.createHelpRequest = async (req, res) => {
    try {
        let requestData = req.body;
        const result = await HelpRequest.create(requestData);
        return res.json({ status: "Success", message: "Request Submited successfully", data: result });
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

exports.getHelpRequestList = async (req, res) => {
    const search_text = req.query.searchText;
    const size = parseInt(req.query.size);
    const pageNo = parseInt(req.query.pageNo);
    if (pageNo < 0 || pageNo === 0) {
        return res.json({ status: "fail", message: "invalid page number" });
    }
    const skip = size * (pageNo - 1);
    const limit = size;
    try {
        let query = {};
        if (search_text) {
            query = {
                $or: [
                    { 'name': { $regex:  search_text, $options: 'i' } },
                    { 'email': { $regex:  search_text, $options: 'i' } },
                    { 'subject': { $regex:  search_text, $options: 'i' } },
                    { 'message': { $regex:  search_text, $options: 'i' } }
                ]
            }
        }
            let totalCount = await HelpRequest.countDocuments(query).exec();
            if (totalCount < 0) {
                throw new ApplicationError("No data found", 500, "");
            }
            const result = await HelpRequest.find(query).skip(skip).limit(limit).sort({ '_id': -1 }).exec();
            if (!_.isEmpty(result)) {
                return res.json({ status: "success", message: "Help Requests Data", data: result, totalCount: totalCount });
            } else {
                throw new ApplicationError("No data found", 500, "");
            }
        
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}