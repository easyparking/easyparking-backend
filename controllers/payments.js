var Insta = require('instamojo-nodejs');
let config = require('../config/config').instamojoInfo;

Insta.setKeys(config.API_KEY, config.AUTH_KEY);

exports.paymentRequest = async (req, res) => {
  try {
    let data = new Insta.PaymentData();
    const { name, email, purpose, amount, redirectUrl } = req.body;
    data.name = name;
    data.email = email;
    data.amount = amount;
    data.purpose = purpose;
    data.setRedirectUrl(redirectUrl);

    Insta.createPayment(data, function (error, response) {
      if (error) {
        // some error
        throw new ApplicationError(error, 500, "");
      } else {
        // Payment redirection link at response.payment_request.longurl
        res.json({ status: 'success', response });
      }
    });
  } catch (error) {
    res.json({ status: 'fail', error })
  }
}