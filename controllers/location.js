exports.addState = async function (req, res) {
    const reqData = req.body
    reqData.state = reqData.state
    try {
        if (_.isEmpty(reqData.state)) {
            throw new ApplicationError("State is required parameter", 500, "");
        }
        var states = await States.find({ "state": reqData.state }).sort({ '_id': 1 }).exec();
        if (!_.isEmpty(states)) {
            return res.json({ status: 'fail', message: reqData.state + ' is already exists' })
        }

        var result = await States.create(reqData)
        return res.json({ status: 'Success', message: 'Successfully added State' })

    } catch (e) {
        return res.json({ status: 'fail', message: e.message })
    }
}

exports.getStates = async function (req, res) {
    try {
        const result = await States.find({}).sort({ 'state': 1 }).exec();
        return res.json({ status: "Success", data: result })
    } catch (error) {
        return res.json({ status: "fail", error: error })
    }
}

exports.updateState = async (req, res) => {
    var id = req.params.id;
    const requestData = req.body;
    try {
        if (_.isEmpty(id)) {
            throw new ApplicationError("Id is required parameter", 500, "");
        }
        var result = await States.update({ _id: id },{$set: requestData}).exec();
        if(result.nModified > 0){
            return res.json({ status: "success", "message": "Record updated succesfully"})
        } else {
            return res.json({ status: "fail", "message": "No record found with id" })
        }
    } catch (error) {
        return res.json({ status: "fail", error: error })
    }
}

exports.deleleState = async function (req, res) {
    var id = req.params.id

    try {
        if (_.isEmpty(id)) {
            throw new ApplicationError("Id is required parameter", 500, "");
        }
        var data = await States.remove({ _id: id }).exec();
        var data1 = await Districts.deleteMany({ stateId: id }).exec();
        var data2 = await Cities.deleteMany({ stateId: id }).exec();

        if (data.deletedCount == 0) {
            throw new ApplicationError("No Data Found", 404, "");
        }
        return res.json({ status: "success", "message": "State is succesffuly deleted", data: data })

    } catch (error) {
        return res.json({ status: "fail", error: error })
    }
}

exports.addDistrict = async function (req, res) {
    var reqData = req.body;
    var stateId = reqData.stateId;
    try {
        if (_.isEmpty(stateId) || _.isEmpty(reqData.district)) {
            throw new ApplicationError("required parameters are missing", 500, "");
        }

        const districts = await Districts.find({ stateId: stateId }).sort({ '_id': 1 }).exec();
        const stateResult = await States.update({ _id: stateId },{$set: {districtsCount: reqData.districtsCount}}).exec();

        if (!_.isEmpty(districts) && districts.find(x => x.district === reqData.district)) {
            return res.json({ status: 'fail', message: reqData.district + ' is already exists' })
        }
        var result = await Districts.create(reqData)
        return res.json({ status: 'Success', message: 'Successfully added District' })

    } catch (e) {
        return res.json({ status: 'fail', message: e.message })
    }
}

exports.getDistricts = async function (req, res) {
    stateId = req.params.stateId
    try {
        var result = await Districts.find({stateId : stateId}).sort({ 'district': 1 }).exec();
        return res.json({ status: "success", data: result })
    } catch (error) {
        return res.json({ status: "fail", error: error })
    }
}

exports.updateDistrict = async (req, res) => {
    var id = req.params.id;
    const requestData = req.body;
    try {
        if (_.isEmpty(id)) {
            throw new ApplicationError("Id is required parameter", 500, "");
        }
        var result = await Districts.update({ _id: id },{$set: requestData}).exec();
        if(result.nModified > 0){
            return res.json({ status: "success", "message": "Record updated succesfully"})
        } else {
            return res.json({ status: "fail", "message": "No record found with id" })
        }
    } catch (error) {
        return res.json({ status: "fail", error: error })
    }
}

exports.deleteDistrict = async function (req, res) {
    const id = req.params.id;
    const stateId = req.query.stateId;
    try {
        if (_.isEmpty(id)) {
            throw new ApplicationError("Id is required parameter", 500, "");
        }
        var data = await Districts.remove({ _id: id }).exec();
        var data1 = await Cities.deleteMany({ districtId: id }).exec();
        var statesCount = await States.update({ _id: stateId },{$set: {districtsCount: req.query.districtsCount}}).exec();

        if (data.deletedCount == 0) {
            throw new ApplicationError("No Data Found", 404, "");
        }
        return res.json({ status: "success", "message": "District is successfully deleted", data: data })

    } catch (error) {
        return res.json({ status: "fail", error: error })
    }
}

exports.addCity = async function (req, res) {
    const reqData = req.body;
    const districtId = reqData.districtId;
    reqData.districtId = reqData.districtId;
    reqData.city = reqData.city;
    reqData.stateId =  reqData.stateId;
    try {
        if (_.isEmpty(districtId) || _.isEmpty(reqData.city) || _.isEmpty(reqData.stateId)) {
            throw new ApplicationError("required parameters are missing", 500, "");
        }
        const cities = await Cities.find({$and : [{ districtId: districtId },{city :reqData.city }]},).sort({ '_id': 1 }).exec();
        const citiesCount = await Districts.update({$and: [{ stateId: stateId },{ _id: districtId}]},{$set: {citiesCount: reqData.citiesCount}}).exec();

        if (!_.isEmpty(cities)) {
            return res.json({ status: 'fail', message: reqData.city + ' is already exists' });
        }
        const result = await Cities.create(reqData)
        return res.json({ status: 'Sucess', message: 'Successfully added City' });

    } catch (e) {
        return res.json({ status: 'fail', message: e.message })
    }
}

exports.getCities = async function (req, res) {
    districtId = req.params.districtId
    try {
        var data = await Cities.find({districtId : districtId}).sort({ 'city': 1 }).exec();
        return res.json({ status: "success", data: data })
    } catch (error) {
        return res.json({ status: "fail", error: error })
    }
}

exports.updateCity = async (req, res) => {
    var id = req.params.id;
    const requestData = req.body;
    try {
        if (_.isEmpty(id)) {
            throw new ApplicationError("Id is required parameter", 500, "");
        }
        var result = await Cities.update({ _id: id },{$set: requestData}).exec();
        if(result.nModified > 0){
            return res.json({ status: "success", "message": "Record updated succesfully"})
        } else {
            return res.json({ status: "fail", "message": "No record found with id" })
        }
    } catch (error) {
        return res.json({ status: "fail", error: error })
    }
}

exports.deleleCity = async function (req, res) {
    var id = req.params.id;
    const districtId = req.query.districtId;
    try {
        if (_.isEmpty(id)) {
            throw new ApplicationError("Id is required parameter", 500, "");
        }
        var data = await Cities.remove({ _id: id }).exec();
        var citiesCount = await Districts.update({ _id: districtId },{$set: {citiesCount: req.query.citiesCount}}).exec();

        if (data.deletedCount == 0) {
            throw new ApplicationError("No Data Found", 404, "");

        }
        return res.json({ status: "success", "message": "City is succesffuly deleted", data: data })

    } catch (error) {
        return res.json({ status: "fail", error: error })
    }
}
