const logger = require('../config/logger');
const SMS = require('./SMS');
exports.createParking = async function (req, res) {
    let parkingData = req.body;
    logger.warn({message: `Create Parking Initalized - ${parkingData.vehicleNumber} / ${parkingData.phone} / ${parkingData.sessionId} / ${parkingData.status}`, level: 'api'});
    if (parkingData.ownerId == "") {
        logger.error({message: `Create Parking: No Owner Data - ${parkingData.vehicleNumber} / ${parkingData.phone} / ${sessionId}`});
        return res.json({ status: "fail", message: "Enter required parameters" });
    }
    try {
        const code = parkingData.standCode || 'PKT';
        if(parkingData.vehicleNumber){
            const vehicleNumber_exist = await Parkings.findOne({$and:[{standId: parkingData.standId, status: 'Entry'},{$or:[{"phone": parkingData.phone},{"vehicleNumber": parkingData.vehicleNumber}]}]}).exec();
            if (!_.isEmpty(vehicleNumber_exist)) {
                if(parkingData.phone == vehicleNumber_exist.phone){
                    logger.error({message: 'Create Parking: With this Phone number a vehicle is already parked. Add New Phone Number!'});
                    return res.json({ status: "fail", message: "With this number a vehicle is already parked." });
                } else {
                    logger.error({message: 'Create Parking: With this number a vehicle is already parked'});
                    return res.json({ status: "fail", message: "With this number a vehicle is already parked." });
                }
                
            }
        }
        if(parkingData.vehicleNumber=='') parkingData.vehicleNumber=parkingData.phone;
        const cardDetails = await Cards.findOne({$and:[{standId: parkingData.standId},{$or:[{"customerPhone": parkingData.phone},{"vehicleNumber": parkingData.vehicleNumber}]}]}).exec();
        if(parkingData.cardNumber != '' && !cardDetails){
            logger.error({message: 'Create Parking: Card Details Not Matched.'});
            return res.json({ status: "fail", message: "Card Details Not Matched.", result:cardDetails });
        }
        if(cardDetails && cardDetails.startDate && cardDetails.cardId && cardDetails.endDate){
            if(cardDetails.vehicleNumber != parkingData.vehicleNumber){
                logger.warn({message: `Create Parking: Given VehicleNumber(${parkingData.vehicleNumber}) not matched with card details(${cardDetails.cardId}).`});
                return res.json({ status: "fail", message: "Card Details Not Matched.", result:cardDetails });
            }
            if(moment(cardDetails.startDate).isBefore(new Date()) && moment(cardDetails.endDate).isAfter(new Date())) {
                parkingData.customerName = cardDetails.customerName;
                parkingData.cardNumber = cardDetails.cardId;
                parkingData.cardStartDate = cardDetails.startDate;
                parkingData.cardEndDate = cardDetails.endDate;
                parkingData.offerPrice = parkingData.finalPrice;
                parkingData.finalPrice = 0;
                parkingData.actualPrice = 0;
            } else {
                parkingData.customerName = '';
                parkingData.cardNumber = '';
                parkingData.cardStartDate = '';
                parkingData.cardEndDate = '';
            }
        }
        const lastParking = await Parkings.find({standId: parkingData.standId}).sort({_id:-1}).limit(1);
        if (lastParking.length == 0) {
            parkingData.parkingId = await helper.getParkingId(0, code)
        } else {
            let lastParkingId = Number(lastParking[0].parkingId.substring(7));
            let isExist = {};
            do {
                parkingData.parkingId = await helper.getParkingId(lastParkingId, code);
                isExist = await Parkings.findOne({parkingId: parkingData.parkingId}).exec();
                lastParkingId += 1;
            } while (!_.isEmpty(isExist));
        };
        logger.warn({message: `Create Parking: Parking Id generated - ${parkingData.parkingId} / ${parkingData.vehicleNumber}`});        
        const result = await Parkings.create(parkingData);
        const sessionUpdate = await Sessions.update({ _id: parkingData.sessionId }, { $inc: { entries: 1 }}).exec();
        if (!_.isEmpty(result)) {
            let smsResult = "SMS service not enabled for parking";
            if(parkingData.sendSMS){
                parkingData.parkingDate = moment(new Date(parkingData.dateAndTimeOfParking)).format('DD.MM.YYYY');
                parkingData.parkingTime = moment(new Date(parkingData.dateAndTimeOfParking)).format('HH:mm:ss');
                smsResult = await SMS.sendSMS('parking_entry',parkingData, parkingData.phone);
                logger.warn({message: `Create Parking: SMS sent to phone  - ${parkingData.phone} / ${parkingData.vehicleNumber}`}); 
            }
            logger.warn({message: `Create Parking: Parkeing vehicle api success - ${parkingData.parkingId} / ${parkingData.vehicleNumber} / ${parkingData.phone} / ${parkingData.sessionId} / ${parkingData.status}`});  
            return res.json({ status: "success", message: "Entry Success", data: result, smsResult: smsResult, sessionUpdate});
        } else {
            throw new ApplicationError("Parking data not created", 500, "");
        }
    } catch (e) {
        logger.error({message: `Create Parking error - ${e}, ${parkingData.vehicleNumber} / ${parkingData.phone} / ${parkingData.sessionId} / ${parkingData.status}`});
        return res.json({ status: "fail", message: e.message });
    }
}

//Parkings list data
exports.getParkingsList = async function (req, res) {
    const {searchText,standId} = req.query;
    const size = parseInt(req.query.size);
    const pageNo = parseInt(req.query.pageNo);
    let query = {};
    //only display parkings on entry status
    // if(standId){
    //     query = {$and:[{
    //         standId: standId
    //     },{status: {$ne: 'Cancelled'}},{status: {$ne: 'Exit'}},{$or:[{
    //         dateAndTimeOfParking: {
    //             $gte: new Date(req.query.startDate)
    //         }
    //     },{status: 'Entry'}]}]}
    // }
    if (searchText && standId) {
        query = {
            $and: [{
                standId: standId
            },{
                $or: [
                    { 'parkingId': { $regex: searchText, $options: 'i' } },
                    { 'vehicleNumber': { $regex: searchText, $options: 'i' } },
                    { 'phone': { $regex: searchText, $options: 'i' } },
                    { 'type': { $regex: searchText, $options: 'i' } },
                    { 'cardNumber': { $regex: searchText, $options: 'i' } }
                ]
            }/*,
            {
                dateAndTimeOfParking: {
                    $gte: new Date(req.query.startDate)
                }
            }*/]
        }
    } else if(!searchText && standId){
        query = {$and:[{
                standId: standId
            },{status: {$ne: 'Cancelled'}},{status: {$ne: 'Exit'}},{$or:[{
                dateAndTimeOfParking: {
                    $gte: new Date(req.query.startDate)
                }
            },{status: 'Entry'}]}]}
    } else {
        query = {
                $or: [
                    { 'parkingId': { $regex: searchText, $options: 'i' } },
                    { 'vehicleNumber': { $regex: searchText, $options: 'i' } },
                    { 'phone': { $regex: searchText, $options: 'i' } },
                    { 'type': { $regex: searchText, $options: 'i' } },
                    { 'cardNumber': { $regex: searchText, $options: 'i' } }
                ]
        }
    }
    if (pageNo < 0 || pageNo === 0) {
        return res.json({ status: "fail", message: "invalid page number" });
    }
    const skip = size * (pageNo - 1);
    const limit = size;
    try {
        let totalCount = await Parkings.countDocuments(query).exec();
        if (totalCount < 0) {
            throw new ApplicationError("No Parkings data found", 500, "");
        }
        const result = await Parkings.find(query).skip(skip).limit(limit).sort({ 'createdDate': -1 }).exec();
        if (!_.isEmpty(result)) {
            return res.json({ status: "success", message: "Parkings Data", data: result, totalCount: totalCount });
        } else {
            throw new ApplicationError("No Parkings data found", 500, "");
        }
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

/* 
Add parking live search with parking ID/vehicle number
**/
exports.getParkingDetails = async function (req, res) {
    const searchInput = req.query.vehicleNumber;
    const standId = req.query.standId;
    try {
        // let result1 = await Parkings.find({ $and: [{$or: [{ 'vehicleNumber': { $regex:  vehicleNumber, $options: 'i' } }, { 'parkingId': { $regex:  vehicleNumber, $options: 'i' } }]},{standId: standId}] }).sort({'status': -1}).exec();
        let result = await Parkings.aggregate([
            {
                $match: { $and: [{$or: [{ 'vehicleNumber': { $regex:  searchInput, $options: 'i' } }, { 'parkingId': { $regex:  searchInput, $options: 'i' } }]},{standId: standId}] }
              },
            { "$project" : {
                "_id" : 1,
                "vehicleNumber" : 1,
                "vehicleName": 1,
                "vehicleType": 1,
                "cardNumber": 1,
                "customerName": 1,
                "cardStartDate":1,
                "cardEndDate":1,
                "customerAddress": 1,
                "phone": 1,
                "status" : 1,
                "parkingId" : 1,
                "ownerId" : 1,
                "standId" : 1,
                "location" : 1,
                "dateAndTimeOfParking" : 1,
                "order" : {
                    "$cond" : {
                        if : { "$eq" : ["$status", "Entry"] }, then : 1,
                        else  : { "$cond" : {
                            "if" : { "$eq" : ["$status", "Exit"] }, then : 2, 
                            else  : 3
                            }
                        }
                    }
                }
            } }, 
            {"$sort" : {"order" : 1} }
        ]);
        let uniqueArray = [];
        let data = result.filter(item => {
            if(!uniqueArray.includes(item.vehicleNumber)){
                uniqueArray.push(item.vehicleNumber);
                return true;
            } else{
                return false;
            }
        })
        if (data.length) {
            return res.json({ status: "success", message: "Parking Details", data });
        } else {
            return res.json({ status: "success", message: "No parkings data found" });
        }
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

exports.getParkingDetailsByOwner = async function (req, res) {
    const vehicleNumber = req.query.vehicleNumber;
    const ownerId = req.query.ownerId;
    try {
        var result = await Parkings.find({ $and: [{ 'vehicleNumber': { $regex:  vehicleNumber, $options: 'i' } }, { status: 'Entry' },{ownerId: ownerId}] }).exec();
        if (!_.isEmpty(result)) {
            return res.json({ status: "success", message: "Parking Details", data: result });
        } else {
            throw new ApplicationError("No Parking data found", 500, "");
        }
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

exports.exitParking = async function (req, res) {
    const parkingData = req.body;
    let validCard = false;
    let options = {
        "parkingId": parkingData.parkingId,
        "standId": parkingData.standId,
        "status": parkingData.status,
        "closedEmpName": parkingData.closedEmpName,
        "closedEmpMobile": parkingData.closedEmpMobile,
        "dateAndTimeOfParking": parkingData.dateAndTimeOfParking,
        "dateAndTimeOfExit": parkingData.dateAndTimeOfExit,
        "vehicleType": parkingData.vehicleType,
        "cardNumber": parkingData.cardNumber
    }
    try {
        const parkingsData = await Parkings.find({ "parkingId": parkingData.parkingId, "standId": parkingData.standId }).exec();
        let parking_details = {};
        logger.warn({message: `Exit Parking Initalized - ${parkingData.parkingId} / ${parkingData.vehicleNumber} / ${parkingData.sessionId}`, level: 'api'});
        if(parkingsData.length > 1){
            logger.warn({message: `Exit Parking: Morethan 1 record with same parking id - ${parkingData.parkingId} / ${parkingData.vehicleNumber} / ${parkingData.sessionId}`, level: 'api'});
            const deleteDuplicate = await Parkings.remove({ "parkingId": parkingData.parkingId }).exec();
            const saveDuplicates = await Duplicates.create({ "duplicates": parkingsData });
            var newdoc = new Parkings(parkingsData[0]);
            newdoc._id = mongoose.Types.ObjectId();
            newdoc.isNew = true;
             const newResponse = await newdoc.save();
             if(newResponse && newResponse._doc && newResponse._doc.hasOwnProperty('parkingId')){
                parking_details = newResponse;
             } else {
                parking_details = await Parkings.findOne({ "parkingId": parkingData.parkingId, "standId": parkingData.standId }).exec();
             }
            logger.warn({message: `Exit Parking: created new record with same parking id - ${parking_details.parkingId} / ${parking_details.vehicleNumber} / ${parkingData.sessionId}`, level: 'api'});
        } else if(parkingsData.length == 1){
            logger.warn({message: `Exit Parking: only one retrived with parking id - ${parkingData.parkingId} / ${parkingData.vehicleNumber} / ${parkingData.sessionId}`, level: 'api'});
            parking_details = parkingsData[0];
        } else {
            logger.warn({message: `Exit Parking: No parkings found with parkingId 1 - ${parkingData.parkingId} / ${parkingData.vehicleNumber} / ${parkingData.sessionId}`, level: 'api'});
            throw new ApplicationError("Parking data not found", 500, "");
        }
        const priceRulesResult = await PriceRules.findOne({ "standId": parkingData.standId }).exec();
        const cardData = await Cards.findOne({cardId: parkingData.cardNumber}).exec();
        const priceDetails = await calculateAmount(options,priceRulesResult,cardData);
        if(cardData){
            if(moment(new Date(cardData.endDate)).isAfter(new Date())){
                logger.warn({message: `Exit Parking: Parking vehicle has valid monthly card - ${parkingData.parkingId} / ${parkingData.vehicleNumber} / ${parkingData.sessionId}`, level: 'api'});
                validCard = true;
            }
        };
        if (_.isEmpty(parking_details)) {
            logger.warn({message: `Exit Parking: No parking data found 2 - ${parkingData.parkingId} / ${parkingData.vehicleNumber} / ${parkingData.sessionId}`, level: 'api'});
            throw new ApplicationError("Parking data not found", 500, "");
        } else {
            if(parkingData.status == 'Cancelled'){
                logger.warn({message: `Exit Parking: Parking status cancelled - ${parkingData.parkingId} / ${parkingData.vehicleNumber} / ${parkingData.sessionId}`, level: 'api'});
                parkingData.finalPrice = 0;
                parkingData.offerPrice = 0;
                parkingData.actualPrice = 0;
            } else if(parkingData.cardNumber == ""){
                parkingData.finalPrice = priceDetails.finalPrice || 0;
                parkingData.offerPrice = priceDetails.offerPrice || 0;
                parkingData.actualPrice = priceDetails.actualPrice || 0;
            } else if(parkingData.cardNumber != "" && !validCard){
                parkingData.finalPrice = priceDetails.finalPrice || 0;
                parkingData.offerPrice = priceDetails.offerPrice || 0;
                parkingData.actualPrice = priceDetails.actualPrice || 0;
            } else {
                parkingData.finalPrice = 0;
                parkingData.offerPrice = priceDetails.actualPrice || 0;
                parkingData.actualPrice = priceDetails.actualPrice || 0;
            }
            const result = await Parkings.findOneAndUpdate({ _id: parking_details._id }, { $set: parkingData }, { new: true }).exec();
            logger.warn({message: `Exit Parking: Parking status updating in DB(parkings collection) - ${parkingData.parkingId} / ${parkingData.vehicleNumber} / ${parkingData.sessionId}`, level: 'api'});
            if(parkingData.status== 'Cancelled'){
                const sessionUpdate = await Sessions.update({ _id: parkingData.sessionId }, { $inc: { cancelled: 1 }}).exec();
            } else {
                const sessionUpdate = await Sessions.update({ _id: parkingData.sessionId }, { $inc: { exits: 1, parkingsAmount: parkingData.finalPrice }}).exec();
            }
            logger.warn({message: `Exit Parking: Parking data updated in DB(sessions collection) - ${parkingData.parkingId} / ${parkingData.vehicleNumber} / ${parkingData.sessionId}`, level: 'api'});
            if (!_.isEmpty(result)) {
                let smsResult = "SMS service not enabled for exit parking";
                if(parkingData.sendSMS){
                    parking_details['exitDate'] = moment(new Date(parkingData.dateAndTimeOfExit)).format('DD.MM.YYYY')
                    parking_details['exitTime'] = moment(new Date(parkingData.dateAndTimeOfExit)).format('HH:mm:ss')
                    parking_details['finalPrice'] = parkingData.finalPrice;
                    smsResult = await SMS.sendSMS('parking_exit',parking_details, parkingData.phone);
                    logger.warn({message: `Exit Parking: Exit SMS sent to customer phone - ${parkingData.parkingId} / ${parkingData.phone} / ${parkingData.vehicleNumber} / ${parkingData.sessionId}`, level: 'api'});
                } 
                if(parkingData.cardNumber != "" && validCard){
                    logger.warn({message: `Exit Parking Success with valid Monthly card - ${parkingData.parkingId} / ${parkingData.vehicleNumber} / ${parkingData.sessionId}`, level: 'api'});
                    return res.json({ status: "success", message: "Exit Success", data: result });
                }else if(parkingData.cardNumber != "" && !validCard){
                    logger.warn({message: `Exit Parking Success, Card Already Expired - ${parkingData.parkingId} / ${parkingData.vehicleNumber} / ${parkingData.sessionId}`, level: 'api'});
                    return res.json({ status: "success", message: "Card Already Expired, Please renew the card. Vehicle Exited Successfully!", data: result });
                } else{
                    logger.warn({message: `Exit Parking Success, without monthly card - ${parkingData.parkingId} / ${parkingData.vehicleNumber} / ${parkingData.sessionId}`, level: 'api'});
                    return res.json({ status: "success", message: "Exit Success", data: result });
                }
            } else {
                logger.error({message: `Exit parking error: Parking data not found on update - ${parkingData.parkingId} / ${parkingData.vehicleNumber} / ${parkingData.sessionId}`, level: 'api'})
                throw new ApplicationError("Parking data not found on update", 500, "");
            }
        }
    } catch (e) {
        logger.error({message: `Exit parking error: ${e.message}- ${parkingData.parkingId} / ${parkingData.vehicleNumber} / ${parkingData.sessionId}`, level: 'api'})
        return res.json({ status: "fail", message: e.message });
    }
}

exports.getParkingHistory = async function (req, res) {
    try {
        const vehicleNumber = req.query.vehicleNumber;
        var result = await Parkings.find({ 'vehicleNumber': vehicleNumber }).select('vehicleName parkingId dateAndTimeOfParking dateAndTimeOfExit').exec();
        if (!_.isEmpty(result)) {
            return res.json({ status: "success", message: "Parking Details", data: result });
        } else {
            throw new ApplicationError("No Parking data found", 500, "");
        }
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

//This API is for offline data should be called on every parking entry for the stand
exports.getParkingsEntryHisotry = async function (req, res) {
    try {
        const { standId, ownerId, status } = req.query;
        let now = moment();
        let currentYear = now.format("YY")
        let currentMonth = now.format("MM")    
        const parkingsCount = await Parkings.countDocuments({}).exec();
        const lastTicket= "PKT" + currentYear + currentMonth + parkingsCount;
        var result = await Parkings.find({ownerId,standId, status}).exec();
        if (!_.isEmpty(result)) {
            return res.json({ status: "success", message: "Parking Details", data: result, lastTicket });
        } else {
            throw new ApplicationError("No Parking data found", 500, "");
        }
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

exports.deleteParking = async function (req, res) {
    var id = req.params.id;
    if (_.isEmpty(id)) {
        return res.json({ status: "fail", message: "Enter required parameters" });
    }
    try {
        var response = await Parkings.findOne({ _id: id }).exec();
        if (_.isEmpty(response)) {
            throw new ApplicationError("Parking data found", 500, "");
        } else {
            var result = await Parkings.remove({ _id: id }).exec();
            if (!_.isEmpty(result)) {
                return res.json({ status: "success", message: "Parking Deleted successfully!.." });
            } else {
                throw new ApplicationError("Parking data not removed", 500, "");
            }
        }
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

exports.getDetailsByPhone = async (req, res) => {
    const phoneNumber = req.query.phoneNumber;
    const standId = req.query.standId;
    try {
        let result = await Parkings.aggregate([
            {
                $match: { $and: [{ 'phone': { $regex:  phoneNumber, $options: 'i' }},{standId: standId}] }
              },
            { "$project" : {
                "_id" : 1,
                "vehicleNumber" : 1,
                "vehicleName": 1,
                "vehicleType": 1,
                "cardNumber": 1,
                "customerName": 1,
                "customerAddress": 1,
                "phone": 1,
                "status" : 1,
                "parkingId" : 1,
                "ownerId" : 1,
                "standId" : 1,
                "location" : 1,
                "dateAndTimeOfParking" : 1,
                "order" : {
                    "$cond" : {
                        if : { "$eq" : ["$status", "Entry"] }, then : 1,
                        else  : { "$cond" : {
                            "if" : { "$eq" : ["$status", "Exit"] }, then : 2, 
                            else  : 3
                            }
                        }
                    }
                }
            } }, 
            {"$sort" : {"order" : 1} }
        ]);
        let uniqueArray = [];
        let data = result.filter(item => {
            if(!uniqueArray.includes(item.vehicleNumber)){
                uniqueArray.push(item.vehicleNumber);
                return true;
            } else{
                return false;
            }
        })
        if (data.length) {
            return res.json({ status: "success", message: "Parking Details", data });
        } else {
            return res.json({ status: "success", message: "No parkings data found" });
        }
    } catch (e) {
        console.log("error: ",e);

        return res.json({ status: "fail", message: e.message });
    }
}

exports.getParkedVehicles = async function (req, res) {
    const vehicleNumber = req.query.vehicleNumber;
    const phone = req.query.phone;
    try {
        var result = await Parkings.find({ $and: [{ 'vehicleNumber': vehicleNumber}, { status: 'Entry' },{phone: phone}] }).exec();
        if (!_.isEmpty(result)) {
            return res.json({ status: "success", message: "Parking Details", data: result });
        } else {
            throw new ApplicationError("No Parking data found", 500, "");
        }
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

exports.getSlipLostVehiclesByOwner = async function (req, res) {
    const ownerId = req.params.ownerId;
    const vehicleNumber = req.query.vehicleNumber;
    const phone = req.query.phone;
    try {
        let query = [{ 'ownerId': ownerId}, { isSlipLost: true }];
        if(vehicleNumber){
            query.push({ 'vehicleNumber': vehicleNumber})
        }
        if(phone){
            query.push({ 'phone': phone})
        }
        // const totalCount = await Parkings.countDocuments({ $and: [query]}).exec();
        var result = await Parkings.find({ $and: query}).exec();
        if (!_.isEmpty(result)) {
            return res.json({ status: "success", message: "Vehicles retrived", data: result });
        } else {
            throw new ApplicationError("No Parking data found", 500, "");
        }
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

exports.slipLostOTP = async function (req, res) {
    const vehicleNumber = req.query.vehicleNumber;
    const phone = req.query.phone;
    try {
        var result = await Parkings.find({ $and: [{ 'vehicleNumber': vehicleNumber}, { status: 'Entry' },{phone: phone}] }).exec();
        if (!_.isEmpty(result)) {
            return res.json({ status: "success", message: "Parking Details", data: result });
        } else {
            throw new ApplicationError("No Parking data found", 500, "");
        }
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

exports.getCardsForParking = async (req,res)=> {
    try{
        const cardId = req.query.cardId;
        const standId = req.query.standId;
        const ownerId = req.query.ownerId;
        const cardData = await Cards.find({$and:[{cardId: { $regex:  cardId, $options: 'i' }}, {standId}, {ownerId}]}).exec();
        return res.json({ status: "success", message: "Parking Details", cardData });
    } catch(e) {
        return res.json({ status: "fail", message: e.message });
    }
}
exports.checkParkingByCard = async (req,res)=> {
    try{
        const cardId = req.query.cardId;
        const standId = req.query.standId;
        const ownerId = req.query.ownerId;
        const parkings = await Parkings.find({$and:[{cardNumber: cardId }, {standId}, {ownerId},{status: 'Entry'}]}).exec();
        return res.json({ status: "success", message: "Parking Details", parkings });
    } catch(e) {
        return res.json({ status: "fail", message: e.message });
    }
}

async function calculateAmount(parkingData,priceRulesResult,cardData) {
    try {
        let priceRulesData = priceRulesResult.vehicleTypes.filter(item => item.vehicletype == parkingData.vehicleType )[0]; 
        let parkingTimeInMins = moment(new Date(parkingData.dateAndTimeOfExit ? parkingData.dateAndTimeOfExit : new Date())).diff(moment(new Date(parkingData.dateAndTimeOfParking)), 'minutes');
        if (cardData) {
            if(moment(new Date(cardData.endDate)).isBefore(new Date()) && moment(new Date(cardData.endDate)).isAfter(new Date(parkingData.dateAndTimeOfParking))){
                parkingTimeInMins = moment(new Date(parkingData.dateAndTimeOfExit ? parkingData.dateAndTimeOfExit : new Date())).diff(moment(new Date(cardData.endDate)), 'minutes');
            }
        }
        let finalAmount = 0;
        let offerPrice = 0;
        const standId = parkingData.standId;
        const standDetails = await Stands.findOne({ "standId": standId }).exec();
        if(standDetails.initialGracePeriod < parkingTimeInMins){
            let numberOfSlots = Math.floor((parkingTimeInMins/ (60 * priceRulesData.duration)));
            let extraTimeThanASlot = 0
            if(numberOfSlots>=1){
            extraTimeThanASlot = (parkingTimeInMins % (60*priceRulesData.duration)) > standDetails.lateGracePeriod ? 1 : 0;
            }
            if (numberOfSlots + extraTimeThanASlot > 0) {
              if (priceRulesData.fixedPriceFor1stHour == 0 && priceRulesData.forEveryNextHour == 0) {
                finalAmount = (numberOfSlots + extraTimeThanASlot) * Number(priceRulesData.amount);
              } else {
                let forEveryNextSlot = (numberOfSlots + extraTimeThanASlot - 1) * Number(priceRulesData.forEveryNextHour)
                finalAmount = Number(priceRulesData.fixedPriceFor1stHour) + forEveryNextSlot;
                offerPrice = Math.abs(Number(priceRulesData.fixedPriceFor1stHour) - Number(priceRulesData.forEveryNextHour)) * (numberOfSlots + extraTimeThanASlot - 1);
              }
            } else {
                if (priceRulesData.fixedPriceFor1stHour == 0 && priceRulesData.forEveryNextHour == 0) {
                  finalAmount =  Number(priceRulesData.amount);
                } else {
                  finalAmount = Number(priceRulesData.fixedPriceFor1stHour);
                  offerPrice = Math.abs(Number(priceRulesData.fixedPriceFor1stHour) - Number(priceRulesData.forEveryNextHour));
                }
            }
          }
            return {
                finalPrice: Math.floor(finalAmount),
                offerPrice : offerPrice,
                actualPrice: finalAmount + offerPrice
            };
    }
     catch (e) {
        console.log("error: ",e);
        return -1;
    }
}
