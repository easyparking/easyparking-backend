exports.getUserId = function (own_id, role) {
	let now = moment();
	let currentYear = now.format("YY")
	let currentMonth = now.format("MM")
	let next_id;
	if (own_id == 0) {
		if (role == 'employee') { next_id = "E" + currentYear + currentMonth + 1 }
		else if (role == 'owner') { next_id = "W" + currentYear + currentMonth + 1 }
		else if (role == 'customer') { next_id = "C" + currentYear + currentMonth + 1 }
	} else {
		next_id = own_id+1;

		if (role == 'employee') { next_id = "E" + currentYear + currentMonth + next_id }
		else if (role == 'owner') { next_id = "W" + currentYear + currentMonth + next_id }
		else if (role == 'customer') { next_id = "C" + currentYear + currentMonth + next_id }
	}
	return next_id;
}

exports.getStandId = function (code, standId) {

	let now = moment();
	let currentYear = now.format("YY")
	let currentMonth = now.format("MM")
	let  next_id;
	if (standId === 0) {
		next_id = code + currentYear + currentMonth + 1
	} else {
		const count  = standId + 1;
		next_id = code + currentYear + currentMonth + count
	}
	return next_id;
}

exports.getJobApplicantionId = function (id) {

	let now = moment();
	let currentYear = now.format("YY")
	let currentMonth = now.format("MM")
	var next_id;
	if (id == 0) {
		next_id = "JAD" + currentYear + currentMonth + 1
	} else {
		const count = id +1;
		next_id = "JAD" + currentYear + currentMonth + count
	}
	return next_id;
}

exports.getParkingId = function (count, code) {
	let now = moment();
	let currentYear = now.format("YY")
	let currentMonth = now.format("MM")
	let next_id;
	if (count == 0) {
		next_id = code + currentYear + currentMonth + 1;
	} else {
		const newId = count+1;
		next_id = code + currentYear + currentMonth + newId;
	}
	return next_id;
}

exports.getCardId = function (cardId, code) {
	let now = moment();
	let currentYear = now.format("YY")
	let currentMonth = now.format("MM")
	let next_id;
	if (cardId == 0) {
		next_id = "MC/" + code +currentYear + currentMonth + 1
	} else {
		const count = cardId +1;
		next_id = "MC/" + code + currentYear + currentMonth + count
	}
	return next_id;
}

exports.createPriceRules = function (standId, ownerId) {
	let array = ['bicycle', 'twoWheeler', 'threeWheeler', 'fourWheeler'];
	const vehcileTypes = array.map(vechileType => {
		let vehicle_obj = {
			"vehicletype": vechileType,
			"amount": 0,
			"duration": 0,
			"cardActualPrice": 0,
			"cardOfferPrice": 0,
			"cardMaxDiscount": 0,
		}
		return vehicle_obj;
	});
	const PRICES_RULES = {
		"standId": standId,
		"ownerId": ownerId,
		"vehicleTypes": vehcileTypes
	}
	return PRICES_RULES;
}

exports.getOwnerCountsQuery = () => {
	return [{
		"$facet": {
			"Active": [{
				$match: {
					$and: [{
						status: 'Active'
					}, {
						role: "owner"
					}]
				}
			}, {
				$count: "Active"
			}],
			"Total": [{
				"$match": {
					$or: [{
						role: "owner"
					}]
				}
			},
			{
				$count: "Total"
			}
			]
		}
	},
	{
		"$project": {
			"Active": {
				"$arrayElemAt": [
					"$Active.Active",
					0
				]
			},
			"Total": {
				"$arrayElemAt": [
					"$Total.Total",
					0
				]
			}
		}
	}
	]
}

//Admin Dashboard Queries
exports.getDashboardVehicleQuery = () => {
	return [{
		"$facet": {
			"Exit": [{
				$match: {
					$and: [{
						status: 'Exit'
					}]
				}
			}, {
				$count: "Exit"
			}],
			"Cancelled": [{
				$match: {
					$and: [{
						status: 'Cancelled'
					}]
				}
			}, {
				$count: "Cancelled"
			}],
			"Total": [{
				"$match": {
					$or: [{
						status: 'Exit'
					},{
						status: 'Cancelled'
					}, {
						status: 'Entry'
					}]
				}
			},
			{
				$count: "Total"
			}
			]
		}
	},
	{
		"$project": {
			"Exit": {
				"$arrayElemAt": [
					"$Exit.Exit",
					0
				]
			},
			"Cancelled": {
				"$arrayElemAt": [
					"$Cancelled.Cancelled",
					0
				]
			},
			"Total": {
				"$arrayElemAt": [
					"$Total.Total",
					0
				]
			}
		}
	}
	]
}

exports.getOnlineCardsQuery = () => {
	return [{
		"$facet": {
			"OnlineCards": [{
				$match: {
					$and: [{
						allowOnlineCards: true
					}]
				}
			}, {
				$count: "OnlineCards"
			}],
			"Total": [{
				"$match": {
					$or: [{
						allowOnlineCards: true
					}, {
						allowOnlineCards: false
					}]
				}
			},
			{
				$count: "Total"
			}
			]
		}
	},
	{
		"$project": {
			"OnlineCards": {
				"$arrayElemAt": [
					"$OnlineCards.OnlineCards",
					0
				]
			},
			"Total": {
				"$arrayElemAt": [
					"$Total.Total",
					0
				]
			}
		}
	}
	]
}

exports.getWantedVehiclesQuery = () => {
	return [{
		"$facet": {
			"wanted": [{
				$match: {
					$or: [{
						isMatched: false
					},{
						isMatched: true
					}]
				}
			}, {
				$count: "wanted"
			}],
			"matched": [{
				"$match": {
					$and: [{
						isMatched: true
					}]
				}
			},
			{
				$count: "matched"
			}
			]
		}
	},
	{
		"$project": {
			"wanted": {
				"$arrayElemAt": [
					"$wanted.wanted",
					0
				]
			},
			"matched": {
				"$arrayElemAt": [
					"$matched.matched",
					0
				]
			}
		}
	}
	]
}

//Owner Dashboard
exports.getOwnerVehiclesQuery = (ownerId, date) => {
	return [{
		"$facet": {
			"bicycle": [{
				$match: {
					$and: [{
						vehicleType: "bicycle"
					}, {
						ownerId: ownerId
					},{
						"dateAndTimeOfParking": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "bicycle"
			}],
			"bicycleExit": [{
				$match: {
					$and: [{
						vehicleType: "bicycle"
					}, {
						ownerId: ownerId
					}, {
						status: 'Exit'
					},{
						"dateAndTimeOfExit": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "bicycleExit"
			}],
			"twoWheeler": [{
				$match: {
					$and: [{
						vehicleType: "twoWheeler"
					}, {
						ownerId: ownerId
					},{
						"dateAndTimeOfParking": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "twoWheeler"
			}],
			"twoWheelerExit": [{
				$match: {
					$and: [{
						vehicleType: "twoWheeler"
					}, {
						ownerId: ownerId
					}, {
						status: 'Exit'
					},{
						"dateAndTimeOfExit": 
						{
							$gte: date
						}
					}
					]
				}
			}, {
				$count: "twoWheelerExit"
			}],
			"threeWheeler": [{
				$match: {
					$and: [{
						vehicleType: "threeWheeler"
					}, {
						ownerId: ownerId
					},{
						"dateAndTimeOfParking": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "threeWheeler"
			}],
			"threeWheelerExit": [{
				$match: {
					$and: [{
						vehicleType: "threeWheeler"
					}, {
						ownerId: ownerId
					}, {
						status: 'Exit'
					},{
						"dateAndTimeOfExit": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "threeWheelerExit"
			}],
			"fourWheeler": [{
				$match: {
					$and: [{
						vehicleType: "fourWheeler"
					}, {
						ownerId: ownerId
					},{
						"dateAndTimeOfParking": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "fourWheeler"
			}],
			"fourWheelerExit": [{
				$match: {
					$and: [{
						vehicleType: "fourWheeler"
					}, {
						ownerId: ownerId
					}, {
						status: 'Exit'
					},{
						"dateAndTimeOfExit": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "fourWheelerExit"
			}],
		}
	},
	{
		"$project": {
			"bicycle": {
				"$arrayElemAt": [
					"$bicycle.bicycle",
					0
				]
			},
			"bicycleExit": {
				"$arrayElemAt": [
					"$bicycleExit.bicycleExit",
					0
				]
			},
			"twoWheeler": {
				"$arrayElemAt": [
					"$twoWheeler.twoWheeler",
					0
				]
			},
			"twoWheelerExit": {
				"$arrayElemAt": [
					"$twoWheelerExit.twoWheelerExit",
					0
				]
			},
			"threeWheeler": {
				"$arrayElemAt": [
					"$threeWheeler.threeWheeler",
					0
				]
			},
			"threeWheelerExit": {
				"$arrayElemAt": [
					"$threeWheelerExit.threeWheelerExit",
					0
				]
			},
			"fourWheeler": {
				"$arrayElemAt": [
					"$fourWheeler.fourWheeler",
					0
				]
			},
			"fourWheelerExit": {
				"$arrayElemAt": [
					"$fourWheelerExit.fourWheelerExit",
					0
				]
			},
		}
	}
	]
}

//offline cards
exports.getCardsByStandQuery = (ownerId, standId, date) => {
	return [{
		"$facet": {
			"bicycleCount": [{
				$match: {
					$and: [{
						vehicleType: "bicycle"
					}, {
						ownerId: ownerId
					}, {
						standId: standId
					}, {
						type: "offline"
					},{ $or:[{
								"createdDate": 
								{
									$gte: date
								}
							},
							{
								"renewedDate": 
								{
									$gte: date
								}
							}
						]
					}]
				}
			}, {
				$count: "bicycleCount"
			}],
			"twoWheelerCount": [{
				$match: {
					$and: [{
						vehicleType: "twoWheeler"
					}, {
						ownerId: ownerId
					}, {
						standId: standId
					}, {
						type: "offline"
					},{ $or:[{
								"createdDate": 
								{
									$gte: date
								}
							},
							{
								"renewedDate": 
								{
									$gte: date
								}
							}
						]
					}]
				}
			}, {
				$count: "twoWheelerCount"
			}],
			"threeWheelerCount": [{
				$match: {
					$and: [{
						vehicleType: "threeWheeler"
					}, {
						ownerId: ownerId
					}, {
						standId: standId
					}, {
						type: "offline"
					},{ $or:[{
								"createdDate": 
								{
									$gte: date
								}
							},
							{
								"renewedDate": 
								{
									$gte: date
								}
							}
						]
					}]
				}
			}, {
				$count: "threeWheelerCount"
			}],
			"fourWheelerCount": [{
				$match: {
					$and: [{
						vehicleType: "fourWheeler"
					}, {
						ownerId: ownerId
					}, {
						standId: standId
					}, {
						type: "offline"
					},{ $or:[{
								"createdDate": 
								{
									$gte: date
								}
							},
							{
								"renewedDate": 
								{
									$gte: date
								}
							}
						]
					}]
				}
			}, {
				$count: "fourWheelerCount"
			}]
		}
	},
	{
		"$project": {
			"bicycleCount": {
				"$arrayElemAt": [
					"$bicycleCount.bicycleCount",
					0
				]
			},
			"twoWheelerCount": {
				"$arrayElemAt": [
					"$twoWheelerCount.twoWheelerCount",
					0
				]
			},
			"threeWheelerCount": {
				"$arrayElemAt": [
					"$threeWheelerCount.threeWheelerCount",
					0
				]
			},
			"fourWheelerCount": {
				"$arrayElemAt": [
					"$fourWheelerCount.fourWheelerCount",
					0
				]
			}
		}
	}
	]
}

//online cards
exports.getOnlineCardsByStandQuery = (ownerId, standId, date) => {
	return [{
		"$facet": {
			"bicycleCount": [{
				$match: {
					$and: [{
						vehicleType: "bicycle"
					}, {
						ownerId: ownerId
					}, {
						standId: standId
					}, {
						type: "online"
					},{
						"createdDate": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "bicycleCount"
			}],
			"twoWheelerCount": [{
				$match: {
					$and: [{
						vehicleType: "twoWheeler"
					}, {
						ownerId: ownerId
					}, {
						standId: standId
					}, {
						type: "online"
					},{
						"createdDate": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "twoWheelerCount"
			}],
			"threeWheelerCount": [{
				$match: {
					$and: [{
						vehicleType: "threeWheeler"
					}, {
						ownerId: ownerId
					}, {
						standId: standId
					}, {
						type: "online"
					},{
						"createdDate": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "threeWheelerCount"
			}],
			"fourWheelerCount": [{
				$match: {
					$and: [{
						vehicleType: "fourWheeler"
					}, {
						ownerId: ownerId
					}, {
						standId: standId
					}, {
						type: "online"
					},{
						"createdDate": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "fourWheelerCount"
			}]
		}
	},
	{
		"$project": {
			"bicycleCount": {
				"$arrayElemAt": [
					"$bicycleCount.bicycleCount",
					0
				]
			},
			"twoWheelerCount": {
				"$arrayElemAt": [
					"$twoWheelerCount.twoWheelerCount",
					0
				]
			},
			"threeWheelerCount": {
				"$arrayElemAt": [
					"$threeWheelerCount.threeWheelerCount",
					0
				]
			},
			"fourWheelerCount": {
				"$arrayElemAt": [
					"$fourWheelerCount.fourWheelerCount",
					0
				]
			}
		}
	}
	]
}

exports.getVehiclesByStandQuery = (ownerId, standId, date) => {
	return [{
		"$facet": {
			"bicycle": [{
				$match: {
					$and: [{
						vehicleType: "bicycle"
					}, {
						ownerId: ownerId
					}, {
						standId: standId
					},{
						"createdDate": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "bicycle"
			}],
			"bicycleExit": [{
				$match: {
					$and: [{
						vehicleType: "bicycle"
					}, {
						ownerId: ownerId
					}, {
						status: 'Exit'
					}, {
						standId: standId
					},{
						"createdDate": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "bicycleExit"
			}],
			"bicycleCancelled": [{
				$match: {
					$and: [{
						vehicleType: "bicycle"
					}, {
						ownerId: ownerId
					}, {
						status: 'Cancelled'
					}, {
						standId: standId
					},{
						"createdDate": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "bicycleCancelled"
			}],
			"twoWheeler": [{
				$match: {
					$and: [{
						vehicleType: "twoWheeler"
					}, {
						ownerId: ownerId
					}, {
						standId: standId
					},{
						"createdDate": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "twoWheeler"
			}],
			"twoWheelerExit": [{
				$match: {
					$and: [{
						vehicleType: "twoWheeler"
					}, {
						ownerId: ownerId
					}, {
						status: 'Exit'
					}, {
						standId: standId
					},{
						"createdDate": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "twoWheelerExit"
			}],
			"twoWheelerCancelled": [{
				$match: {
					$and: [{
						vehicleType: "twoWheeler"
					}, {
						ownerId: ownerId
					}, {
						status: 'Cancelled'
					}, {
						standId: standId
					},{
						"createdDate": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "twoWheelerCancelled"
			}],
			"threeWheeler": [{
				$match: {
					$and: [{
						vehicleType: "threeWheeler"
					}, {
						ownerId: ownerId
					}, {
						standId: standId
					},{
						"createdDate": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "threeWheeler"
			}],
			"threeWheelerExit": [{
				$match: {
					$and: [{
						vehicleType: "threeWheeler"
					}, {
						ownerId: ownerId
					}, {
						status: 'Exit'
					}, {
						standId: standId
					},{
						"createdDate": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "threeWheelerExit"
			}],
			"threeWheelerCancelled": [{
				$match: {
					$and: [{
						vehicleType: "threeWheeler"
					}, {
						ownerId: ownerId
					}, {
						status: 'Cancelled'
					}, {
						standId: standId
					},{
						"createdDate": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "threeWheelerCancelled"
			}],
			"fourWheeler": [{
				$match: {
					$and: [{
						vehicleType: "fourWheeler"
					}, {
						ownerId: ownerId
					}, {
						standId: standId
					},{
						"createdDate": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "fourWheeler"
			}],
			"fourWheelerExit": [{
				$match: {
					$and: [{
						vehicleType: "fourWheeler"
					}, {
						ownerId: ownerId
					}, {
						status: 'Exit'
					}, {
						standId: standId
					},{
						"createdDate": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "fourWheelerExit"
			}],
			"fourWheelerCancelled": [{
				$match: {
					$and: [{
						vehicleType: "fourWheeler"
					}, {
						ownerId: ownerId
					}, {
						status: 'Cancelled'
					}, {
						standId: standId
					},{
						"createdDate": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "fourWheelerCancelled"
			}]
		}
	},
	{
		"$project": {
			"bicycle": {
				"$arrayElemAt": [
					"$bicycle.bicycle",
					0
				]
			},
			"bicycleExit": {
				"$arrayElemAt": [
					"$bicycleExit.bicycleExit",
					0
				]
			},
			"bicycleCancelled": {
				"$arrayElemAt": [
					"$bicycleCancelled.bicycleCancelled",
					0
				]
			},
			"twoWheeler": {
				"$arrayElemAt": [
					"$twoWheeler.twoWheeler",
					0
				]
			},
			"twoWheelerExit": {
				"$arrayElemAt": [
					"$twoWheelerExit.twoWheelerExit",
					0
				]
			},
			"twoWheelerCancelled": {
				"$arrayElemAt": [
					"$twoWheelerCancelled.twoWheelerCancelled",
					0
				]
			},
			"threeWheeler": {
				"$arrayElemAt": [
					"$threeWheeler.threeWheeler",
					0
				]
			},
			"threeWheelerExit": {
				"$arrayElemAt": [
					"$threeWheelerExit.threeWheelerExit",
					0
				]
			},
			"threeWheelerCancelled": {
				"$arrayElemAt": [
					"$threeWheelerCancelled.threeWheelerCancelled",
					0
				]
			},
			"fourWheeler": {
				"$arrayElemAt": [
					"$fourWheeler.fourWheeler",
					0
				]
			},
			"fourWheelerExit": {
				"$arrayElemAt": [
					"$fourWheelerExit.fourWheelerExit",
					0
				]
			},
			"fourWheelerCancelled": {
				"$arrayElemAt": [
					"$fourWheelerCancelled.fourWheelerCancelled",
					0
				]
			}
		}
	}
	]
}
//employee dashboard query
exports.getVehiclesByEmployeeQuery = (empId,date) => {
	return [{
		"$facet": {
			"bicycle": [{
				$match: {
					$and: [{
						vehicleType: "bicycle"
					}, {
						$or: [{
							openEmpId: empId
						}, {
							closedEmpId: empId
						}]
					},{
						"createdDate": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "bicycle"
			}],
			"bicycleExit": [{
				$match: {
					$and: [{
						vehicleType: "bicycle"
					}, {
						status: 'Exit'
					}, {
						$or: [{
							openEmpId: empId
						}, {
							closedEmpId: empId
						}]
					},{
						"dateAndTimeOfExit": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "bicycleExit"
			}],
			"twoWheeler": [{
				$match: {
					$and: [{
						vehicleType: "twoWheeler"
					}, {
						$or: [{
							openEmpId: empId
						}, {
							closedEmpId: empId
						}]
					},{
						"createdDate": 
						{
							$gte: date
						}
					},{
						status: 'Entry'
					}]
				}
			}, {
				$count: "twoWheeler"
			}],
			"twoWheelerExit": [{
				$match: {
					$and: [{
						vehicleType: "twoWheeler"
					}, {
						status: 'Exit'
					}, {
						$or: [{
							openEmpId: empId
						}, {
							closedEmpId: empId
						}]
					},{
						"dateAndTimeOfExit": 
						{
							$gte: date
						}
					}
					]
				}
			}, {
				$count: "twoWheelerExit"
			}],
			"threeWheeler": [{
				$match: {
					$and: [{
						vehicleType: "threeWheeler"
					}, {
						$or: [{
							openEmpId: empId
						}, {
							closedEmpId: empId
						}]
					},{
						"createdDate": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "threeWheeler"
			}],
			"threeWheelerExit": [{
				$match: {
					$and: [{
						vehicleType: "threeWheeler"
					},{
						status: 'Exit'
					}, {
						$or: [{
							openEmpId: empId
						}, {
							closedEmpId: empId
						}]
					},{
						"dateAndTimeOfExit": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "threeWheelerExit"
			}],
			"fourWheeler": [{
				$match: {
					$and: [{
						vehicleType: "fourWheeler"
					}, {
						$or: [{
							openEmpId: empId
						}, {
							closedEmpId: empId
						}]
					},{
						"createdDate": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "fourWheeler"
			}],
			"fourWheelerExit": [{
				$match: {
					$and: [{
						vehicleType: "fourWheeler"
					}, {
						status: 'Exit'
					}, {
						$or: [{
							openEmpId: empId
						}, {
							closedEmpId: empId
						}]
					},{
						"dateAndTimeOfExit": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "fourWheelerExit"
			}],
		}
	},
	{
		"$project": {
			"bicycle": {
				"$arrayElemAt": [
					"$bicycle.bicycle",
					0
				]
			},
			"bicycleExit": {
				"$arrayElemAt": [
					"$bicycleExit.bicycleExit",
					0
				]
			},
			"twoWheeler": {
				"$arrayElemAt": [
					"$twoWheeler.twoWheeler",
					0
				]
			},
			"twoWheelerExit": {
				"$arrayElemAt": [
					"$twoWheelerExit.twoWheelerExit",
					0
				]
			},
			"threeWheeler": {
				"$arrayElemAt": [
					"$threeWheeler.threeWheeler",
					0
				]
			},
			"threeWheelerExit": {
				"$arrayElemAt": [
					"$threeWheelerExit.threeWheelerExit",
					0
				]
			},
			"fourWheeler": {
				"$arrayElemAt": [
					"$fourWheeler.fourWheeler",
					0
				]
			},
			"fourWheelerExit": {
				"$arrayElemAt": [
					"$fourWheelerExit.fourWheelerExit",
					0
				]
			},
		}
	}
	]
}

exports.getVehiclesByEmployeeStandQuery = (empId,standId, ownerId, date) => {
	return [{
		"$facet": {
			"bicycle": [{
				$match: {
					$and: [{
						vehicleType: "bicycle"
					}, {
						$or: [{
							openEmpId: empId
						}, {
							closedEmpId: empId
						}]
					}, {
						standId: standId
					},{
						"createdDate": 
						{
							$gte: date
						}
					},{
						status: 'Entry'
					}]
				}
			}, {
				$count: "bicycle"
			}],
			"bicyclePending": [{
				$match: {
					$and: [{
						vehicleType: "bicycle"
					}, {
						standId: standId
					},{
						status: 'Entry'
					},{
						"createdDate": 
						{
							$lt: date
						}
					}]
				}
			}, {
				$count: "bicyclePending"
			}],"bicyclePendingOnLogout": [{
				$match: {
					$and: [{
						vehicleType: "bicycle"
					}, {
						standId: standId
					},{
						status: 'Entry'
					}]
				}
			}, {
				$count: "bicyclePendingOnLogout"
			}],
			"bicycleExit": [{
				$match: {
					$and: [{
						vehicleType: "bicycle"
					}, {
						status: 'Exit'
					}, {
						$or: [{
							openEmpId: empId
						}, {
							closedEmpId: empId
						}]
					}, {
						standId: standId
					},{
						"dateAndTimeOfExit": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "bicycleExit"
			}],
			"bicycleCancelled": [{
				$match: {
					$and: [{
						vehicleType: "bicycle"
					}, {
						ownerId: ownerId
					}, {
						status: 'Cancelled'
					}, {
						standId: standId
					},{$or: [{
						"createdDate": 
						{
							$gte: date
						}
					}, {
						"dateAndTimeOfExit": {
							$gte: date
						}
					}]
					}]
				}
			}, {
				$count: "bicycleCancelled"
			}],
			"twoWheeler": [{
				$match: {
					$and: [{
						vehicleType: "twoWheeler"
					}, {
						$or: [{
							openEmpId: empId
						}, {
							closedEmpId: empId
						}]
					}, {
						standId: standId
					},{
						"createdDate": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "twoWheeler"
			}],
			"twoWheelerPending": [{
				$match: {
					$and: [{
						vehicleType: "twoWheeler"
					}, {
						standId: standId
					},{
						status: 'Entry'
					},{
						"createdDate": 
						{
							$lt: date
						}
					}]
				}
			}, {
				$count: "twoWheelerPending"
			}],
			"twoWheelerPendingOnLogout": [{
				$match: {
					$and: [{
						vehicleType: "twoWheeler"
					}, {
						standId: standId
					},{
						status: 'Entry'
					}]
				}
			}, {
				$count: "twoWheelerPendingOnLogout"
			}],
			"twoWheelerExit": [{
				$match: {
					$and: [{
						vehicleType: "twoWheeler"
					}, {
						status: 'Exit'
					}, {
						$or: [{
							openEmpId: empId
						}, {
							closedEmpId: empId
						}]
					}, {
						standId: standId
					},{
						"dateAndTimeOfExit": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "twoWheelerExit"
			}],
			"twoWheelerCancelled": [{
				$match: {
					$and: [{
						vehicleType: "twoWheeler"
					}, {
						ownerId: ownerId
					}, {
						status: 'Cancelled'
					}, {
						standId: standId
					},{$or: [{
						"createdDate": 
						{
							$gte: date
						}
					}, {
						"dateAndTimeOfExit": {
							$gte: date
						}
					}]
					}]
				}
			}, {
				$count: "twoWheelerCancelled"
			}],
			"threeWheeler": [{
				$match: {
					$and: [{
						vehicleType: "threeWheeler"
					}, {
						$or: [{
							openEmpId: empId
						}, {
							closedEmpId: empId
						}]
					}, {
						standId: standId
					},{
						"createdDate": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "threeWheeler"
			}],
			"threeWheelerPending": [{
				$match: {
					$and: [{
						vehicleType: "threeWheeler"
					}, {
						standId: standId
					},{
						status: 'Entry'
					},{
						"createdDate": 
						{
							$lt: date
						}
					}]
				}
			}, {
				$count: "threeWheelerPending"
			}],
			"threeWheelerPendingOnLogout": [{
				$match: {
					$and: [{
						vehicleType: "threeWheeler"
					}, {
						standId: standId
					},{
						status: 'Entry'
					}]
				}
			}, {
				$count: "threeWheelerPendingOnLogout"
			}],
			"threeWheelerExit": [{
				$match: {
					$and: [{
						vehicleType: "threeWheeler"
					},{
						status: 'Exit'
					}, {
						$or: [{
							openEmpId: empId
						}, {
							closedEmpId: empId
						}]
					}, {
						standId: standId
					},{
						"dateAndTimeOfExit": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "threeWheelerExit"
			}],
			"threeWheelerCancelled": [{
				$match: {
					$and: [{
						vehicleType: "threeWheeler"
					}, {
						ownerId: ownerId
					}, {
						status: 'Cancelled'
					}, {
						standId: standId
					},{$or: [{
						"createdDate": 
						{
							$gte: date
						}
					}, {
						"dateAndTimeOfExit": {
							$gte: date
						}
					}]
					}]
				}
			}, {
				$count: "threeWheelerCancelled"
			}],
			"fourWheeler": [{
				$match: {
					$and: [{
						vehicleType: "fourWheeler"
					}, {
						$or: [{
							openEmpId: empId
						}, {
							closedEmpId: empId
						}]
					}, {
						standId: standId
					},{
						"createdDate": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "fourWheeler"
			}],
			"fourWheelerPending": [{
				$match: {
					$and: [{
						vehicleType: "fourWheeler"
					}, {
						standId: standId
					},{
						status: 'Entry'
					},{
						"createdDate": 
						{
							$lt: date
						}
					}]
				}
			}, {
				$count: "fourWheelerPending"
			}],
			"fourWheelerPendingOnLogout": [{
				$match: {
					$and: [{
						vehicleType: "fourWheeler"
					}, {
						standId: standId
					},{
						status: 'Entry'
					}]
				}
			}, {
				$count: "fourWheelerPendingOnLogout"
			}],
			"fourWheelerExit": [{
				$match: {
					$and: [{
						vehicleType: "fourWheeler"
					}, {
						status: 'Exit'
					}, {
						$or: [{
							openEmpId: empId
						}, {
							closedEmpId: empId
						}]
					}, {
						standId: standId
					},{
						"dateAndTimeOfExit": 
						{
							$gte: date
						}
					}]
				}
			}, {
				$count: "fourWheelerExit"
			}],
			"fourWheelerCancelled": [{
				$match: {
					$and: [{
						vehicleType: "fourWheeler"
					}, {
						ownerId: ownerId
					}, {
						status: 'Cancelled'
					}, {
						standId: standId
					},{$or: [{
						"createdDate": 
						{
							$gte: date
						}
					}, {
						"dateAndTimeOfExit": {
							$gte: date
						}
					}]
					}]
				}
			}, {
				$count: "fourWheelerCancelled"
			}],
		}
	},
	{
		"$project": {
			"bicycle": {
				"$arrayElemAt": [
					"$bicycle.bicycle",
					0
				]
			},
			"bicycleExit": {
				"$arrayElemAt": [
					"$bicycleExit.bicycleExit",
					0
				]
			},
			"bicycleCancelled": {
				"$arrayElemAt": [
					"$bicycleCancelled.bicycleCancelled",
					0
				]
			},
			"bicyclePending": {
				"$arrayElemAt": [
					"$bicyclePending.bicyclePending",
					0
				]
			},
			"bicyclePendingOnLogout": {
				"$arrayElemAt": [
					"$bicyclePendingOnLogout.bicyclePendingOnLogout",
					0
				]
			},
			"twoWheelerPending": {
				"$arrayElemAt": [
					"$twoWheelerPending.twoWheelerPending",
					0
				]
			},
			"twoWheelerPendingOnLogout": {
				"$arrayElemAt": [
					"$twoWheelerPendingOnLogout.twoWheelerPendingOnLogout",
					0
				]
			},
			"threeWheelerPending": {
				"$arrayElemAt": [
					"$threeWheelerPending.threeWheelerPending",
					0
				]
			},
			"threeWheelerPendingOnLogout": {
				"$arrayElemAt": [
					"$threeWheelerPendingOnLogout.threeWheelerPendingOnLogout",
					0
				]
			},
			"fourWheelerPending": {
				"$arrayElemAt": [
					"$fourWheelerPending.fourWheelerPending",
					0
				]
			},
			"fourWheelerPendingOnLogout": {
				"$arrayElemAt": [
					"$fourWheelerPendingOnLogout.fourWheelerPendingOnLogout",
					0
				]
			},
			"twoWheeler": {
				"$arrayElemAt": [
					"$twoWheeler.twoWheeler",
					0
				]
			},
			"twoWheelerExit": {
				"$arrayElemAt": [
					"$twoWheelerExit.twoWheelerExit",
					0
				]
			},
			"twoWheelerCancelled": {
				"$arrayElemAt": [
					"$twoWheelerCancelled.twoWheelerCancelled",
					0
				]
			},
			"threeWheeler": {
				"$arrayElemAt": [
					"$threeWheeler.threeWheeler",
					0
				]
			},
			"threeWheelerExit": {
				"$arrayElemAt": [
					"$threeWheelerExit.threeWheelerExit",
					0
				]
			},
			"threeWheelerCancelled": {
				"$arrayElemAt": [
					"$threeWheelerCancelled.threeWheelerCancelled",
					0
				]
			},
			"fourWheeler": {
				"$arrayElemAt": [
					"$fourWheeler.fourWheeler",
					0
				]
			},
			"fourWheelerExit": {
				"$arrayElemAt": [
					"$fourWheelerExit.fourWheelerExit",
					0
				]
			},
			"fourWheelerCancelled": {
				"$arrayElemAt": [
					"$fourWheelerCancelled.fourWheelerCancelled",
					0
				]
			}
		}
	}
	]
}

//Admin offline monthly cards
exports.getOfflineCardsQuery = (onwerId, cardType) => {
	return [{
		"$facet": getVehcilesCountQuery(onwerId, cardType)
	},
	{
		"$project": {
			"newCards": {
				"$arrayElemAt": [
					"$newCards.newCards",
					0
				]
			},
			"renewalCards": {
				"$arrayElemAt": [
					"$renewalCards.renewalCards",
					0
				]
			},
			"expiredCards": {
				"$arrayElemAt": [
					"$expiredCards.expiredCards",
					0
				]
			},
			"bicyclenewCards": {
				"$arrayElemAt": [
					"$bicyclenewCards.bicyclenewCards",
					0
				]
			},
			"bicyclerenewalCards": {
				"$arrayElemAt": [
					"$bicyclerenewalCards.bicyclerenewalCards",
					0
				]
			},
			"bicycleexpiredCards": {
				"$arrayElemAt": [
					"$bicycleexpiredCards.bicycleexpiredCards",
					0
				]
			},
			"twoWheelernewCards": {
				"$arrayElemAt": [
					"$twoWheelernewCards.twoWheelernewCards",
					0
				]
			},
			"twoWheelerrenewalCards": {
				"$arrayElemAt": [
					"$twoWheelerrenewalCards.twoWheelerrenewalCards",
					0
				]
			},
			"twoWheelerexpiredCards": {
				"$arrayElemAt": [
					"$twoWheelerexpiredCards.twoWheelerexpiredCards",
					0
				]
			},
			"threeWheelernewCards": {
				"$arrayElemAt": [
					"$threeWheelernewCards.threeWheelernewCards",
					0
				]
			},
			"threeWheelerrenewalCards": {
				"$arrayElemAt": [
					"$threeWheelerrenewalCards.threeWheelerrenewalCards",
					0
				]
			},
			"threeWheelerexpiredCards": {
				"$arrayElemAt": [
					"$threeWheelerexpiredCards.threeWheelerexpiredCards",
					0
				]
			},
			"fourWheelernewCards": {
				"$arrayElemAt": [
					"$fourWheelernewCards.fourWheelernewCards",
					0
				]
			},
			"fourWheelerrenewalCards": {
				"$arrayElemAt": [
					"$fourWheelerrenewalCards.fourWheelerrenewalCards",
					0
				]
			},
			"fourWheelerexpiredCards": {
				"$arrayElemAt": [
					"$fourWheelerexpiredCards.fourWheelerexpiredCards",
					0
				]
			}
		}
	}
	]
}
exports.getOfflineCardsByStandQuery = (onwerId, standId, cardType) => {
	return [{
		"$facet": getVehcilesCountByStandQuery(onwerId,standId, cardType)
	},
	{
		"$project": {
			"newCards": {
				"$arrayElemAt": [
					"$newCards.newCards",
					0
				]
			},
			"renewalCards": {
				"$arrayElemAt": [
					"$renewalCards.renewalCards",
					0
				]
			},
			"expiredCards": {
				"$arrayElemAt": [
					"$expiredCards.expiredCards",
					0
				]
			},
			"bicyclenewCards": {
				"$arrayElemAt": [
					"$bicyclenewCards.bicyclenewCards",
					0
				]
			},
			"bicyclerenewalCards": {
				"$arrayElemAt": [
					"$bicyclerenewalCards.bicyclerenewalCards",
					0
				]
			},
			"bicycleexpiredCards": {
				"$arrayElemAt": [
					"$bicycleexpiredCards.bicycleexpiredCards",
					0
				]
			},
			"twoWheelernewCards": {
				"$arrayElemAt": [
					"$twoWheelernewCards.twoWheelernewCards",
					0
				]
			},
			"twoWheelerrenewalCards": {
				"$arrayElemAt": [
					"$twoWheelerrenewalCards.twoWheelerrenewalCards",
					0
				]
			},
			"twoWheelerexpiredCards": {
				"$arrayElemAt": [
					"$twoWheelerexpiredCards.twoWheelerexpiredCards",
					0
				]
			},
			"threeWheelernewCards": {
				"$arrayElemAt": [
					"$threeWheelernewCards.threeWheelernewCards",
					0
				]
			},
			"threeWheelerrenewalCards": {
				"$arrayElemAt": [
					"$threeWheelerrenewalCards.threeWheelerrenewalCards",
					0
				]
			},
			"threeWheelerexpiredCards": {
				"$arrayElemAt": [
					"$threeWheelerexpiredCards.threeWheelerexpiredCards",
					0
				]
			},
			"fourWheelernewCards": {
				"$arrayElemAt": [
					"$fourWheelernewCards.fourWheelernewCards",
					0
				]
			},
			"fourWheelerrenewalCards": {
				"$arrayElemAt": [
					"$fourWheelerrenewalCards.fourWheelerrenewalCards",
					0
				]
			},
			"fourWheelerexpiredCards": {
				"$arrayElemAt": [
					"$fourWheelerexpiredCards.fourWheelerexpiredCards",
					0
				]
			}
		}
	}
	]
}

function getVehcilesCountQuery(onwerId, cardType){
	try {
	let query = {
		"newCards": [{
		$match: {
			$and: [{
				type: cardType
			},{
				status: 'Active'
			},{
				ownerId: onwerId
			}]
		}
	}, {
		$count: "newCards"
	}],"renewalCards": [{
		$match: {
			$and: [{
				type: cardType
			},{
				status: 'Renewal'
			},{
				ownerId: onwerId
			}]
		}
	}, {
		$count: "renewalCards"
	}],"expiredCards": [{
		$match: {
			$and: [{
				type: cardType
			},{
				status: 'Expired'
			},{
				ownerId: onwerId
			}]
		}
	}, {
		$count: "expiredCards"
	}],};
	const vehicleTypes = ['bicycle','twoWheeler','threeWheeler','fourWheeler'] //
	for(let i=0; i<vehicleTypes.length;i++){
		let newCards = vehicleTypes[i]+"newCards";
		query[newCards]= [{
				$match: {
					$and: [{
						type: cardType
					},{
						status: 'Active'
					},{
						ownerId: onwerId
					},{
						vehicleType: vehicleTypes[i]
					}]
				}
			}, {
				$count: vehicleTypes[i]+"newCards"
			}];
		let renewalCards = vehicleTypes[i]+"renewalCards";
			query[renewalCards]=  [{
				$match: {
					$and: [{
						type: cardType
					},{
						status: 'Renewal'
					},{
						ownerId: onwerId
					},{
						vehicleType: vehicleTypes[i]
					}]
				}
			}, {
				$count: vehicleTypes[i]+"renewalCards"
			}]
		let expiredCards = vehicleTypes[i]+"expiredCards";
			query[expiredCards]= [{
				$match: {
					$and: [{
						type: cardType
					},{
						status: 'Expired'
					},{
						ownerId: onwerId
					},{
						vehicleType: vehicleTypes[i]
					}]
				}
			}, {
				$count: vehicleTypes[i]+"expiredCards"
			}]
	}
	return query;
} catch(e){
	console.log("eooro", e);
}
}

function getVehcilesCountByStandQuery(onwerId, standId, cardType){
	try {
	let query = {
		"newCards": [{
		$match: {
			$and: [{
				type: cardType
			},{
				status: 'Active'
			},{
				ownerId: onwerId
			},{
				standId: standId
			}]
		}
	}, {
		$count: "newCards"
	}],"renewalCards": [{
		$match: {
			$and: [{
				type: cardType
			},{
				status: 'Renewal'
			},{
				ownerId: onwerId
			},{
				standId: standId
			}]
		}
	}, {
		$count: "renewalCards"
	}],"expiredCards": [{
		$match: {
			$and: [{
				type: cardType
			},{
				status: 'Expired'
			},{
				ownerId: onwerId
			},{
				standId: standId
			}]
		}
	}, {
		$count: "expiredCards"
	}],};
	const vehicleTypes = ['bicycle','twoWheeler','threeWheeler','fourWheeler'] //
	for(let i=0; i<vehicleTypes.length;i++){
		let newCards = vehicleTypes[i]+"newCards";
		query[newCards]= [{
				$match: {
					$and: [{
						type: cardType
					},{
						status: 'Active'
					},{
						ownerId: onwerId
					},{
						standId: standId
					},{
						vehicleType: vehicleTypes[i]
					}]
				}
			}, {
				$count: vehicleTypes[i]+"newCards"
			}];
		let renewalCards = vehicleTypes[i]+"renewalCards";
			query[renewalCards]=  [{
				$match: {
					$and: [{
						type: cardType
					},{
						status: 'Renewal'
					},{
						ownerId: onwerId
					},{
						standId: standId
					},{
						vehicleType: vehicleTypes[i]
					}]
				}
			}, {
				$count: vehicleTypes[i]+"renewalCards"
			}]
		let expiredCards = vehicleTypes[i]+"expiredCards";
			query[expiredCards]= [{
				$match: {
					$and: [{
						type: cardType
					},{
						status: 'Expired'
					},{
						ownerId: onwerId
					},{
						standId: standId
					},{
						vehicleType: vehicleTypes[i]
					}]
				}
			}, {
				$count: vehicleTypes[i]+"expiredCards"
			}]
	}
	return query;
} catch(e){
	console.log("eooro", e);
}
}