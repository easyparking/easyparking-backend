const SMS = require('./SMS');
exports.sendDownloadUrl = async (req, res) => {
    try {
        const mobile = req.params.mobileNumber;
        if (_.isEmpty(mobile)) {
            return res.json({ status: 'success', message: "Mobile Number is required" })
        }
        const smsResult = await SMS.sendSMS('app_registration','',mobile);
        if (!_.isEmpty(smsResult)) {
            const result = await AppDownloads.create({ mobileNumber: mobile });
            return res.json({ status: 'success', message: "Download url sent to mobile Number.", data: smsResult })
        }
    } catch (e) {
        return res.json({ status: 'fail', message: e.message })
    }
}