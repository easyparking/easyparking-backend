exports.getOwners = async function (req, res) {
    var size = req.body.size;
    var pageNo = req.body.pageNo;
    var search_text = req.body.searchText
    if (pageNo < 0 || pageNo === 0) {
        return res.json({ status: "fail", message: "invalid page number" });
    }
    var skip = size * (pageNo - 1);
    var limit = size;
    if (search_text) {
        var query = {
            $and: [
                { role: 'owner' },
                {
                    $or: [
                        { 'userName': { $regex: search_text, $options: 'i' } },
                        { 'ownerId': { $regex: search_text, $options: 'i' } },
                        { 'name': { $regex: search_text, $options: 'i' } },
                        { 'status': { $regex: search_text, $options: 'i' } },
                        { 'emailId': { $regex: search_text, $options: 'i' } },
                        { 'phoneNumber': { $regex: search_text, $options: 'i' } },
                        { 'planType': { $regex: search_text, $options: 'i' } },
                    ]
                }
            ]
        }
    }
    try {
        let result;
        if (search_text) {
            result = await Users.find(query, { auth: 0 }).skip(skip).limit(limit).sort({ 'createdDate': -1 }).exec();
            if (_.isEmpty(result)) {
                throw new ApplicationError("Owners data not found", 500, "");
            }
        } else {
            result = await Users.find({ role: "owner" }, { auth: 0 }).skip(skip).limit(limit).sort({ 'createdDate': -1 }).exec();
            if (_.isEmpty(result)) {
                throw new ApplicationError("Owners data not found", 500, "");
            }
        }
        return res.json({ status: "success", message: "user Data", data: result });
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

exports.getActiveAccounts = async function (req, res) {
    try {
        query = helper.getOwnerCountsQuery();
        const activeAccounts = await Users.aggregate(query).exec();
        let count = {
            Active: activeAccounts.length && activeAccounts[0].Active ? activeAccounts[0].Active : 0,
            Total: activeAccounts.length && activeAccounts[0].Total ? activeAccounts[0].Total : 0,
        };
        return res.json({ status: "success", count: count });
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

exports.getOwnerNames = async function (req, res) {
    try {
        var ownerslist = await Users.find({ role: "owner" }).exec();
        if (!_.isEmpty(ownerslist)) {
            return res.json({ status: "success", message: "List of Owner Names", data: ownerslist });
        } else {
            throw new ApplicationError("Owners data not found", 500, "");
        }
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

exports.updateStatus = async function (req, res) {
    var updateStatus = req.body.status
    var ownerId = req.body.ownerId
    try {
        if (_.isEmpty(ownerId) || _.isEmpty(updateStatus)) {
            throw new ApplicationError("Missing require parameters", 500, "");
        }
        var ownerslist = await Users.findOne({ ownerId: ownerId }).exec();
        if (_.isEmpty(ownerslist)) {
            throw new ApplicationError("Owner data not found", 500, "");
        } else {
            var update = await Users.updateMany({ ownerId: ownerId }, { $set: { 'status': updateStatus } })
            var updateStand = await Stands.updateMany({ ownerId: ownerId }, { $set: { 'status': updateStatus } })
            return res.json({ status: 'success', data: update })

        }

    } catch (e) {
        return res.json({ status: 'fail', message: e.message })
    }
}

exports.updateOwner = async function (req, res) {

    var new_password = req.body.password ? req.body.password : undefined;
    var userName = req.body.userName;
    // var id = mongoose.mongo.ObjectId(req.params.id);
    var ownerId = req.body.ownerId;
    var phoneNumber = req.body.phoneNumber ? req.body.phoneNumber : undefined;
    var name = req.body.name ? req.body.name : undefined;
    if (_.isEmpty(userName) || _.isEmpty(ownerId)) {
        return res.json({ status: "fail", message: "Enter required parameters" });
    }
    try {
        let user = await Users.findOne({ "ownerId": ownerId }).exec();
        if (_.isEmpty(user)) {
            throw new ApplicationError("Owner data not found", 500, "");
        } else if(user && user.status === 'Inactive') {
            throw new ApplicationError("User Account inactive. Please activate user account", 500, "");
        } else if (userName != user.userName) {
            let user_data = await Users.findOne({ "userName": userName }).exec();
            if (!_.isEmpty(user_data)) {
                throw new ApplicationError("User Name already registered", 500, "");
            }
        } else {
            //  console.log("do nothing")
        }
        if (new_password) {
            var saltKey = Math.round((new Date().valueOf() * Math.random())) + '';
            var password = crypto.createHmac('sha512', saltKey).update(new_password).digest('hex');
        }
        var result = await Users.update({ "ownerId": ownerId }, {
            $set:
            {
                // 'auth.password': password,
                // 'auth.salt': saltKey,
                name: name ? name : user.name,
                phoneNumber: phoneNumber ? phoneNumber : user.phoneNumber,
                onlinePaymentPercentage: req.body.onlinePaymentPercentage,
                emailId: req.body.emailId,
                planType: req.body.planType,
                userName: req.body.userName
            }
        }).exec();

        return res.json({ status: "success", message: " Owner data updated successfully!.." });

    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}
