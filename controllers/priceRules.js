exports.getPriceRules = async function (req, res) {
    try {
        if (_.isEmpty(req.query.standId)) {
            throw new ApplicationError("stand Id is required parameter", 500, "");
        }
        var priceRules = await PriceRules.find({ standId: req.query.standId }).exec();
        if (!_.isEmpty(priceRules)) {
            return res.json({ status: 'success', data: priceRules })
        } else {
            return res.json({ status: 'success', message: 'No data found!' })
        }
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

exports.addUpdatePriceRules = async function (req, res) {
    const priceRulesData = req.body;
    try {
        if (_.isEmpty(priceRulesData.standId)) {
            throw new ApplicationError("Stand Id is required parameter", 500, "");
        }
        const stands = await Stands.find({ "standId": priceRulesData.standId }).sort({ '_id': 1 }).exec();
        if (_.isEmpty(stands)) {
            throw new ApplicationError("No stands found with given stand Id", 500, "");
        }


        const priceRules = await PriceRules.find({ "standId": priceRulesData.standId }).sort({ '_id': 1 }).exec();
        let allowedVehicles= [];
        priceRulesData.vehicleTypes.forEach(element => {
            if(element.amount > 0){
                allowedVehicles.push(element.vehicletype);
            }
        });
        if (_.isEmpty(priceRules)) {
            var result = await PriceRules.create(priceRulesData);     
            const updateStandData = await Stands.update({ "standId": priceRulesData.standId },{ $set: {allowedVehicles} }).exec();
            return res.json({ status: 'Sucess', message: 'Successfully added prices Rules' });

        } else {
            var result = await PriceRules.update({ "standId": priceRulesData.standId }, { $set: priceRulesData }).exec();
            const updateStandData = await Stands.update({ "standId": priceRulesData.standId },{ $set: {allowedVehicles} }).exec();
            return res.json({ status: 'Sucess', message: 'Successfully updated prices Rules' })
        }
    } catch (e) {
        return res.json({ status: 'fail', message: e.message })
    }
}