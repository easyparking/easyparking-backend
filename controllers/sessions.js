const SMS = require('./SMS');
const { generateEmployeeReports } = require('./reports')
const logger = require('../config/logger');
exports.createSession = async (req,res) => {
    try{
        const requestBody = req.body;
        const lastLoginSession = requestBody.start_time;
        if(_.isEmpty(requestBody.userName)){
            return res.json({ status: "fail", message: "Enter required parameters" });
        }
        const pendingonLogin = await Parkings.countDocuments({$and:[{
            status: 'Entry'
        }, {
            dateAndTimeOfParking: {
                $lt: new Date(requestBody.start_time)
            }
        },{
            ownerId: requestBody.ownerId
        },{
            standId: requestBody.standId
        }]}).exec();
        const query = getPendingOnLoginVehicles(requestBody);
        const parkingsResult = await Parkings.aggregate(query).exec();
        
        let pendingVehiclesOnLogin = [{
            type: "bicycle",
            total: parkingsResult.length && parkingsResult[0].bicycle ? parkingsResult[0].bicycle : 0,
        },{
            type: "twoWheeler",
            total: parkingsResult.length && parkingsResult[0].twoWheeler ? parkingsResult[0].twoWheeler : 0,
        },{
            type: "threeWheeler",
            total: parkingsResult.length && parkingsResult[0].threeWheeler ? parkingsResult[0].threeWheeler : 0,
        },{
            type: "fourWheeler",total: parkingsResult.length && parkingsResult[0].fourWheeler ? parkingsResult[0].fourWheeler : 0,
        },{
            type: "Rickshaw",total: parkingsResult.length && parkingsResult[0].Rickshaw ? parkingsResult[0].Rickshaw : 0,
        },{
            type: "ElectricBike",total: parkingsResult.length && parkingsResult[0].ElectricBike ? parkingsResult[0].ElectricBike : 0,
        }];
        requestBody.pendingonLogin=pendingonLogin;
        requestBody.pendingonLogout=pendingonLogin;
        requestBody.pendingVehiclesOnLogin=pendingVehiclesOnLogin;
        const othersResult = await endOthersSessions(requestBody.standId);
        const sessionUpdate = await Sessions.create(requestBody);
        const result = await Users.update({ userName: requestBody.userName }, { $set: { lastLoginSession } }).exec();
        if(requestBody.role == 'employee'){
            const ownerDetails = await Users.findOne({ownerId: requestBody.ownerId}).exec();
            requestBody['ownerDetails'] = ownerDetails;
            startTime = moment(sessionUpdate.startDate).format('DD/MM/YYYY HH:mm:ss');
            requestBody['startTime'] = startTime;
            const sendSMS = await SMS.sendSMS('employee_login',requestBody,ownerDetails.phoneNumber);
        }
        return res.json({ status: "success", data: sessionUpdate,pendingonLogin,pendingVehiclesOnLogin,othersResult, message: 'session created for user.' });
    } catch(error) {
        return  res.json({status: "fail", error : error});
    }
}

endOthersSessions = async(standId) => {
    try{
    const result = await Sessions.find({standId: standId, end_time: null, $or: [{role: 'employee',role: 'owner'}]}).exec();
    await Promise.all(result.map(async (element) => {
        let obj = {
            "start_time": element.start_time,
            "userName":  element.userName,
            "standId": element.standId,
            "ownerId": element.ownerId,
            "employeeId":  element.employeeId
        }
        const result = await endOtherSession(obj)
      }));
        return { status: "success", message: `${result.length} sessions ended for stand.` };
    } catch(error){
        return {status: "fail", error : error}
    }
}

endOtherSession = async(requestBody)=>{
    try{
        requestBody.end_time = Date.now;
        if(_.isEmpty(requestBody.userName) &&  _.isEmpty(requestBody.id)){
            return{ status: "fail", message: "Enter required parameters" };
        }
        // DB call for getting parking vehicles count
        const query = getEndSessionParkings(requestBody);
        const parkingResult = await Parkings.aggregate(query).exec();
        const columns = ["entries","exits","cancelled","pendingonLogout"];
        let total = {};
        for(let i=0;i<columns.length;i++){
            total[columns[i]]=parkingResult.length && parkingResult[0][columns[i]] ? parkingResult[0][columns[i]] : 0;
        }
        //DB call for getting cards Amount
        const cardsAmountQuery = getCardsCollectionQuery(requestBody);
        const cardsCollection = await Cards.aggregate(cardsAmountQuery).exec();
        const cardAmount = cardsCollection.length ? cardsCollection[0].SUM : 0;

        //DB call for getting Parkings Amount
        const parkingAmountQuery = getTotalParkingsQuery(requestBody);
        const parkingsData = await Parkings.aggregate(parkingAmountQuery).exec();
        const parkingsAmount = parkingsData.length ? parkingsData[0].SUM : 0;

        const sessionUpdate = await Sessions.update({$and: [{userName: requestBody.userName}, {'start_time': requestBody.start_time}]},{$set : {'end_time': requestBody.end_time, pendingonLogout: total.pendingonLogout}});
        const userUpdate = await Users.update({userName: requestBody.userName}, { $set: { loginStatus: false, accessToken: '', refreshToken: '', lastLogoutSession: requestBody.end_time } }).exec();
        if(sessionUpdate.nModified >0 && userUpdate.nModified >0) {
            if(requestBody.standId != "" && requestBody.ownerId != ""){
                const data = {
                    "name": requestBody.userName,
                    "standName": requestBody.standId,
                    "login": moment(requestBody.start_time).format('DD/MM/YYYY HH:mm:ss'),
                    "logout": moment(requestBody.end_time).format('DD/MM/YYYY HH:mm:ss'),
                    "vechileEntry": total.entries,
                    "vechileExit": total.exits,
                    "pending": total.pendingonLogout,
                    "collectedAmount": parkingsAmount,
                    "cardAmount": cardAmount,
                    "totalAmount": parkingsAmount + cardAmount
                }
                const ownerDetails = await Users.findOne({ownerId: requestBody.ownerId, role: 'owner'}).exec();
                const sendMessage = await SMS.sendSMS('employee_logout',data, ownerDetails.phoneNumber);
                generateEmployeeReports(requestBody);
            }
            return { status: "success", message: 'session ended for user.' };
        } else {
            return { status: "fail", message: 'session already ended.',sessionUpdate,userUpdate };
        }
    } catch(error) {
        return {status: "fail", error : error};
    }
}
exports.endSession = async (req,res) => {
    try{
        const requestBody = req.body;
        requestBody.end_time = new Date(requestBody.end_time).toISOString();
        if(_.isEmpty(requestBody.userName) &&  _.isEmpty(requestBody.id)){
            return res.json({ status: "fail", message: "Enter required parameters" });
        }
        // DB call for getting parking vehicles count
        const query = getEndSessionParkings(requestBody);
        const parkingResult = await Parkings.aggregate(query).exec();
        const columns = ["entries","exits","cancelled","pendingonLogout"];
        let total = {};
        for(let i=0;i<columns.length;i++){
            total[columns[i]]=parkingResult.length && parkingResult[0][columns[i]] ? parkingResult[0][columns[i]] : 0;
        }
        //DB call for getting cards Amount
        const cardsAmountQuery = getCardsCollectionQuery(requestBody);
        const cardsCollection = await Cards.aggregate(cardsAmountQuery).exec();
        const cardAmount = cardsCollection.length ? cardsCollection[0].SUM : 0;

        //DB call for getting Parkings Amount
        const parkingAmountQuery = getTotalParkingsQuery(requestBody);
        const parkingsData = await Parkings.aggregate(parkingAmountQuery).exec();
        const parkingsAmount = parkingsData.length ? parkingsData[0].SUM : 0;

        const sessionUpdate = await Sessions.update({$and: [{userName: requestBody.userName}, {'start_time': requestBody.start_time}]},{$set : {'end_time': requestBody.end_time, pendingonLogout: total.pendingonLogout}});
        const updateAllSessions = await Sessions.updateMany({$and: [{userName: requestBody.userName}, {'end_time': null}]},{$set : {'end_time': requestBody.start_time}});
        const userUpdate = await Users.update({userName: requestBody.userName}, { $set: { loginStatus: false, accessToken: '', refreshToken: '', lastLogoutSession: requestBody.end_time } }).exec();
        if(sessionUpdate.nModified >0 && userUpdate.nModified >0) {
            if(requestBody.role === "employee"){
                const data = {
                    "name": requestBody.userName,
                    "standName": requestBody.standId,
                    "login": moment(requestBody.start_time).format('DD/MM/YYYY HH:mm:ss'),
                    "logout": moment(requestBody.end_time).format('DD/MM/YYYY HH:mm:ss'),
                    "vechileEntry": total.entries,
                    "vechileExit": total.exits,
                    "pending": total.pendingonLogout,
                    "collectedAmount": parkingsAmount,
                    "cardAmount": cardAmount,
                    "totalAmount": parkingsAmount + cardAmount
                }
                const ownerDetails = await Users.findOne({ownerId: requestBody.ownerId, role: 'owner'}).exec();
                const sendMessage = await SMS.sendSMS('employee_logout', data, ownerDetails.phoneNumber);
                logger.warn({message: `End Session For : ${requestBody.employeeId} / ${requestBody.sessionId} / ${requestBody.end_time}`})
                generateEmployeeReports(requestBody);
            }
            return res.json({ status: "success", message: 'session ended for user.',sessionUpdate,userUpdate });
        } else {
            return res.json({ status: "fail", message: 'session already ended.',sessionUpdate,userUpdate });
        }
    } catch(error) {
        return  res.json({status: "fail", error : error});
    }
}

exports.checkSession = async  function(req, res) {
    try {
        const {userName, start_time} = req.body;
        const result = await Sessions.findOne({$and: [{userName, start_time}]}).exec();
        return res.json({ status: "success", message: 'session details.',result });
    } catch(error) {
        return  res.json({status: "fail", error});
    }
}

exports.getSessionReportByOwner = async(req, res) => {
    try {
        const request = req.body;
        const {ownerId, standId} = request;
        const employeeId = req.params.empId;
        const role = req.body.role;
        const size = parseInt(req.body.size);
        const pageNo = parseInt(req.body.pageNo);
        const search_text = req.body.searchText;
        if (pageNo < 0 || pageNo === 0) {
            return res.json({ status: "fail", message: "invalid page number" });
        }
        const skip = size * (pageNo - 1);
        let limit = size;
        let data = {
            ownerId: request.ownerId,
            standId: request.standId,
            employeeId,
            startDate: request.startDate,
            endDate: request.endDate
        }
        //DB call for getting cards Amount
        const cardsAmountQuery = getCardsCollectionBySessionHistoryQuery(data);
        const cardsCollection = await Sessions.aggregate(cardsAmountQuery).exec();
        const cardAmount = cardsCollection.length ? cardsCollection[0].SUM : 0;

        //DB call for getting cards Amount
        const parkingAmountQuery = getTotalParkingsBySessionHistoryQuery(data);
        const parkingsData = await Sessions.aggregate(parkingAmountQuery).exec();
        const parkingsAmount = parkingsData.length ? parkingsData[0].SUM : 0;

        //DB call to query total count of sessions
        const sessionHistory = getTotalSessionHistory(data);
        const parkingResult = await Parkings.aggregate(sessionHistory).exec();
        const columns = ["entries","exits","cancelled","pendingonLogout"];
        let total = {};
        for(let i=0;i<columns.length;i++){
            total[columns[i]]=parkingResult.length && parkingResult[0][columns[i]] ? parkingResult[0][columns[i]] : 0;
        }
        total.cardAmount = cardAmount;
        total.parkingsAmount = parkingsAmount;

        let query = {};
        let condition = [{employeeId}]
        if (search_text) {
            condition.push({
                $or: [
                    { 'userName': { $regex:  search_text, $options: 'i' } }
                ]
            })
        };
        query = {$and: condition};
        const currentVehiclesCount = {ownerId,standId,status: 'Entry'};
        const currentPendingVehicles = await Parkings.countDocuments(currentVehiclesCount).exec();
        const totalCount = await Sessions.countDocuments(query).exec();
        const sessionResult = await Sessions.find(query).skip(skip).limit(limit).sort({ '_id': -1 }).exec();
        sessionResult && sessionResult.length ? sessionResult[0].pendingonLogout = currentPendingVehicles : '';
        return res.json({status: 'success', sessionResult,totalCount, total})
    } catch(error) {
        return  res.json({status: "fail", error});
    }
}

//Queries 

getEndSessionParkings = (data) => {
    const { end_time, ownerId, standId, start_time, employeeId } = data;

    return [{
    "$facet": {
        "entries": [{
            "$match": {
                $and: [{
                    dateAndTimeOfParking: {
                        $gte: new Date(start_time),
                        $lt: new Date(end_time)
                    }
                },{
                    ownerId: ownerId
                },{
                    standId: standId
                },{
                    $or: [{
                        closedEmpId: employeeId
                    },{
                        openEmpId: employeeId
                    }]
                }]
            }
        },
        {
            $count: "entries"
        }
        ],
        "exits": [{
            "$match": {
                $and: [{
                    status: 'Exit'
                }, {
                    dateAndTimeOfExit: {
                        $gte: new Date(start_time),
                        $lt: new Date(end_time)
                    }
                },{
                    ownerId: ownerId
                },{
                    standId: standId
                },{
                    $or: [{
                        closedEmpId: employeeId
                    },{
                        openEmpId: employeeId
                    }]
                }]
            }
        },
        {
            $count: "exits"
        }
        ],
        "cancelled": [{
            "$match": {
                $and: [{
                    status: 'Cancelled'
                }, {
                    dateAndTimeOfParking: {
                        $gte: new Date(start_time),
                        $lt: new Date(end_time)
                    }
                },{
                    ownerId: ownerId
                },{
                    standId: standId
                },{
                    $or: [{
                        closedEmpId: employeeId
                    },{
                        openEmpId: employeeId
                    }]
                }]
            }
        },
        {
            $count: "cancelled"
        }
        ],
        "pendingonLogout": [{
            "$match": {
                $and: [{
                    status: "Entry"
                }, {
                    dateAndTimeOfParking: {
                        $lt: new Date(end_time)
                    }
                },{
                    ownerId: ownerId
                },{
                    standId: standId
                },{
                    $or: [{
                        closedEmpId: employeeId
                    },{
                        openEmpId: employeeId
                    }]
                }]
            }
        },
        {
            $count: "pendingonLogout"
        }
        ]
    }
},
{
    "$project": {
        "entries": {
            "$arrayElemAt": [
                "$entries.entries",
                0
            ]
        },
        "exits": {
            "$arrayElemAt": [
                "$exits.exits",
                0
            ]
        },
        "cancelled": {
            "$arrayElemAt": [
                "$cancelled.cancelled",
                0
            ]
        },
        "pendingonLogout": {
            "$arrayElemAt": [
                "$pendingonLogout.pendingonLogout",
                0
            ]
        }
    }
}
]
}

getCardsCollectionQuery = (data) => {
    const { start_time, end_time, ownerId, standId, employeeId } = data;
    return [{
        $match: {
            $and: [{
                createdDate: {
                $gte: new Date(start_time),
                $lt: new Date(end_time)
                }
            },{
                ownerId: ownerId
            },{
                standId: standId
            },{
                $or: [{
                    closedEmpId: employeeId
                },{
                    openEmpId: employeeId
                }]
            }]
        }
     }, {
  $group: {
     _id: null,
     SUM: {
        $sum: "$amount"
     },
     COUNT: {
        $sum: 1
     }
  }
  }]
}

getTotalParkingsQuery = data => {
    const { start_time, end_time, ownerId, standId, employeeId } = data;
    return [{
        $match: {
            $and: [{
                dateAndTimeOfExit: {
                $gte: new Date(start_time),
                $lt: new Date(end_time)
                }
            },{
                ownerId: ownerId
            },{
                standId: standId
            },{
                $or: [{
                    closedEmpId: employeeId
                },{
                    openEmpId: employeeId
                }]
            }]
        }
     }, {
  $group: {
     _id: null,
     SUM: {
        $sum: "$finalPrice"
     },
     COUNT: {
        $sum: 1
     }
  }
  }]
}

getTotalSessionHistory = (data)=>{
    const { employeeId, ownerId, standId } = data;
    return [{
    "$facet": {
        "entries": [{
            "$match": {
                $and: [{
                    ownerId: ownerId
                },{
                    standId: standId
                },{
                    openEmpId: employeeId
                }]
            }
        },
        {
            $count: "entries"
        }
        ],
        "exits": [{
            "$match": {
                $and: [{
                    status: 'Exit'
                },{
                    ownerId: ownerId
                },{
                    standId: standId
                },{
                    closedEmpId: employeeId
                }]
            }
        },
        {
            $count: "exits"
        }
        ],
        "cancelled": [{
            "$match": {
                $and: [{
                    status: 'Cancelled'
                },{
                    ownerId: ownerId
                },{
                    standId: standId
                },{
                        closedEmpId: employeeId
                }]
            }
        },
        {
            $count: "cancelled"
        }
        ],
        "pendingonLogout": [{
            "$match": {
                $and: [{
                    status: "Entry"
                },{
                    ownerId: ownerId
                },{
                    standId: standId
                }]
            }
        },
        {
            $count: "pendingonLogout"
        }
        ]
    }
},
{
    "$project": {
        "entries": {
            "$arrayElemAt": [
                "$entries.entries",
                0
            ]
        },
        "exits": {
            "$arrayElemAt": [
                "$exits.exits",
                0
            ]
        },
        "cancelled": {
            "$arrayElemAt": [
                "$cancelled.cancelled",
                0
            ]
        },
        "pendingonLogout": {
            "$arrayElemAt": [
                "$pendingonLogout.pendingonLogout",
                0
            ]
        }
    }
}
]
}

getCardsCollectionBySessionHistoryQuery = (data) => {
    const {startDate, endDate, employeeId, ownerId, standId } = data;
    return [{
        $match: {
            $and: [{
                ownerId: ownerId
            },{
                standId: standId
            }, {employeeId: employeeId},{
                start_time: {
                $gte: new Date(startDate)
            }
        },{end_time: {
                $lte: new Date(endDate)
            }}]
        }
     }, {
  $group: {
     _id: null,
     SUM: {
        $sum: "$cardAmount"
     },
     COUNT: {
        $sum: 1
     }
  }
  }]
}

getTotalParkingsBySessionHistoryQuery = data => {
    const { startDate, endDate, employeeId, ownerId, standId } = data;
    return [{
        $match: {
            $and: [{
                ownerId: ownerId
            },{
                standId: standId
            },{
                employeeId: employeeId
            },{
                start_time: {
                $gte: new Date(startDate)
            }
        },{end_time: {
                $lte: new Date(endDate)
            }}]
        }
     }, {
  $group: {
     _id: null,
     SUM: {
        $sum: "$parkingsAmount"
     },
     COUNT: {
        $sum: 1
     }
  }
  }]
}

getPendingOnLoginVehicles = (requestBody) => {
	return [{
		"$facet": {
			"bicycle": [{
				$match: {
					$and: [{
						vehicleType: "bicycle"
					}, {
                        status: 'Entry'
                    }, {
                        dateAndTimeOfParking: {
                            $lt: new Date(requestBody.start_time)
                        }
                    },{
                        ownerId: requestBody.ownerId
                    },{
                        standId: requestBody.standId
                    }]
				}
			}, {
				$count: "bicycle"
			}],
			"twoWheeler": [{
				$match: {
					$and: [{
						vehicleType: "twoWheeler"
					},{
                        status: 'Entry'
                    }, {
                        dateAndTimeOfParking: {
                            $lt: new Date(requestBody.start_time)
                        }
                    },{
                        ownerId: requestBody.ownerId
                    },{
                        standId: requestBody.standId
                    }]
				}
			}, {
				$count: "twoWheeler"
			}],
			"threeWheeler": [{
				$match: {
					$and: [{
						vehicleType: "threeWheeler"
					},{
                        status: 'Entry'
                    }, {
                        dateAndTimeOfParking: {
                            $lt: new Date(requestBody.start_time)
                        }
                    },{
                        ownerId: requestBody.ownerId
                    },{
                        standId: requestBody.standId
                    }]
				}
			}, {
				$count: "threeWheeler"
			}],
			"fourWheeler": [{
				$match: {
					$and: [{
						vehicleType: "fourWheeler"
					},{
                        status: 'Entry'
                    }, {
                        dateAndTimeOfParking: {
                            $lt: new Date(requestBody.start_time)
                        }
                    },{
                        ownerId: requestBody.ownerId
                    },{
                        standId: requestBody.standId
                    }]
				}
			}, {
				$count: "fourWheeler"
			}],
            "Rickshaw": [{
                $match: {
                    $and: [{
                        vehicleType: "Rickshaw"
                    },{
                        status: 'Entry'
                    }, {
                        dateAndTimeOfParking: {
                            $lt: new Date(requestBody.start_time)
                        }
                    },{
                        ownerId: requestBody.ownerId
                    },{
                        standId: requestBody.standId
                    }]
                }
            }, {
                $count: "Rickshaw"
            }],
            "ElectricBike": [{
                $match: {
                    $and: [{
                        vehicleType: "ElectricBike"
                    },{
                        status: 'Entry'
                    }, {
                        dateAndTimeOfParking: {
                            $lt: new Date(requestBody.start_time)
                        }
                    },{
                        ownerId: requestBody.ownerId
                    },{
                        standId: requestBody.standId
                    }]
                }
            }, {
                $count: "ElectricBike"
            }]
		}
	},
	{
		"$project": {
			"bicycle": {
				"$arrayElemAt": [
					"$bicycle.bicycle",
					0
				]
			},
			"twoWheeler": {
				"$arrayElemAt": [
					"$twoWheeler.twoWheeler",
					0
				]
			},
			"threeWheeler": {
				"$arrayElemAt": [
					"$threeWheeler.threeWheeler",
					0
				]
			},
			"fourWheeler": {
				"$arrayElemAt": [
					"$fourWheeler.fourWheeler",
					0
				]
			},
            "Rickshaw": {
                "$arrayElemAt": [
                    "$Rickshaw.Rickshaw",
                    0
                ]
            },
            "ElectricBike": {
                "$arrayElemAt": [
                    "$ElectricBike.ElectricBike",
                    0
                ]
            },
		}
	}
	]
}