exports.createDemo = async(req, res) => {
    try {
        const reqData = req.body;
        const isEmailExist = await Demos.findOne({ email: reqData.email }).exec();
        if (!_.isEmpty(isEmailExist)) {
            throw new ApplicationError("Email already registered", 500, "");
        }
        var result = await Demos.create(reqData);
        return res.json({ status: 'Success', message: 'Request Added Successfully' });
    } catch(e) {
        return res.json({ status: 'fail', message: e.message });
    }
}

exports.getRequestDemos = async (req, res) => {
    const search_text = req.query.searchText;
    const size = parseInt(req.query.size);
    const pageNo = parseInt(req.query.pageNo);
    if (pageNo < 0 || pageNo === 0) {
        return res.json({ status: "fail", message: "invalid page number" });
    }
    const skip = size * (pageNo - 1);
    const limit = size;
    try {
        let query = {};
        if (search_text) {
            query = {
                $or: [
                    { 'name': { $regex:  search_text, $options: 'i' } },
                    { 'email': { $regex:  search_text, $options: 'i' } },
                    { 'phone': { $regex:  search_text, $options: 'i' } },
                    { 'location': { $regex:  search_text, $options: 'i' } },
                    { 'message': { $regex:  search_text, $options: 'i' } },
                ]
            }
        }
            let totalCount = await Demos.countDocuments(query).exec();
            if (totalCount < 0) {
                throw new ApplicationError("No Demos data found", 500, "");
            }
            const result = await Demos.find(query).skip(skip).limit(limit).sort({ '_id': -1 }).exec();
            if (!_.isEmpty(result)) {
                return res.json({ status: "success", data: result, totalCount: totalCount });
            } else {
                throw new ApplicationError("No Demos data found", 500, "");
            }
        
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}