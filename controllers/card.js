const SMS = require('./SMS');
const axios = require('axios');
const { MONTHLY_CARD_EXPIRED }  = require('../config/constants');
const logger = require('../config/logger');
exports.addCard = async function (req, res) {
    try {
    let cardData = req.body;
    const startDate = cardData.startDate
    const endDate = cardData.endDate;
    logger.warn({message: `Monthly Card Initialization : ${cardData.vehicleNumber} / ${cardData.customerPhone}`})
    const isCardExist = await Cards.findOne({vehicleNumber: cardData.vehicleNumber,customerPhone: cardData.customerPhone}).exec();
    if(!_.isEmpty(isCardExist)){
        logger.error({message: `Monthly Card already exists: ${cardData.vehicleNumber} / ${cardData.customerPhone}`})
        return res.json({ status: "fail", message: "Monthly card already exist with this details, please renew the card.",cardData: isCardExist });
    }
    const cardDetails = await Cards.countDocuments({standId: cardData.standId}).sort({ _id: -1 }).exec();
    const standDetails = await Stands.findOne({standId: cardData.standId}).exec();
    if(!standDetails){
        standCode = 'CRD';
    } else {
        standCode = standDetails.standCode;
    }
    if (cardDetails == 0) {
        cardData.cardId = await helper.getCardId(0, standCode);
    } else {
        cardData.cardId = await helper.getCardId(cardDetails, standCode);
    }
    cardData["type"] = cardData.role === 'employee' ? 'offline' : 'online';
    logger.warn({message: `Monthly Card Id generated: ${cardData.vehicleNumber} / ${cardData.cardId} / ${cardData.type}`})
    let PriceRulesResult = await PriceRules.findOne({standId: cardData.standId}).exec();
    if (_.isEmpty(PriceRulesResult)) {
        return res.json({ status: "fail", message: "No pricerules for this stand" });
    }
    const selectedPrice = PriceRulesResult.vehicleTypes.filter(item => {
        return item.vehicletype == cardData.vehicleType
    })
    cardData["discount"] = selectedPrice.length ? selectedPrice[0].cardMaxDiscount : 0;
    cardData["cardHistory"] = [{
        startDate: startDate,
        endDate: endDate,
        duration: cardData.duration,
        amount: cardData.amount
    }];
    cardData.endDate = endDate;
    cardData.startDate = startDate;
        const result = await Cards.create(cardData);
        const expiryDate = cardData.endDate;
        cardData['expiryDate'] = moment(new Date(expiryDate)).format('DD/MM/YYYY');
        cardData.startDate = moment(new Date(cardData.startDate)).format('DD/MM/YYYY HH:mm:ss')
        cardData.endDate = moment(new Date(cardData.endDate)).format('DD/MM/YYYY HH:mm:ss')
        logger.warn({message: `Monthly Card Scheduled SMS init: ${cardData.vehicleNumber} / ${cardData.cardId} / ${cardData.type}`})
        sendScheduledSMS(cardData, startDate);
        if(cardData.type === 'offline'){
        const sessionUpdate = await Sessions.update({ _id: cardData.sessionId }, { $inc: { offlineCards: 1, cardAmount: cardData.amount }}).exec();
        }
        let smsResult = "SMS service not enabled for Monthly cards";
            if(cardData.sendSMS){
                cardData.startDate = cardData.startDate.split(' ')[0];
                cardData.endDate = cardData.endDate.split(' ')[0];
                if(cardData.type === 'offline'){
                smsResult = await SMS.sendSMS('monthlycard_purchase_offline',cardData,cardData.customerPhone);
                } else {
                    smsResult = await SMS.sendSMS('monthlycard_purchase_online',cardData,cardData.customerPhone);
                }
            } 
            logger.warn({message: `Monthly Card Added Succesfully: ${cardData.vehicleNumber} / ${cardData.cardId} / ${cardData.type}`})
            // return res.json({ status: "fail", message: '',smsResult });  
        return res.json({ status: "Success", message: "card added succesfully", data: result, smsResult: smsResult })
    } catch (error) {
        return res.json({ status: "fail", message: error.message });
    }
}

exports.cardRenewal = async (req, res) => {
    try {
        const cardData = req.body;
        const cardDetails = Cards.findOne({cardId: cardData.cardId}).exec();
        if (!_.isEmpty(cardDetails)) {
            throw new ApplicationError("Card details not found", 500, "");
        }
        const result = await Cards.update({cardId: cardData.cardId}, {
            $set:{
                cardHistory: cardData.cardHistory,
                startDate: cardData.startDate,
                endDate: cardData.endDate,
                renewedDate: cardData.renewalDate,
                employeeId: cardData.employeeId,
                amount: cardData.amount
            }
        });
        if(cardData.cardType == 'Offline'){
            const sessionUpdate = await Sessions.update({ _id: cardData.sessionId }, { $inc: { offlineCards: 1, cardAmount: cardData.amount }}).exec();
        }
        let smsResult = "SMS service not enabled for Monthly cards";
            if(cardData.sendSMS){
                smsResult = await SMS.sendSMS('monthlycard_renewal_success',cardData,cardData.phone);
            } 
        return res.json({ status: "Success", message: "Card Renewed Succesfully" });
    } catch(error) {
        return res.json({ status: "fail", message: error.message });
    }
    
}

exports.getCardById = async function (req, res) {
    const { cardId, standId } = req.body;
    try {
        const cardData = await Cards.findOne({ "cardId": { $regex:  cardId, $options: 'i' },standId }).exec();
        if (_.isEmpty(cardData)) {
            throw new ApplicationError("No card data found");
        }
        return res.json({ status: 'success', data: cardData })
    } catch (error) {
        return res.json({ 'status': 'fail', message: error.message })
    }
}
// need to do development 
exports.getCardsList = async function (req, res) {
    const standId = req.query.standId;
    const ownerId = req.query.ownerId;
    const type = req.query.type;
    var size = parseInt(req.query.size);
    var pageNo = parseInt(req.query.pageNo);
    const search_text = req.query.searchText;
    let totalCount = 0;
    if (pageNo < 0 || pageNo === 0) {
        return res.json({ status: "fail", message: "invalid page number" });
    }
    try { 
    var skip = size * (pageNo - 1);
    var limit = size;
    let searchQuery = {};
    let roleQuery = {};
    let queries = [];
    let query = {};
    if (!standId && _.isEmpty(type)) {
        throw new ApplicationError("Type is required", 500, "");
    }
    if(standId){
        roleQuery = {"standId": standId};
        queries.push(roleQuery)
    } 
    if(type) {
        queries.push({type: type})
    }
    if(ownerId){
        roleQuery = {"ownerId": ownerId};
        queries.push(roleQuery)
    }
    
    if(search_text){
        searchQuery = {
            $or: [
                { 'cardId': { $regex:  search_text, $options: 'i' } },
                { 'customerName': { $regex:  search_text, $options: 'i' } },
                { 'customerPhone': { $regex:  search_text, $options: 'i' } },
                { 'vehicleNumber': { $regex:  search_text, $options: 'i' } },
                { 'type': { $regex:  search_text, $options: 'i' } },
                { 'status': { $regex:  search_text, $options: 'i' } }
            ]
        }
        queries.push(searchQuery)
    }
          
                if(queries.length){
                    query = {$and: [...queries]}
                } 
                totalCount = await Cards.countDocuments(query).exec();
                if (totalCount < 0) {
                    throw new ApplicationError("data not found", 500, "");
                }
                var result = await Cards.find(query).skip(skip).limit(limit).sort({ 'status': 1 }).exec()
                if (_.isEmpty(result)) {
                    return res.json({ status: 'success', message: "No result found", data: result })
                }
            return res.json({ status: 'success', data: result, totalCount: totalCount })
    } catch (e) {
        return res.json({ status: 'fail', message: e.message })
    }
}

exports.getCardId = async function (req, res) {

    try {
        var cardData = await Cards.countDocuments({}, { cardId: 1 }).sort({ _id: -1 }).exec();
        if (cardData==0) {
            cardId = await helper.getCardId(0,'CRD');
        } else {
            cardId = await helper.getCardId(cardData,'CRD')
        }
        return res.json({ status: 'success', data: cardId })
    } catch (error) {
        return res.json({ 'status': 'fail', message: error.message })
    }
}

exports.getAdminCards = async (req, res) => {
    try{
        const standId = req.query.standId;
        const ownerId = req.query.ownerId;
        const cardType = req.query.cardType;
        const size = parseInt(req.query.size);
        const pageNo = parseInt(req.query.pageNo);
        const skip = size * (pageNo - 1);
        const limit = size;
        const search_text = req.query.searchText;
        let totalCount = 0;
        let query = {};
        if(search_text){
            query = {
                $or: [
                    { 'stand': { $regex:  search_text, $options: 'i' } },
                    { 'location': { $regex:  search_text, $options: 'i' } },
                    { 'status': { $regex:  search_text, $options: 'i' } }
                ]
            }
        }
    if (pageNo < 0 || pageNo === 0) {
        return res.json({ status: "fail", message: "invalid page number" });
    }
        const cardsResult = Cards.find({$and:[{standId: standId},{ownerId: ownerId},{type: cardType}]}).skip(skip).limit(limit).sort({ 'status': 1 }).exec()
        res.json({'status': 'success', message:"success", data: cardsResult})
    } catch(e) {
        return res.json({ 'status': 'fail', message: error.message })
    }
}

exports.getOfflineCardsByOwner = async (req, res)=>{
    try{
        const ownerId = req.query.ownerId;
        const cardType = req.query.cardType;
        let data = {};
        const query = await helper.getOfflineCardsQuery(ownerId, cardType);
        const result = await Cards.aggregate(query).exec();
        let totalCards = {};
	    const vehicleTypes = ['','bicycle','twoWheeler','threeWheeler','fourWheeler'];
        const standsCount = await Stands.countDocuments({ownerId}).exec();
	for(let i=0; i<vehicleTypes.length;i++){
        let totalCount = {}
		let newCards = vehicleTypes[i]+"newCards";
		totalCount[newCards]= result.length && result[0][newCards] ? result[0][newCards] : 0;
		let renewalCards = vehicleTypes[i]+"renewalCards";
        totalCount[renewalCards]= result.length && result[0][renewalCards] ? result[0][renewalCards] : 0;
		let expiredCards = vehicleTypes[i]+"expiredCards";
        totalCount[expiredCards]= result.length && result[0][expiredCards] ? result[0][expiredCards] : 0;
        
        totalCards[vehicleTypes[i]] = totalCount;
        if(vehicleTypes[i] == ""){
            data.total = data[vehicleTypes[i]];
            delete data[""];
        }
	}

        res.json({status: 'success', message: "success", totalCards: totalCards,standsCount})
    } catch(e){
        res.json({status: 'fail', message: e})
    }
}

exports.getOfflineCardsByStand = async (req, res)=>{
    try{
        const ownerId = req.query.ownerId;
        const standId = req.query.standId;
        const cardType = req.query.cardType;
        const query = await helper.getOfflineCardsByStandQuery(ownerId, standId, cardType);
        const result = await Cards.aggregate(query).exec();
        let data = {};
	const vehicleTypes = ['','bicycle','twoWheeler','threeWheeler','fourWheeler'];
	for(let i=0; i<vehicleTypes.length;i++) {
        let totalCount = {};
		let newCards = vehicleTypes[i]+"newCards";
		totalCount[newCards]= result.length && result[0][newCards] ? result[0][newCards] : 0;
		let renewalCards = vehicleTypes[i]+"renewalCards";
        totalCount[renewalCards]= result.length && result[0][renewalCards] ? result[0][renewalCards] : 0;
		let expiredCards = vehicleTypes[i]+"expiredCards";
        totalCount[expiredCards]= result.length && result[0][expiredCards] ? result[0][expiredCards] : 0;
        
        data[vehicleTypes[i]] = totalCount;
        if(vehicleTypes[i] == ""){
            data.total = data[vehicleTypes[i]];
            delete data[""];
        }
	}

        res.json({status: 'success', message: "success", data})
    }
     catch(e){
        res.json({status: 'fail', message: e})
    }
}

exports.getExpiryCards = async (req, res) => {
    try{
        const standId = req.params.standId;
        const data  = await Cards.find({"endDate" : { $lt : new Date() },standId, type: "offline"}).exec();
        res.json({status: 'success', message: "success", data})
    } catch(e) {
        res.json({status: 'fail', message: e})
    }
}

exports.getPaymentReports = async (req, res) => {
    const { type, ownerId } = req.query;
    var size = parseInt(req.query.size);
    var pageNo = parseInt(req.query.pageNo);
    const search_text = req.query.searchText;
    let totalCount = 0;
    if (pageNo < 0 || pageNo === 0) {
        return res.json({ status: "fail", message: "invalid page number" });
    }
    try { 
    var skip = size * (pageNo - 1);
    var limit = size;
    let query = {type, ownerId};
    
    if(search_text){
        let searchQuery = {
            $or: [
                { 'standId': { $regex:  search_text, $options: 'i' } },
                { 'cardId': { $regex:  search_text, $options: 'i' } },
                { 'customerPhone': { $regex:  search_text, $options: 'i' } },
                { 'vehicleNumber': { $regex:  search_text, $options: 'i' } },
                { 'status': { $regex:  search_text, $options: 'i' } }
            ]
        }
        query = {type, searchQuery, ownerId}
    }
                totalCount = await Cards.countDocuments(query).exec();
                if (totalCount < 0) {
                    throw new ApplicationError("data not found", 500, "");
                }
                var result = await Cards.find(query).skip(skip).limit(limit).sort({ 'status': 1 }).exec()
                if (_.isEmpty(result)) {
                    return res.json({ status: 'success', message: "No result found", data: result })
                }
            return res.json({ status: 'success', data: result, totalCount: totalCount })
    } catch (e) {
        return res.json({ status: 'fail', message: e.message })
    }
}

sendScheduledSMS = async function(data,startDate) {
    try{
        // const schedule_time = moment().add(3, 'minutes').unix(); 
        let days = 0; 
        switch (data['duration']) {
            case 1:
                days = 30
                break;
            case 2: 
                days = 60
            case 3: 
                days = 90
            case 4: 
                days = 180
            case 5: 
                days = 365
            default:
                days = 30
                break;
        };
        const schedule_time = moment(new Date(startDate)).add((days + 1), 'days').unix();  //&schedule_time='+schedule_time+';
        const message = MONTHLY_CARD_EXPIRED(data);
        logger.warn({message: `Monthly card schedule SMS content: ${message}`})
        const url = 'https://api.textlocal.in/send/?apikey='+config.textLocalAPIkey+'&numbers='+data.customerPhone+'&schedule_time='+schedule_time+'&sender=STSPAR&message=' + encodeURIComponent(message);
        let response;
        await axios
        .get(url)
        .then(function (resp) {
            response = resp.data;
        })
        .catch(function (error) {
        response = error;
        console.log("error", error);
        throw new ApplicationError(error, 500, "");
        });
        // return res.json({ status: "success", message: response, otp });
    }
     catch(error){
         console.log(error);
    }
}