exports.addVehicleType = async function (req, res) {
    try {
    const { type, code } = req.body;
        if (_.isEmpty(type) || _.isEmpty(code)) {
            throw new ApplicationError("Missing required parameter", 500, "");
        }
        const plans = await VehicleTypes.findOne({type}).exec();
        if (!_.isEmpty(plans)) {
            return res.json({ status: 'fail', message: plans + ' is already exists' })
        }
        const result = await VehicleTypes.create({type, code})
        return res.json({ status: 'Success', message: 'Vehicle Type added succesfully' })
    } catch (e) {
        return res.json({ status: 'fail', message: e.message })
    }
}

exports.getAllVehicleTypes = async function (req, res) {
    try {
        const plans = await VehicleTypes.find({}).exec();
        return res.json({ status: 'Success', plans })
    } catch (e) {
        return res.json({ status: 'fail', message: e.message })
    }
}

exports.getVehicleTypesList = async function (req, res) {
    try {
    const size = parseInt(req.query.size);
    const pageNo = parseInt(req.query.pageNo);
    const search_text = req.query.searchText;
    const skip = size * (pageNo - 1);
    const limit = size;
    let totalCount = 0;
    let query = {};

    if (pageNo < 0 || pageNo === 0) {
        return res.json({ status: "fail", message: "invalid page number" });
    }

    if (search_text) {
        query = {
            $or: [
                { 'type': { $regex:  search_text, $options: 'i' } },
            ]
        }
    }
        totalCount = await VehicleTypes.countDocuments(query).sort({ '_id': 1 }).exec();
        if (totalCount < 0) {
            throw new ApplicationError("Vehicle Type data not found", 500, "");
        }
        var result = await VehicleTypes.find(query).skip(skip).limit(limit).sort({ '_id': 1 }).exec()
        if (_.isEmpty(result)) {
            return res.json({ status: 'success', message: "No result found" })
        }

        return res.json({ status: 'Success', data: result, totalCount: totalCount })
    } catch (e) {
        return res.json({ status: 'fail', message: e.message })
    }
}


exports.updateVehicleType = async function (req, res) {
    
    try {
        const id = mongoose.mongo.ObjectId(req.body.id);
        const obj = {
        type: req.body.type, 
        code: req.body.code, 
    }
        if (_.isEmpty(req.body.type)) {
            throw new ApplicationError("Missing required parameter", 500, "");
        }
        const result = await VehicleTypes.update({"_id": id},{$set: obj})
        return res.json({ status: 'Success', message: 'Vehicle Type updated succesfully' })
    } catch (e) {
        return res.json({ status: 'fail', message: e.message })
    }
}

exports.deleteVehicleType = async function (req, res) {
    try {
    const id = mongoose.mongo.ObjectId(req.params.id);
    
        var result = await VehicleTypes.remove({ _id: id }).exec();
        if (!_.isEmpty(result)) {
            return res.json({ status: "success", message: "Vehicle Type deleted successfully!.." });
        } else {
            throw new ApplicationError("Unable to delete the record", 500, "");
        }
    } catch (error) {
        return res.json({ status: "fail", error: "Unable to delete the record, please check the Id" })
    }
}