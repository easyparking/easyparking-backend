exports.logoutList = async(req, res) => {
    try {
        const ownerId = req.query.ownerId;
        const standId = req.query.standId;
        const size = parseInt(req.query.size);
        const pageNo = parseInt(req.query.pageNo);
        const searchText = req.query.searchText;
        if (pageNo < 0 || pageNo === 0) {
            return res.json({ status: "fail", message: "invalid page number" });
        }
        const skip = size * (pageNo - 1);
        const limit = size;
        let query = [{'loginStatus': true}];
        if(ownerId && ownerId != ""){
            query.push({ownerId})
        }
        if(standId && standId != ""){
            query.push({standId})
        }
        let serachQuery = {};
        if(searchText){
            serachQuery = {
                $or: [
                    { 'standName': { $regex:  searchText, $options: 'i' } },
                    { 'employeeId': { $regex:  searchText, $options: 'i' } },
                    { 'name': { $regex:  searchText, $options: 'i' } },
                    // { 'lastLoginSession': { $regex:  searchText, $options: 'i' } }
                ]
            }
        }
        const totalCount = await Users.countDocuments({$and:[...query,serachQuery]}).exec();
        const result = await Users.find({$and:[...query,serachQuery]}).select('standId ownerId standName employeeId name userName lastLoginSession').skip(skip).limit(limit).sort({ 'status': 1 }).exec();
        // const result = await Sessions.find({$and:[{ownerId,standId}]}).skip(skip).limit(limit).sort({ 'status': 1 }).exec() || [];
        // if (_.isEmpty(result)) {
        return res.json({ status: "success", message: 'sessions data retrived', result, totalCount });
        // }
    } catch(e) {
        return res.json({ status: "fail", error: e });
    }
}

exports.getSessionHistory = async(req, res)=> {
    try {
        const userName = req.params.userName;
        if (_.isEmpty(userName)) {
            throw new ApplicationError("userName is required");
        }
        const result = await Sessions.find({userName: userName}).exec();
        return res.json({ status: "success", message: 'sessions data retrived', result });
    } catch(e) {
        return res.json({ 'status': 'fail', message: e })
    }
}