exports.createJobApplication = async (req, res) => {
    try {
        let requestData = req.body;
        const applicationDetails = await JobApplications.countDocuments().sort({ _id: -1 }).exec();
        if (applicationDetails == 0) {
            requestData.applicantId = await helper.getJobApplicantionId(0);
        } else {
            requestData.applicantId = await helper.getJobApplicantionId(applicationDetails);
        }
        const isEmailExist = await JobApplications.findOne({ email: requestData.email }).sort({ _id: -1 }).exec();
        if (!_.isEmpty(isEmailExist)) {
            throw new ApplicationError("Email already registered", 500, "");
        }
        const result = await JobApplications.create(requestData);
        return res.json({ status: "Success", message: "Details submitted successfully", data: result });
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

exports.getJobApplicationsList = async (req, res) => {
    const search_text = req.query.searchText;
    const size = parseInt(req.query.size);
    const pageNo = parseInt(req.query.pageNo);
    if (pageNo < 0 || pageNo === 0) {
        return res.json({ status: "fail", message: "invalid page number" });
    }
    const skip = size * (pageNo - 1);
    const limit = size;
    try {
        let query = {};
        if (search_text) {
            query = {
                $or: [
                    { 'name': { $regex:  search_text, $options: 'i' } },
                    { 'email': { $regex:  search_text, $options: 'i' } },
                    { 'phone': { $regex:  search_text, $options: 'i' } },
                    { 'state': { $regex:  search_text, $options: 'i' } },
                    { 'district': { $regex:  search_text, $options: 'i' } },
                    { 'city': { $regex:  search_text, $options: 'i' } },
                    { 'prefferedSalary': { $regex:  search_text, $options: 'i' } },
                    { 'age': { $regex:  search_text, $options: 'i' } },
                    { 'message': { $regex:  search_text, $options: 'i' } },
                    // { 'createdDate': { $regex:  search_text, $options: 'i' } }
                ]
            }
        }
            let totalCount = await JobApplications.countDocuments(query).exec();
            if (totalCount < 0) {
                throw new ApplicationError("No job applications data found", 500, "");
            }
            const result = await JobApplications.find(query).skip(skip).limit(limit).sort({ '_id': -1 }).exec();
            if (!_.isEmpty(result)) {
                return res.json({ status: "success", message: "job applications Data", data: result, totalCount: totalCount });
            } else {
                throw new ApplicationError("No job applications data found", 500, "");
            }
        
    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}