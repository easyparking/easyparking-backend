
exports.createStand = async function (req, res) {
    let standData = req.body;
    standData.SMS = {
        entry: false,
        exit: false,
        monthlyCards: false
    };
    standData.allowOnlineCards = true,
    standData.status = 'Active';
    try {
        if (_.isEmpty(standData.ownerId)) {
            throw new ApplicationError("Owner Id is required parameter", 500, "");
        }

        const standName = standData.standName,
              matches = standName.match(/\b(\w)/g);
        standData.standCode = matches.join('').substring(0,2);
        if(standData.standCode.length === 1){
            standData.standCode += standName.substring(1,2)
        } 
        const location = standData.location.city.city.charAt(0);
        standData.standCode = (standData.standCode + location).toUpperCase();
        const standDetails = await Stands.find().sort({ _id: -1 }).exec();
        const employeeCount = await Users.countDocuments({ownerId: standData.ownerId, role: "employee"}).exec();
        standData.employeeCount = employeeCount;
        if (standDetails.length == 0) {
            standData.standId = await helper.getStandId(standData.standCode, 0)
        } else {
            const id = Number(standDetails[0].standId.substring(7));
            standData.standId = await helper.getStandId(standData.standCode, id)
        }
        const ownerDetails = await Users.findOne({ownerId: standData.ownerId}).exec();
        var result = await Stands.create(standData);
        const price_rules = helper.createPriceRules(standData.standId, standData.ownerId);
        const price_result = await PriceRules.create(price_rules);
        if(ownerDetails && ownerDetails.phoneNumber){
            standData['password'] = ownerDetails.auth['default'] || (ownerDetails.userName + '619');
            const sendSMS = await SMS.sendSMS('Stand_creation',standData,ownerDetails.phoneNumber);
        }
        return res.json({ status: "Success", message: "Success", data: result });

    } catch (e) {
        return res.json({ status: "fail", message: e.message });
    }
}

exports.getAllStands = async function (req, res) {
    var size = parseInt(req.query.size);
    var pageNo = parseInt(req.query.pageNo);
    const search_text = req.query.searchText;
    if (pageNo < 0 || pageNo === 0) {
        return res.json({ status: "fail", message: "invalid page number" });
    }
    var skip = size * (pageNo - 1);
    var limit = size;
    let query = {};
    try {
        if (search_text) {
            query = {
                $or: [
                    { 'standName': { $regex:  search_text, $options: 'i' } },
                    { 'standId': { $regex:  search_text, $options: 'i' } },
                    { 'location.city.city': { $regex:  search_text, $options: 'i' } },
                    { 'type': { $regex:  search_text, $options: 'i' } },
                    { 'status': { $regex:  search_text, $options: 'i' } }
                ]
            }
        }
        var result = await Stands.find(query).skip(skip).limit(limit).sort({ 'status': 1 }).exec()
        if (_.isEmpty(result)) {
            return res.json({ status: 'success', message: "No result found", data: result })
        }
            return res.json({ status: 'success', data: result })
    } catch (e) {
        return res.json({ status: 'fail', message: e.message })
    }
}

exports.getOnlineCardsCount = async function (req, res) {
    try{
        const query = helper.getOnlineCardsQuery();
        const activeAccounts = await Stands.aggregate(query).exec();
        let onlineCardsCount = {
            Active: activeAccounts.length && activeAccounts[0].OnlineCards ? activeAccounts[0].OnlineCards : 0,
            Total: activeAccounts.length && activeAccounts[0].Total ? activeAccounts[0].Total : 0,
        };
        return res.json({ status: 'success', onlineCardsCount: onlineCardsCount })
    } catch(e) {
        return res.json({ status: 'fail', message: e.message })
    }
}

exports.getStandNames = async function (req, res) {
    var empId = req.params.empId
    try {
        if (_.isEmpty(empId)) {
            throw new ApplicationError("Employee Id is required parameter", 500, "");
        }
        var userData = await Users.find({ "employeeId": empId }, { ownerId: 1 }).sort({ '_id': 1 }).exec();
        if (_.isEmpty(userData)) {
            throw new ApplicationError("No data found");
        }
        var standData = await Stands.find({ ownerId: userData[0].ownerId }, { standName: 1, standId: 1, _id: 0 }).sort({ '_id': 1 }).exec();
        return res.json(standData)
    } catch (e) {
        return res.json({ status: 'fail', message: e.message })
    }
}

exports.getStand = async function (req, res) {
    var standId = req.params.standId
    try {
        if (_.isEmpty(standId)) {
            throw new ApplicationError("Stand Id is required parameter", 500, "");
        }

        var result = await Stands.find({ "standId": standId }).sort({ '_id': 1 }).exec();
        if (_.isEmpty(result)) {
            throw new ApplicationError("No data found");
        }
        return res.json({ status: 'success', data: result })
    } catch (e) {
        return res.json({ status: 'fail', message: e.message })
    }
}

exports.getStandNamesByOwner = async function (req, res) {
    const ownerId = req.params.ownerId;
    try {
        if (_.isEmpty(ownerId)) {
            return new ApplicationError("ownerName is required parameter", 500, "")
        }
        const result = await Stands.find({ ownerId: ownerId, status: 'Active' }).exec();
        if (_.isEmpty(result)) {
            return res.json({ status: "fail", message: "No data found" });
        }
        return res.json({ status: "success", data: result });

    } catch (error) {
        return res.json({ status: "fail", error: error })
    }
}

exports.updateStandSMS = async function (req, res) {
    var standId = req.params.standId
    var SMSData = req.body.SMS

    entry = SMSData.entry ? SMSData.entry : false
    exit = SMSData.exit ? SMSData.exit : false
    monthlyCards = SMSData.monthlyCards ? SMSData.monthlyCards : false
    try {
        if (_.isEmpty(standId)) {
            return new ApplicationError("standId is required parameter", 500, "")
        }
        var result = await Stands.find({ "standId": standId }).sort({ '_id': 1 }).exec();
        if (_.isEmpty(result)) {
            throw new ApplicationError("No Stand data found");
        }
        const smsUpdate = await Stands.update(
            { "standId": standId },
            {
                $set: {
                    SMS: {
                        entry: entry,
                        exit: exit,
                        monthlyCards: monthlyCards
                    }
                }
            });
        if (!_.isEmpty(smsUpdate)) {
            return res.json({ status: "Success", data: smsUpdate, message:"SMS status updated successfully." })
        }

    } catch (error) {
        return res.json({ status: "fail", error: error })
    }
}

exports.updateStand = async function (req, res) {

    var standId = req.params.standId
    allowableKeys =  [
        "standType",
        "address",
        "addressLine1",
        "addressLine2",
        "addressLine3",
        "addressLine4",
        "price",
        "policeStation",
        "policeStationContact",
        "latitude",
        "longtitue",
        "standImage",
        "GSTIn",
        "initialGracePeriod",
        "lateGracePeriod",
        "offer",
        "location",
        "allowOnlineCards",  
        'allowOfflineCards',      
        "priceRules",
        "isOnline",
        "status",
        "expiryDate",
        "note",
        "isPublic"
    ]
    reqKeys = Object.keys(req.body)
    // return res.json({ status: "success", data: reqKeys })

    const notAllowed = reqKeys.filter(element => (allowableKeys.indexOf(element) == -1));

    try {
        if (_.isEmpty(standId)) {
            return new ApplicationError("standId is required parameter", 500, "")
        }
        if (notAllowed.length > 0) {
            // return res.json({ status: "success", data: allow })
            throw new ApplicationError(`These [${notAllowed}] keys are not allowed to update `, 500 ,"")
        }
        const standUpdate = await Stands.update(
            { "standId": standId },
            {
                $set: req.body
            },
            {new: true});
        if (!_.isEmpty(standUpdate)) {
            return res.json({ status: "success", data: standUpdate, message: "Record updated succesfully" })
        }
    } catch (error) {
        return res.json({ status: "fail", error: error })
    }
}

exports.deleteStand = async function (req, res) {
    const standId = req.params.standId;
    try {
        const standData = await Stands.find({ standId: standId }).exec();
        if (_.isEmpty(standData)) {
            throw new ApplicationError("No record found to delete", 500, "");
        }
        var result = await Stands.remove({ standId: standId }).exec();
        if (!_.isEmpty(result)) {
            const UsersResult= await Users.deleteMany({ standId: standId }).exec();
            const PriceRulResult= await PriceRules.deleteMany({ standId: standId }).exec();
            const CardResult= await Cards.deleteMany({ standId: standId }).exec();
            return res.json({ status: "success", message: "Stand Deleted successfully!.." });
        } else {
            throw new ApplicationError("Unable to delete the record", 500, "");
        }
    } catch (error) {
        return res.json({ status: "fail", error: error })
    }
}
