/**
 * Module dependencies.
*/
const logger = require('./logger');
var morgan = require('morgan'),
	fs = require('fs'),
	bodyParser = require('body-parser'),
	methodOverride = require('method-override');
	// logFile = fs.createWriteStream(__dirname + '/../easy_parkings.log', { flags: 'a' }),
	// logInfo = ':remote-user [:date] ":method :url HTTPS/:http-version" :status :res[content-length] :response-time ms ":referrer"';
var cors = require('cors');

module.exports = function (app) {
	app.set('showStackError', true);
	app.use(morgan('combined', { stream: logger.stream }));
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
	app.use(methodOverride());
	app.use(function (req, res, next) {
		if (req.headers.origin) {
			res.setHeader('Access-Control-Allow-Origin', req.headers.origin);
		}
		res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
		res.setHeader('Access-Control-Allow-Credentials', true);
		res.setHeader('Access-Control-Allow-Headers', 'origin, X-requested-with, content-type, accept, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers, access-control-allow-credentials');
		next();
	});
	app.use(cors())

}
