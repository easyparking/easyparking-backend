const winston = require('winston');
var appRoot = require('app-root-path');
const date = new Date().toDateString();
var options = {
    levels: { error: 0, warn: 1, info: 2, verbose: 3, debug: 4, critical: 5 },
    file: {
      level: 'warn',
      filename: `${appRoot}/api-logs/app.log`,
      handleExceptions: true,
      json: true,
      maxsize: 5242880, // 5MB
      maxFiles: 5,
      colorize: false,
    },
    console: {
      level: 'debug',
      handleExceptions: true,
      json: false,
      colorize: true,
    }
  };
  const logConfiguration = {
    levels: options.levels,
    'transports': [
        new winston.transports.File(options.file),
        new winston.transports.Console(options.console)
    ],
    format: winston.format.combine(
        winston.format.timestamp({
           format: 'MMM-DD-YYYY HH:mm:ss'
       }),
        winston.format.printf(info => `${info.level}: ${[info.timestamp]}: ${info.message}`),
    ),
    exitOnError: false,
};

var logger = winston.createLogger(logConfiguration);

// create a stream object with a 'write' function that will be used by `morgan`
logger.stream = {
    write: function(message, encoding) {
      // use the 'info' log level so the output will be picked up by both transports (file and console)
      logger.info(message);
    },
  };
  
  module.exports = logger;