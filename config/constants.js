exports.LOGOUT_MSG = (data) => {
    const {
        name,
        standName,
        login,
        logout,
        vechileEntry,
        vechileExit,
        pending,
        collectedAmount,
        cardAmount,
        totalAmount
    } = data;
    return `Emp Name: ${name} Stand Name: ${standName} Login: ${login} Logout: ${logout} Vechicles Entry: ${vechileEntry} Exit: ${vechileExit} Pending: ${pending} Collected Amt: ${collectedAmount} Cards Amt: ${cardAmount} Total Amt: ${totalAmount} - SREETECH`;
}
exports.STAND_CREATED = ({ownerId, standId, password}) => {
    return `Dear Stand Owner! You have chosen the best to get more business. Your A/C.No. ${ownerId} and Stand ID.No. ${standId} is created. Default password for your A/c login is ${password} Please change this default password on your first login. We hope you will always be together with us. – SREETECH`
}
exports.SIGNIN_data = (otp) => {
    return `Your OTP Verification Code is ${otp}. Do not share it with anyone. - SREETECH`
}
exports.EMPLOYEE_LOGIN = (data, ownerDetails) => {
    const {name, startTime, standName,pendingonLogin, standCity} = data;
    return `Dear ${ownerDetails.name}! Your Employee ${name} is logged in to the session ${name} located at ${standName}, ${standCity}. Login : ${startTime} Vehicles Pending on login: ${pendingonLogin} - SREETECH`
}
exports.PARKING_ENTRY = ({vehicleNumber, standName, location, parkingId, parkingTime,parkingDate}) => {
    return `Thank you for parking your Veh. No. ${vehicleNumber} at ${standName}, ${location} with parking ID: ${parkingId} on ${parkingDate} at ${parkingTime}. Please download the App link Easy Parking from Playstore to get the e-slip details. - SREETECH`
}
exports.PARKING_EXIT = ({vehicleNumber, standName, location, parkingId, exitDate, exitTime, finalPrice}) => {
    return `Your Veh.No. ${vehicleNumber} with ID.NO. ${parkingId} is exited on ${exitDate} at ${exitTime}. from at ${standName}, ${location}. Thank you for paying parking charges of Rs.${finalPrice}/-. Please download the App Easy Parking from Google Playstore to get the e-slip. - SREETECH`
}
exports.MONTHLY_CARD_PURCHASE_OFFLINE = ({customerName}) => {
    return `Dear ${customerName}, On this Covid pandemic, Please avoid contact touchings. Buy online Monthly Parking Cards and get Smart Parking Services through. www.easyparkings.in Click to install https://bit.ly/2RoTAij - SREETECH`
}
exports.MONTHLY_CARD_PURCHASE_ONLINE = ({customerName, amount, cardId, startDate, endDate}) => {
    return `Dear ${customerName}! Thank you for purchasing of monthly parking card of Rs. ${amount}/- Your Card ID is ${cardId} and valid for 30 days i.e., from ${startDate} to ${endDate}. Please renew your card before it expires and avoid normal charges. - SREETECH`
}
exports.MONTHLY_CARD_REMINDER = (data) => {
    return `Your data Verification Code is ${data}. Do not share it with anyone. - SREETECH`
}
exports.MONTHLY_CARD_EXPIRED = ({cardId, vehicleNumber, standName, expiryDate, standCity}) => {
    return `Your Monthly Parking Card ID.No: ${cardId} for the Vehicle No. ${vehicleNumber} to park at ${standName} ${standCity} had expired on ${expiryDate}. Please Renew Today! - SREETECH`
}
exports.SLIP_LOST = (data) => {
    return `Your data Verification Code is ${data}. Do not share it with anyone. - SREETECH`
}
exports.MATCHED_VEHICLE = (data, vehicleData) => {
    return `Your Vehicle with No. ${vehicleData.vehicleNumber} Located at ${data.standName},${data.location} on ${data.matchedDate} Please take Local Police Support. Show your Vehicle records at the stand. App Link: https://play.google.com/ - SREETECH`
}
exports.STAND_RENEWAL_REMINDER = (data) => {
    return `Your data Verification Code is ${data}. Do not share it with anyone. - SREETECH`
}
exports.STAND_LICENCE_EXPIRED = (data) => {
    return `Your data Verification Code is ${data}. Do not share it with anyone. - SREETECH`
}
exports.APP_DOWNLOAD_URL = (data) => {
    return `Your data Verification Code is ${data}. Do not share it with anyone. - SREETECH`
}
