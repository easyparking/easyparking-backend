var express = require('express'),
    fs = require('fs'),
    env = process.env.NODE_ENV || 'production',
    log = require('./config').log,
    auth = require('./middlewares/authorization'),
    mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.set('debug', (env !== 'production' ? false : false));
exports.mongoose = mongoose;

exports.initialize = function (config, callback) {
    try{
        mongoose.set('useUnifiedTopology', true)
        mongoose.set( 'useNewUrlParser', true )
        mongoose.connect(config.parkingsdb.url, function (err, conct) {
            if (err)
                console.log("err--------------------".err);
            else {
                console.log("mongo connected" );
            }
            var models_path = __dirname + '/../models';
            fs.readdirSync(models_path).forEach(function (file) {
                var fileParts = file.split(".");
                if (fileParts[fileParts.length - 1] !== "js") {
                    return;
                }
                require(models_path + '/' + file);
            });
            app = express();
            require("./express")(app);
            app.use(express.static('public'));
            require('../controllers/my_requires')(auth);
            require("./routes")(app, auth);
            app.get('*', function (req, res) {
                res.sendFile(path.join(__dirname,'/../public/index.html'));
            });
            
            if (callback) {
                callback(app);
            }
        })
    } catch(error){
        console.log("Mongo db error: ", error);
    }
    
};


exports.run = function (app, config, port, callback) {

    server = app.listen(port,config.hostName, function () {
        console.log("Port is ", port)
        log.info("running on port " + port);
    });
    server.timeout = 0;
    if (callback) {
        callback(server);
    }

};

exports.stop = function (server, callback) {
    server.close(function () {
        log.info("server was stopped");
        if (callback) {
            callback();
        }
    });
}
