module.exports = function (app,auth) {

    const Dashboard = require('../controllers/dashboard');
        //Admin Dashbaord
        app.get('/dashboard', Dashboard.getAllData)
        app.get('/getDashboardDetails', Dashboard.getDashboardDetails)
        //Owner Dashbaord
        app.get('/getOwnersDashboard/:ownerId',Dashboard.getOwnersDashboard);
        app.get('/getOwnerDashboardDetails',Dashboard.getOwnerDashboardDetails);
        app.get('/getParkingsByStand/',Dashboard.getParkingsCountByStand);
        app.post('/getGraphData/:ownerId',Dashboard.getGraphData);
        //Employee Dashbaord
        app.get('/getParkingsByEmployee/:id',Dashboard.getParkingsByEmployee);
        app.get('/getParkingsByEmployeeStand',Dashboard.getParkingsByEmployeeStand);
 
    const Users = require('../controllers/users');
        app.get('/verifyUserName/:userName',Users.verifyUserName);
        app.post('/createUser',Users.createUser);
        app.post('/userSignIn',Users.userSignIn);
        // app.get('/getUserData/:id',auth.authenticateToken,Users.getUserData);
        app.get('/getUserData/:id',Users.getUserData);
        // app.get('/getOwnerNames', Users.getOwnerNames)
        app.post('/resetPassword', Users.resetPassword);

        app.post('/getAllUsers',Users.getAllUsers);
        app.post('/searchUser',Users.searchUser); 
        app.put('/updateUser/:id',Users.updateUser); 
        app.delete('/deleteUser/:id', Users.deleteUser);
        app.put('/updateLoginStatus',Users.updateLoginStatus);
        app.post('/refresh',Users.refresh);

    const Customers = require('../controllers/customers');
        // app.post('/createCustomer', Customers.createCustomer);
        // app.get('/checkUserName/:userName', Customers.checkUserName);
        app.get('/sendOTP/:phoneNumber', Customers.sendOTP);
        app.get('/getMonthlyCards/:userName', Customers.getMonthlyCards);
         
    const SMS = require('../controllers/SMS');
    // app.get('/sendsms',SMS.sendSMS);
    app.get('/testSMS',SMS.testSMS);

    const Owners = require('../controllers/owners');
        app.post('/getOwners',Owners.getOwners);
        app.get('/getOwnerNames',Owners.getOwnerNames);
        app.post('/updateOwnerStatus',Owners.updateStatus);
        app.post('/updateOwner',Owners.updateOwner);
        app.get('/getActiveAccounts',Owners.getActiveAccounts);

    const Stands = require('../controllers/stands');
        app.post('/createStand',Stands.createStand)    
        app.get('/getAllStands',Stands.getAllStands)    
        app.get('/getStandNames/:empId', Stands.getStandNames)
        app.get('/getStand/:standId', Stands.getStand)
        app.get('/getStandNamesByOwner/:ownerId', Stands.getStandNamesByOwner)
        app.put('/updateStandSMS/:standId', Stands.updateStandSMS)
        app.put('/updateStand/:standId', Stands.updateStand)
        app.delete('/deleteStand/:standId', Stands.deleteStand)
        app.get('/getOnlineCardsCount', Stands.getOnlineCardsCount)
        
    const PriceRules = require('../controllers/priceRules');
        app.get('/getPriceRules',PriceRules.getPriceRules)
        app.post('/addUpdatePriceRules',PriceRules.addUpdatePriceRules)

    const Parkings = require('../controllers/parkings');
        app.post('/createParking',Parkings.createParking);
        app.get('/getParkingsList',Parkings.getParkingsList);        
        app.get('/getParkingDetails',Parkings.getParkingDetails);        
        app.get('/getParkingDetailsByOwner',Parkings.getParkingDetailsByOwner);        
        app.get('/getDetailsByPhone',Parkings.getDetailsByPhone);        
        app.get('/getParkingHistory',Parkings.getParkingHistory);        
        app.put('/exitParking',Parkings.exitParking); 
        app.delete('deleteParking', Parkings.deleteParking);
        app.get('/getParkedVehicles',Parkings.getParkedVehicles);
        app.get('/getSlipLostVehiclesByOwner/:ownerId',Parkings.getSlipLostVehiclesByOwner);
        app.get('/checkParkingByCard',Parkings.checkParkingByCard);
        app.get('/getCardsForParking',Parkings.getCardsForParking);
        app.get('/getParkingsEntryHisotry',Parkings.getParkingsEntryHisotry);

    const Employees = require('../controllers/employees');
        app.post('/getEmployees',Employees.getEmployees);
        app.get('/getEmployeesByStand/:ownerId',Employees.getEmployeesByStand);
        app.put('/updateEmployee',Employees.updateEmployee);
        app.put('/updateEmployeeStatus/:id',Employees.updateEmployeeStatus);
        app.delete('/deleteEmployee/:id', Employees.deleteEmployee);


    const Locations = require('../controllers/location') 
        app.post('/addState',Locations.addState);
        app.post('/addDistrict',Locations.addDistrict);
        app.post('/addCity', Locations.addCity)

        app.get('/getStates', Locations.getStates)
        app.get('/getDistricts/:stateId', Locations.getDistricts)
        app.get('/getCities/:districtId', Locations.getCities)

        app.delete('/deleteState/:id', Locations.deleleState)
        app.delete('/deleteDistrict/:id', Locations.deleteDistrict)
        app.delete('/deleteCity/:id', Locations.deleleCity)

        app.put('/updateState/:id', Locations.updateState)
        app.put('/updateDistrict/:id', Locations.updateDistrict)
        app.put('/updateCity/:id', Locations.updateCity)

    const Cards = require('../controllers/card')
        app.post('/addCard',Cards.addCard);  
        app.get('/getCardId', Cards.getCardId);  
        app.post('/getCardById/', Cards.getCardById);  
        app.get('/getCardsList/', Cards.getCardsList)  
        app.put('/cardRenewal/', Cards.cardRenewal) 
        app.get('/getOfflineCardsByOwner', Cards.getOfflineCardsByOwner)   
        app.get('/getOfflineCardsByStand', Cards.getOfflineCardsByStand)   
        app.get('/getExpiryCards/:standId', Cards.getExpiryCards)   
        app.get('/getPaymentReports/', Cards.getPaymentReports)   

    const job_applications = require('../controllers/jobApplications')
        app.post('/createJobApplication', job_applications.createJobApplication);
        app.get('/getJobApplicationsList', job_applications.getJobApplicationsList);

    const Vehicles = require('../controllers/vehicles')
        app.post('/addVehicleName', Vehicles.addVehicleName);
        app.get('/getVehicleNames', Vehicles.getVehicleNamesList);
        app.get('/getVehiclesList', Vehicles.getVehiclesList);
        app.put('/updateVehicle', Vehicles.updateVehicle);
        app.delete('/deleteVehicleName/:id', Vehicles.deleteVehicleName);

    const Plans = require('../controllers/plans')
        app.post('/addstandPlans', Plans.addStandPlans);
        app.get('/getAllPlansList', Plans.getAllPlansList);
        app.get('/getPlansList', Plans.getPlansList);
        app.put('/updatePlanType', Plans.updatePlanType);
        app.delete('/deleteStandType/:id', Plans.deleteStandType);

    const ParkingTypes = require('../controllers/parkingTypes')
        app.post('/addParkingType', ParkingTypes.addParkingType);
        app.get('/getParkingTypesList', ParkingTypes.getParkingTypesList);
        app.get('/getAllParkingTypes', ParkingTypes.getAllParkingTypes);
        app.put('/updateParkingType', ParkingTypes.updateParkingType);
        app.delete('/deleteParkingType/:id', ParkingTypes.deleteParkingType);
        
    const VehicleTypes = require('../controllers/vehicleTypes')
        app.post('/addVehicleType', VehicleTypes.addVehicleType);
        app.get('/getVehicleTypesList', VehicleTypes.getVehicleTypesList);
        app.get('/getAllVehicleTypes', VehicleTypes.getAllVehicleTypes);
        app.put('/updateVehicleType', VehicleTypes.updateVehicleType);
        app.delete('/deleteVehicleType/:id', VehicleTypes.deleteVehicleType);
    
    const appDownloads = require('../controllers/appDownloads');
        app.get('/sendDownloadUrl/:mobileNumber', appDownloads.sendDownloadUrl);

    const Demos = require('../controllers/demo');
        app.post('/createDemo', Demos.createDemo);
        app.get('/getRequestDemos', Demos.getRequestDemos);

    const wantedVehicles = require('../controllers/wantedVehicles')
        app.post('/addWantedVehicle', wantedVehicles.addWantedVehicle);
        app.get('/getWantedVehiclesList', wantedVehicles.getWantedVehiclesList);
        app.delete('/deleteWantedVehicle/:id', wantedVehicles.deleteWantedVehicle);
        app.put('/updateWantedVehicle/:id', wantedVehicles.updateWantedVehicle);

    const Analytics = require('../controllers/analytics');
        app.post('/getAnalytics', Analytics.getAnalytics);

    const ForceLogout = require('../controllers/forceLogout');
        app.get('/logoutList', ForceLogout.logoutList);
        app.get('/getSessionHistory/:userName', ForceLogout.getSessionHistory);

    const Reports = require('../controllers/reports');
        app.post('/getCardSalesReports', Reports.getCardSalesReports);
        app.get('/getEmpList/:ownerId', Reports.getEmpList);
        app.get('/getEmpSessionsList/:userName', Reports.getEmpSessionsList);
        app.post('/getEmployeeReport', Reports.getEmployeeReport)

    const LoginActivity = require('../controllers/loginActivity');
        app.post('/getLoginActivity', LoginActivity.getLoginActivity);
        app.get('/getAllEmployeesList/:ownerId', LoginActivity.getAllEmployeesList);

    const Sessions = require('../controllers/sessions');
        app.post('/createSession',Sessions.createSession);
        app.put('/endSession',Sessions.endSession);
        app.post('/checkSession', Sessions.checkSession);
        app.post('/getSessionReportByOwner/:empId', Sessions.getSessionReportByOwner);
        // app.get('/endOthersSessions/:standId', Sessions.endOthersSessions)

    const homeScreen = require('../controllers/home_screen');
        app.post('/searchStand', homeScreen.searchStand);
        app.get('/getAllCities/:stateId', homeScreen.getAllCities);
    
    const payments = require('../controllers/payments');
        app.post('/paymentRequest', payments.paymentRequest);
    
    const HelpRequests = require('../controllers/help_request');
        app.post('/createHelpRequest', HelpRequests.createHelpRequest);
        app.get('/getHelpRequestList', HelpRequests.getHelpRequestList);
};
