module.exports = {
  generateAccessToken:function(username) {
    return jwt.sign(username, process.env.ACCESS_TOKEN_SECRET, { algorithm: "HS256",expiresIn: "900s" }); //process.env.ACCESS_TOKEN_LIFE
  },
  generateRefreshToken:function(username){
    return jwt.sign(username, process.env.ACCESS_TOKEN_SECRET, { algorithm: "HS256",expiresIn: "24h" });
  },
  authenticateToken: function (req, res, next) {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];
    console.log(token,"...")
    if (token == null){
      //return res.sendStatus(401);
       return res.json({status:"fail",message: "null token"});
    }

    jwt.verify(token,process.env.ACCESS_TOKEN_SECRET, function(err,user){
      if (err){
        return res.json({status:"fail",message:err.name});
      }
      req.user = user; 
      next();
    })
  }
}
