/*
	App configurations
*/

module.exports = {
	env: 'production',
	root: require('path').normalize(__dirname + '/..'),
	log: new (require("log"))('debug'),
	logger: "production",
	textLocalAPIkey: "ZTgwMjI3YTI0NGIyZGNlNmFlNzQ0MjNlYWY2ODc2MTQ=",
	instamojoInfo: {
		API_KEY: 'f373f539bd6c243eff012d7e7f1a6958',
		AUTH_KEY: '75a76743aee32d4ee98ba7d3fa1fe833'
	},
	port: 3000,
	hostName: '127.0.0.1',
	parkingsdb: {
		url: "mongodb+srv://easyparking:easyparking@cluster0.igwtr.mongodb.net/easyParking?retryWrites=true&w=majority",
		host: "localhost",
		name: "easy_parkings",
		port: 27017,
		user: "",
		pwd: ""
	}
};
