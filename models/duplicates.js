var mongoose = require('mongoose');
Schema = mongoose.Schema;

var duplicates = new Schema({
    // "sessionId":{type:String},
    "duplicates":{type: Array},
	"created_date":{type: Date, default: Date.now}
});
mongoose.model('duplicates', duplicates, 'duplicates'); 