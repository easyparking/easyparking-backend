var mongoose = require('mongoose');
Schema = mongoose.Schema;

var sessions = new Schema({
	"start_time":{type:Date},
    "end_time":{type:Date},
    "userName" : {type:String},
    "standName" : {type:String},
    "standId" : {type:String},
    "employeeId" : {type:String},
    "ownerId" : {type:String},
    "role" : {type:String},
    "ipaddress" : {type:String},
    "name" : {type:String},
    "phone" : {type:String},
    "pendingonLogout" : {type:Number},
    "pendingonLogin" : {type:Number},
    "entries": {type:Number},
    "exits": {type:Number},
    "cancelled": {type:Number},
    "cardAmount": {type:Number}, 
    "offlineCards": {type:Number}, 
    "parkingsAmount": {type:Number},
	"createdDate":{type: Date, default: Date.now}
});
mongoose.model('sessions', sessions, 'sessions');