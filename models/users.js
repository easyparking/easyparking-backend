var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
    crypto = require('crypto');
var User = new Schema({
	"name": {type: String,required: true},
    "userName":{type:String,required: true},
	"privileges":{ type:Array,default:[]},
    "role":{type:String,required: true},
    "ownerId":{type:String,default:""},
    "employeeId":{type:String,default:""},
    "customerId":{type:String},
    "vehicleNumber":{type:String},
    "standId":{type:String,default:""},
    "standName":{type:String,default:""},
    "phoneNumber":{type:String,required: true},
    "onlinePaymentPercentage":{type:String,default:""},
    "status":{ type: String, default:"Active"},
    "loginStatus":{ type: Boolean, default:false},
    "token":{type:String},
    "planType": {type:String},
    "emailId": {type:String},
    "refreshToken":{type:String},
    "auth": {
        "auth_type": {type: String},
        "raw": {type: String},
        "auth_id": {type: String},
        "password": {type: String},
        "salt": {type: String}
    },
    "lastLoginSession": {type: Date, default: null},
    "lastLogoutSession": {type: Date, default: null},
    "createdDate":{type: Date, default: Date.now},
    "expiryDate": {type: Date},
    "rentalDuration": {type: String},
    "modifiedDate": {type: Date}
});

// User.virtual('password')
//     .set(function (password) {
        
//         this._password = password
//         this.auth.salt = this.makeSalt()
//         this.auth.password = this.encryptPassword(password)
//         // console.log(this.auth.password);
        
//     })
//     .get(function () {
//         return this._password
//     })


// var validatePresenceOf = function (value) {
//     return value && value.length
// }

// function lowercase(val) {
//     return val.toLowerCase();
// }

// User.path('auth.password').validate(function (hashed_password) {    
//     if(this.auth.auth_type != "") { 
//         return true;       
//     } else if (hashed_password === undefined) {
//         return false;       
//     } else {
//         return hashed_password.length;
//     }
// }, 'Password cannot be blank')


// User.pre('save', function (next) {    
//     if (!this.isNew){
//         console.log('adada----------->',this);
//         return next()
//     }

//     if ((this.auth.auth_type == "" || this.auth.auth_type === undefined) && !validatePresenceOf(this.auth.password))
//         next(new Error('Invalid password'))
//     else
//         next()
// })


// User.method('authenticate', function (plainText) {
//     console.log(plainText,"---------------")
//     return this.encryptPassword(plainText) === this.auth.password
   
//         console.log(this.encryptPassword(plainText),"-------------------")
// })

// User.method('makeSalt', function () {
//     return Math.round((new Date().valueOf() * Math.random())) + ''
// })

// User.method('encryptPassword', function (password) {
//     if (!password)
//         return ''
//     return crypto.createHmac('sha512', this.auth.salt).update(password).digest('hex')
// })

mongoose.model('User', User);