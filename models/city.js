var mongoose = require('mongoose');
Schema = mongoose.Schema;

var city = new Schema({
    "city":{type:String},    
    "districtId" : {type:String}, 
    "stateId" : {type:String},   
	"created_date":{type: Date, default: Date.now}
});
mongoose.model('city', city, 'city');