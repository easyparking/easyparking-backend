var mongoose = require('mongoose');
Schema = mongoose.Schema;

var state = new Schema({
	"state":{type:String, required: true},
	"districtsCount": {type:Number,default: 0},
	"created_date":{type: Date, default: Date.now}
});
mongoose.model('state', state, 'state');