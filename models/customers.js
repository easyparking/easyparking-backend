const mongoose = require('mongoose');
Schema = mongoose.Schema;

var customer = new Schema({
	"name": {type: String,required: true},
    "userName":{type:String,required: true},
	"privileges":{ type:Array,default:[]},
    "role":{type:String,required: true},
    "ownerId":{type:String,default:""},
    "employeeId":{type:String,default:""},
    "customerId":{type:String},
    "standId":{type:String,default:""},
    "standName":{type:String,default:""},
    "phoneNumber":{type:String,required: true},
    "onlinePaymentPercentage":{type:String,default:""},
    "status":{ type: String, default:"Active"},
    "loginStatus":{ type: Boolean, default:false},
    "token":{type:String},
    "planType": {type:String},
    "emailId": {type:String},
    "refreshToken":{type:String},
    "auth": {
        "auth_type": {type: String},
        "auth_id": {type: String},
        "password": {type: String},
        "salt": {type: String}
    },
    "lastLoginSession": {type: Date, default: null},
    "lastLogoutSession": {type: Date, default: null},
    "createdDate":{type: Date, default: Date.now}
});
mongoose.model('customer', customer, 'customer');