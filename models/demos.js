var mongoose = require('mongoose');
Schema = mongoose.Schema;

var demos = new Schema({
    "name":{type:String},
    "location":{type:String},
    "email":{type:String},
    "phone" : {type:String},
    "address" : {type:String},
    "message" : {type:String},
	"created_date":{type: Date, default: Date.now}
});
mongoose.model('demos', demos, 'demos'); 