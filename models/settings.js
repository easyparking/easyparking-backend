var mongoose = require('mongoose');
Schema = mongoose.Schema;

var settings = new Schema({
	"process_changes":{type:String},
	"instructions":{type:String},
	"createdDate":{type: Date, default: Date.now}
});
mongoose.model('settings', settings, 'settings');