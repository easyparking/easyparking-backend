const mongoose = require('mongoose');
Schema = mongoose.Schema;

const cardHistory = {
    startDate: {type: String,required: true},
    endDate: {type: String,required: true},
    duration: {type: String,required: true},
    amount: {type: String,required: true},
    createdDate: { type: String,default: Date.now },
}
const card = new Schema({
    "standId": {
        type: String,
        required: true
    },
    "standName": {
        type: String,
        required: true
    },
    "standId": {
        type: String,
        required: true
    },
    "cardId": {
        type: String,
        required: true
    },
    "ownerId": {
        type: String,
        required: true
    },
    "createdBy": {
        type: String,
        required: true
    },
    "employeeId": {
        type: String
    },  
    "vehicleType": {
        type: String,
        required: true
    },
    "vehicleName": {
        type: String
    },
    "vehicleNumber": {
        type: String
    },
    "isOfflineData": {
        type: Boolean
    },
    "customerName": { type: String,required: true },
    "customerPhone": {
        type: String, required: true
    },
    "cardHistory": {type: [cardHistory]},
    "role":{
        type: String,
        required: true
    },
    "amount": { type: Number, required: true },
    "duration": { type: String, required: true },
    "status": { type: String, default: 'Active' },
    "type": { type: String },
    "createdDate": { type: Date,default: Date.now },
    "renewedDate": { type: Date,default: "" },
    "startDate": { type: Date,default: "", required: true },
    "endDate": { type: Date,default: "" },
});
mongoose.model('card', card, 'card');