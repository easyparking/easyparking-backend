var mongoose = require('mongoose');
Schema = mongoose.Schema;

var reports = new Schema({
	"sessionId":{type:String},
	"standId":{type:String},
	"ownerId":{type:String},
	"employeeId":{type:String},
	"logoutReport":{type: Object},
	"PendingVehiclesReport":{type: Array},
	"PendingLogoutVehiclesReport":{type: Array},
	"PendingCurrentVehiclesReport":{type: Array},
	"cardsReport":{type: Array},
	"createdDate":{type: Date, default: Date.now}
});
mongoose.model('reports', reports, 'reports');