var mongoose = require('mongoose');
Schema = mongoose.Schema;

var employees = new Schema({
	"ownerId": {type:String, default:""},
	"employeeId": {type:String, default:""},
    "employeeName": {type:String, default:""},
    "phone": {type:String},
    "status":{type:String, default:""},
    //"salary":{type:String, default:""}
	//"advance":{type:String, default:""}
    "created_date":{type: Date, default: Date.now}
});

mongoose.model('employees', employees, 'employees');