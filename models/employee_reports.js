var mongoose = require('mongoose');
Schema = mongoose.Schema;

var employee_reports = new Schema({
	"name":{type:String},
	"qualification":{type:String},
	"age":{type:String},
	"emal":{type:String},
	"phone":{type:String},
	"state":{type:String},
	"city":{type:String},
	"town":{type:String},
	"address":{type:String},
	"message":{type:String},
	"created_date":{type: Date, default: Date.now}
});
mongoose.model('employee_reports', employee_reports, 'employee_reports');