var mongoose = require('mongoose');
Schema = mongoose.Schema;

var job_applications = new Schema({
	"applicantId":{type:String},
	"name":{type:String},
	"qualification":{type:String},
	"age":{type:String},
	"email":{type:String},
	"phone":{type:String},
	"state":{type:String},
	"city":{type:String},
	"district":{type:String},
	"address":{type:String},
	"message":{type:String},
	"prefferedSalary": {type: String},
	"createdDate":{type: Date, default: Date.now}
});
mongoose.model('job_applications', job_applications, 'job_applications');