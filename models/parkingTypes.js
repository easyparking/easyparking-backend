const mongoose = require('mongoose');
Schema = mongoose.Schema;

const parking_types = new Schema({
	"type": {type:String, required: true},
	// "districtsCount": {type:Number,default: 0},
	"created_date":{type: Date, default: Date.now}
});
mongoose.model('parking_types', parking_types, 'parking_types');