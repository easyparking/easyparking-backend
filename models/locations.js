var mongoose = require('mongoose');
Schema = mongoose.Schema;

var locations = new Schema({
	"state":{type:String},
	"districts":{type:String},
	"created_date":{type: Date, default: Date.now}
});
mongoose.model('locations', locations, 'locations');