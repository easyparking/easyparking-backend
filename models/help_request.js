var mongoose = require('mongoose');
Schema = mongoose.Schema;

var help_request = new Schema({
	"name":{type:String},
	"email":{type:String},
	"subject":{type:String},
	"message":{type:String},
	"createdDate":{type: Date, default: Date.now}
});
mongoose.model('help_request', help_request, 'help_request');