const mongoose = require('mongoose');
Schema = mongoose.Schema;

const vehicle_types = new Schema({
	"type": {type:String, required: true},
	"code": {type:String, required: true},
	"created_date":{type: Date, default: Date.now}
});
mongoose.model('vehicle_types', vehicle_types, 'vehicle_types');