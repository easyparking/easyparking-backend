const mongoose = require('mongoose');
Schema = mongoose.Schema;

const Stand = new Schema({
	"ownerId":{type:String},
	"standId":{type:String},
	"planType":{type:String},
	"standType":{type:String},
	"ownerName":{type:String},
	"employeeCount":{type:Number, default: 0},
	"location":{type:Object},
	"state": {type: String},
	"city": {type: String},
	"standName":{type:String},
	"status": {type:String},
	"address":{type:String},
	"addressLine1":{type:String},
	"addressLine2":{type:String},
	"addressLine3":{type:String},
	"addressLine4":{type:String},
	"standPhone":{type:String},
	"policeStation":{type:String},
	"policeStationContact":{type:String},
	"latitude":{type:String},
	"longtitue":{type:String},
	"standImage":{type: Object},
	"GSTIn":{type:String},
	"initialGracePeriod":{type:Number},
	"lateGracePeriod":{type:Number},
	"bicycle":{type:Object},
	"twoWheeler":{type:Object},
	"threeWheeler":{type:Object},
	"fourWheeler":{type:Object},
	"allowedVehicles": {type: Array},
	// "offer": {type:String},
	"SMS": {
		"entry" : {type:Boolean},
		"exit" : {type:Boolean},
		"monthlyCards" : {type:Boolean},
	},
	"allowOnlineCards" : {type : Boolean},
	"allowOfflineCards" : {type : Boolean},
	"isOnline": {type :Boolean},
    "createdDate":{type: Date, default: Date.now},
	"expiryDate": {type: Date, default: null},
	"note": {type:String},
	"appLink": {type:String},
	"isPublic": {type:Boolean},
	"standCode": {type: String}
});
mongoose.model('stands', Stand,'stands');
