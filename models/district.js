var mongoose = require('mongoose');
Schema = mongoose.Schema;

var district = new Schema({
    "district":{type:String},
    "districtId":{type:String},
    "citiesCount":{type:Number, default: 0},
    "stateId" : {type:String},
	"created_date":{type: Date, default: Date.now}
});
mongoose.model('district', district, 'district'); 