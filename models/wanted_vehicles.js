var mongoose = require('mongoose');
Schema = mongoose.Schema;

var wanted_vehicles = new Schema({
	"vehicleNumber":{type:String},
	"type":{type:String},
	"phoneNumber":{type:String},
	"insuranceValidty":{type:String},
	"missingDate":{type:String},
	"requestBy":{type:String},
	"requestDate":{type:String},
	"policeComplaintMade":{type:String},
	"remarks":{type:String},
	"isMatched":{type:Boolean, default: false},
	"createdDate":{type: Date, default: Date.now},
	"engineNumber":{type:String},
	"chassisNumber":{type:Number},
	"insuranceFrom":{type:Date},
	"insuranceTo":{type:Date},
	"cBook": {type: String},
	"locatedAt": {type: String,default: ''},
	"stolenDate":{type:Date},
	"matchedDate": {type: Date}
});
mongoose.model('wanted_vehicles', wanted_vehicles, 'wanted_vehicles');