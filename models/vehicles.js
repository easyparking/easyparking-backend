var mongoose = require('mongoose');
Schema = mongoose.Schema;

var vehicles = new Schema({
	"vehicleName":{type:String, required: true},
	"type":{type:String, required: true},
	"createdDate":{type: Date, default: Date.now}
});
mongoose.model('vehicles', vehicles, 'vehicles');