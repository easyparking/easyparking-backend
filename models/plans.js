const mongoose = require('mongoose');
Schema = mongoose.Schema;

const plans = new Schema({
	"type": {type:String, required: true},
	// "districtsCount": {type:Number,default: 0},
	"created_date":{type: Date, default: Date.now}
});
mongoose.model('plans', plans, 'plans');