var mongoose = require('mongoose');
Schema = mongoose.Schema;

var itemsObject ={
	"vehicletype":{type:String, required: true},
	"amount":{type:Number, required: true},
	"duration":{type:Number , required: true},
	"cardActualPrice":{type:Number , required: true},
	"cardOfferPrice":{type:Number, required: true},
	"fixedPriceFor1stHour":{type:Number , required: true, default:0},
	"forEveryNextHour":{type:Number, required: true, default:0},
	"cardMaxDiscount":{type:Number,required: true},
}
var price_rules = new Schema({
	"standId":{type:String},
	"ownerId":{type:String},
	"vehicleTypes": [itemsObject]
});
mongoose.model('price_rules', price_rules, 'price_rules');