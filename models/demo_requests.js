var mongoose = require('mongoose');
Schema = mongoose.Schema;

var demo_requests = new Schema({
	"name":{type:String},
	"location":{type:String},
	"phone":{type:String},
	"email":{type:String},
	"address":{type:String},
	"message":{type:String},
	"createdDate":{type: Date, default: Date.now}
});
mongoose.model('demo_requests', demo_requests, 'demo_requests');