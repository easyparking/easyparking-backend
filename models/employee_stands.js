// owner has stands and employees
// owner can have multiple stands
// employee logins
var mongoose = require('mongoose');
Schema = mongoose.Schema;

var employee_stands = new Schema({
	"ownerId": {type:String, default:""},
	"standId": {type:String, default:""},
    "employeeId": {type:String, default:""},
    "created_date":{type: Date, default: Date.now}
});
mongoose.model('employee_stands', employee_stands, 'employee_stands');